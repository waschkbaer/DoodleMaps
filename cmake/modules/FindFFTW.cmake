# - Find FFTW
# Find the native FFTW includes and library
#
#  FFTW_INCLUDES    - where to find fftw3.h
#  FFTW_LIBRARIES   - List of libraries when using FFTW.
#  FFTW_FOUND       - True if FFTW found.


MESSAGE("-- FFTW Module")

IF (NOT APPLE AND NOT WIN32) #LINUX
	
	MESSAGE("--- searching FFTW for Unix")
	find_path (FFTW_INCLUDES fftw3.h PATHS ${NEUROVIS_ROOT_DIR}/ext/FFTW3/include)
	#find_library (FFTW_LIBRARIES NAMES libfftw3 PATHS ${NEUROVIS_ROOT_DIR}/ext/FFTW3/libs/linux)
	find_library (FFTW_LIBRARIES NAMES fftw3)

	if(FFTW_INCLUDES)
		MESSAGE("--- found fftw includes")
		include_directories(FFTW_INCLUDES)
	endif(FFTW_INCLUDES)

	if(FFTW_LIBRARIES)
		MESSAGE("--- found fftw libraries")
	endif(FFTW_LIBRARIES)

	# handle the QUIETLY and REQUIRED arguments and set FFTW_FOUND to TRUE if
	# all listed variables are TRUE
	include (FindPackageHandleStandardArgs)
	find_package_handle_standard_args (FFTW DEFAULT_MSG FFTW_LIBRARIES FFTW_INCLUDES)

	mark_as_advanced (FFTW_LIBRARIES FFTW_INCLUDES)

ENDIF()

IF (APPLE) #APPLE

	MESSAGE("--- searching FFTW for Apple")
	find_path (FFTW_INCLUDES fftw3.h PATHS ${NEUROVIS_ROOT_DIR}/ext/FFTW3/include)
	find_library (FFTW_LIBRARIES NAMES libfftw3 PATHS ${NEUROVIS_ROOT_DIR}/ext/FFTW3/LIBS/osx)

	if(FFTW_INCLUDES)
		MESSAGE("--- found fftw includes")
		include_directories(FFTW_INCLUDES)
	endif(FFTW_INCLUDES)

	if(FFTW_LIBRARIES)
		MESSAGE("--- found fftw libraries")
	endif(FFTW_LIBRARIES)

	# handle the QUIETLY and REQUIRED arguments and set FFTW_FOUND to TRUE if
	# all listed variables are TRUE
	include (FindPackageHandleStandardArgs)
	find_package_handle_standard_args (FFTW DEFAULT_MSG FFTW_LIBRARIES FFTW_INCLUDES)

	mark_as_advanced (FFTW_LIBRARIES FFTW_INCLUDES)

ENDIF()

IF (WIN32) #WINDOWS
	MESSAGE("--- searching FFTW for Windows")
	set( FFTW_DIR "${NEUROVIS_ROOT_DIR}/ext/FFTW3/libs/windows" CACHE FILEPATH FFTW_DIR )
	include_directories(${FFTW_DIR})
	link_directories(${FFTW_DIR})
ENDIF ()


