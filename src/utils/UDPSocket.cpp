
#include "mocca/net/stream/stddefines.h"
#ifdef DETECTED_OS_WINDOWS
#define NOMINMAX
#include "mocca/net/stream/Sockets.h"

#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#endif

#include "utils/UDPSocket.h"
#include "mocca/log/LogManager.h"
#include "mocca/base/Memory.h"

#include "silverbullet/time/Timer.h"


#include <stdlib.h>
#include <errno.h>
#include <time.h>

const uint64_t MAX_PACKAGE_SIZE = 65535;

using namespace utils;


UDPSocket::UDPSocket():
_filter(new SimpleDataFilter())
{
}

UDPSocket::~UDPSocket() {
    interrupt();
    LDEBUG("UDPSocket | UDP socket deconstructed port "<<_port);
}

void UDPSocket::bindPort(int port) {
#ifdef DETECTED_OS_WINDOWS
    WSADATA wsa;
    if (WSAStartup(0x0101, &wsa) != 0)
    {
        LERROR("Could not open Windows connection.");
        return;
    }
#endif

    _port = port;
    _socket = socket (AF_INET, SOCK_DGRAM, 0);
    if (_socket < 0) {
     LERROR ("UDPSocket | Can not open socket.");
     throw std::exception();
    }

#ifdef DETECTED_OS_WINDOWS
    const char y = '1';
#else
    const int y = 1;
#endif
    struct sockaddr_in servAddr;
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl (INADDR_ANY);
    servAddr.sin_port = htons (port);
    setsockopt(_socket, SOL_SOCKET, SO_BROADCAST, &y, sizeof(int));

    struct timeval tv;

    tv.tv_sec = 0;
    tv.tv_usec = 500;

    setsockopt(_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));

    int rc = bind ( _socket, (struct sockaddr *) &servAddr, sizeof (servAddr));

    if (rc < 0) {
     LERROR ("UDPSocket | Can not bind port.");
     throw std::exception();
    }
}

void UDPSocket::setDataFilter(std::unique_ptr<SimpleDataFilter> filter){
    _filter = std::unique_ptr<SimpleDataFilter>(std::move(filter));
}

void UDPSocket::send(const mocca::ByteArray& data,std::string host, int port) {
    struct hostent *hp;
    struct sockaddr_in servaddr;

    /* fill in the server's address and data */
    memset((char*)&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);

    /* look up the address of the server given its name */
    hp = gethostbyname(host.c_str());
    if (!hp) {
        LERROR("UDPSocket | could not obtain address");
    }

    /* put the host's address into the server address structure */
    memcpy((void *)&servaddr.sin_addr, hp->h_addr_list[0], hp->h_length);

    /* send a message to the server */
    if (sendto(_socket,(const char*) data.data(), data.size(), 0, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
        LERROR("UDPSocket | sendto failed");
    }
}

std::unique_ptr<mocca::ByteArray> UDPSocket::getNext() {
    if(_dataQueue.size() > 0){
        std::unique_ptr<mocca::ByteArray> ba = std::move(_dataQueue[0]);
        _dataQueue.erase(_dataQueue.begin());
        return ba;
    }
    return nullptr;
}

std::unique_ptr<mocca::ByteArray> UDPSocket::getLatest() {
    if(_dataQueue.size() > 0){
        int latest = _dataQueue.size()-1;
        std::unique_ptr<mocca::ByteArray> ba = std::move(_dataQueue[latest]);
        _dataQueue.clear();
        return ba;
    }
    return nullptr;
}

void UDPSocket::run() {
    sockaddr_in cliAddr;
    socklen_t len;

    char data[MAX_PACKAGE_SIZE];
    int result = 0;

    Core::Time::Timer packageTimer;
    packageTimer.start();
    double current = packageTimer.elapsed();
    double last = current;
    uint64_t packagesTotal = 0;
    uint64_t packagesInLastSecond = 0;
    uint64_t secondCounter = 0;

    LINFO("UDPSocket | Waiting for packages on UDP Port: " << _port);
    while(!isInterrupted()){

        memset (data, 0, MAX_PACKAGE_SIZE);

        len = sizeof (cliAddr);
        result = recvfrom ( _socket, data, MAX_PACKAGE_SIZE, 0,
                       (struct sockaddr *) &cliAddr, &len );

        if(result > 0 && _filter->isValid(data,result)){
            std::unique_ptr<mocca::ByteArray> bytearray(new mocca::ByteArray());
            bytearray->append(data,result);
            _dataQueue.push_back(std::move(bytearray));
            packagesInLastSecond++;
        }

#ifdef DEBUG
        current = packageTimer.elapsed();
        if(current-last > 1000){
            secondCounter++;
            packagesTotal = packagesTotal+packagesInLastSecond;
            LDEBUG("UDPSocket | Packages in Last Second : " << packagesInLastSecond);
            LDEBUG("UDPSocket | Packages average: " << (packagesTotal/secondCounter));
            packagesInLastSecond = 0;
            last = current;
        }
#endif
    }

    LDEBUG("UDPSocket | run loop finished "<<_port);
}
