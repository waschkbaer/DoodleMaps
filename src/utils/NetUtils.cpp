#include "utils/NetUtils.h"

#ifdef DETECTED_OS_WINDOWS
#define NOMINMAX
#include <winsock2.h>
#ifndef libsock
#define libsock
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "wtsapi32.lib")
#endif
#else
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#ifdef DETECTED_OS_WINDOWS
#include <Ws2tcpip.h>
#include <Mstcpip.h>
#define REQUEST_WINSOCK_VERSION (BYTE)2
#define REQUEST_WINSOCK_SUBVERSION (BYTE)2
#endif

#include <stdio.h>
#include <sys/types.h>
#include <string.h>

#include "mocca/log/LogManager.h"

namespace utils{

bool checkLocalIP (const std::string& hostname) {
#ifdef DETECTED_OS_WINDOWS

#else

    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;

    getifaddrs(&ifAddrStruct);

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa ->ifa_addr->sa_family==AF_INET) { // check it is IP4
            // is a valid IP4 Address
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            std::string foundAdr(addressBuffer);
            if(foundAdr == hostname) return true;

            //printf("'%s': %s\n", ifa->ifa_name, addressBuffer);
         } else if (ifa->ifa_addr->sa_family==AF_INET6) { // check it is IP6
            // is a valid IP6 Address
            tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
            char addressBuffer[INET6_ADDRSTRLEN];
            inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);

            std::string foundAdr(addressBuffer,INET_ADDRSTRLEN);
            if(foundAdr == hostname) return true;

            //printf("'%s': %s\n", ifa->ifa_name, addressBuffer);
        }
    }
    if (ifAddrStruct!=NULL)
        freeifaddrs(ifAddrStruct);//remember to free ifAddrStruct
#endif
    return false;

}

std::string getLocalIP(){
#ifdef DETECTED_OS_WINDOWS

#else
    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;

    getifaddrs(&ifAddrStruct);

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa ->ifa_addr->sa_family==AF_INET) { // check it is IP4
            // is a valid IP4 Address
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            std::string foundAdr(addressBuffer);
            if( foundAdr.find(':') == std::string::npos &&
                foundAdr != "127.0.0.1" && foundAdr != "localhost" &&
                foundAdr.find('.') != std::string::npos)
            {
                return foundAdr;
            }

         } else if (ifa->ifa_addr->sa_family==AF_INET6) { // check it is IP6
            // is a valid IP6 Address
            tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
            char addressBuffer[INET6_ADDRSTRLEN];
            inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);

            std::string foundAdr(addressBuffer,INET_ADDRSTRLEN);
            if( foundAdr.find(':') == std::string::npos &&
                foundAdr != "127.0.0.1" && foundAdr != "localhost" &&
                foundAdr.find('.') != std::string::npos)
            {
                return foundAdr;
            }
        }
    }
    if (ifAddrStruct!=NULL)
        freeifaddrs(ifAddrStruct);//remember to free ifAddrStruct
#endif
    return "127.0.0.1";

}

}
