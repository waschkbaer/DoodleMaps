#pragma once

#include "mocca/base/Thread.h"
#include "mocca/base/ByteArray.h"

namespace utils{


class SimpleDataFilter{
public:
    SimpleDataFilter(){};
    ~SimpleDataFilter(){};

    bool isValid(const char* data, int length){
        return true;
    }
private:
};

class UDPSocket : public mocca::Runnable {
public:
	UDPSocket();
	~UDPSocket();

    void bindPort(int port);
    void send(const mocca::ByteArray& data,std::string host, int port);
    std::unique_ptr<mocca::ByteArray> getNext();
    std::unique_ptr<mocca::ByteArray> getLatest();

    void setDataFilter(std::unique_ptr<SimpleDataFilter> filter);

private:
	void run() override;

private:
    std::vector<std::unique_ptr<mocca::ByteArray>> _dataQueue;

	int _socket;
    int _port;
    std::unique_ptr<SimpleDataFilter> _filter;
};

};
