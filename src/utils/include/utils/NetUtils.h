#pragma once

#ifndef NETUTILS
#define NETUTILS
#include "mocca/net/stream/stddefines.h"




#include <stdio.h>
#include <sys/types.h>
#include <string.h>


#include "mocca/log/LogManager.h"

namespace utils{



bool checkLocalIP (const std::string& hostname);

std::string getLocalIP();

};

#endif
