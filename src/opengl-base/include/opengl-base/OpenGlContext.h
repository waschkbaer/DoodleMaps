#pragma once
#include <string>

class OpenGlContext {
public :
    virtual ~OpenGlContext() {};

    bool isValid() const {return bIsValid;}

    float getVersion() const {
      if(bIsValid && strVersion.find_first_of(" ") != std::string::npos) {
        const std::string num = strVersion.substr(0,strVersion.find_first_of(" "));
        return float(atof(num.c_str()));
      } else {
        return 0;
      }
    }

    std::string getVersionInfo() const {return strVersion;}

    std::string getRendererInfo() const {return strRenderer;}

    virtual void makeCurrent() = 0;
protected:
    virtual void createContext() = 0;
    virtual void destroyContext() = 0;

    bool bIsValid;
    std::string strVersion;
    std::string strRenderer;
};

