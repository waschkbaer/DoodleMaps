#pragma once
#include "GLModel.h"
#include <memory>

        
class GLBoundingBox: public GLModel{
public:
  GLBoundingBox();
  GLBoundingBox(Core::Math::Vec3f min, Core::Math::Vec3f max);
  virtual ~GLBoundingBox();

  void update(Core::Math::Vec3f min, Core::Math::Vec3f max);

private:
  std::vector<Core::Math::Vec3f> _Cube;
};
typedef std::shared_ptr<GLBoundingBox> GLBoundingBoxPtr;

class GLBoundingQuad: public GLModel{
public:
  GLBoundingQuad(float* data);
  virtual ~GLBoundingQuad();

private:
};


class GLBoundingQuadX: public GLBoundingQuad{
public:
  GLBoundingQuadX();
  virtual ~GLBoundingQuadX();

private:
};



class GLBoundingQuadY: public GLBoundingQuad{
public:
GLBoundingQuadY();
virtual ~GLBoundingQuadY();

private:
};



class GLBoundingQuadZ: public GLBoundingQuad{
public:
GLBoundingQuadZ();
virtual ~GLBoundingQuadZ();

private:
};

