#include "OpenGLWidget.h"
#include <QOpenGLTexture>
#include <QOpenGLShaderProgram>
#include "opengl-base/ShaderDescriptor.h"
#include "mocca/log/LogManager.h"


#include <iostream>

using namespace rendering;
using namespace std;

#define PROGRAM_VERTEX_ATTRIBUTE 0
#define PROGRAM_TEXCOORD_ATTRIBUTE 1

OpenGLWidget::OpenGLWidget(QWidget* parent) :
QOpenGLWidget(parent),
  _lastPosition(0,0),
  _movementScale(0.4f),
  _cubeGeometry(nullptr),
  _cubeShader(nullptr),
  _camera(nullptr),
  _tip0(1,0,0),
  _tip1(1,0,0),
  _tip2(1,0,0)
{
    std::printf("opengl works");
}

void OpenGLWidget::mousePressEvent( QMouseEvent* ev )
{
    QPoint p = ev->pos();
    _lastPosition.x = p.rx();
    _lastPosition.y = p.ry();
}

void OpenGLWidget::mouseMoveEvent(QMouseEvent *ev)
{
    QPoint p = ev->pos();
    Core::Math::Vec2f delta;
    delta.x = (float)(p.rx()-_lastPosition.x)*_movementScale;
    delta.y = (float)(p.ry()-_lastPosition.y)*_movementScale;
    _lastPosition.x = p.rx();
    _lastPosition.y = p.ry();

    _camera->rotateCamera(Core::Math::Vec3f(-delta.y,-delta.x,0));
    update();
}

void OpenGLWidget::mouseReleaseEvent( QMouseEvent* ev )
{
    QPoint p = ev->pos();
    _lastPosition.x = p.rx();
    _lastPosition.y = p.ry();
}

void OpenGLWidget::wheelEvent(QWheelEvent* event)
{

}

Core::Math::Vec2ui OpenGLWidget::getWidgetResolution(){
    Core::Math::Vec2ui res;
    res.x = this->width()*devicePixelRatio();
    res.y = this->height()*devicePixelRatio();
    return res;
}

OpenGLWidget::~OpenGLWidget() {
}

void OpenGLWidget::createShaderProgram() {

    std::vector<std::string> fs,vs;
    vs.push_back("/Users/waschkbaer/NeuroVis/shader/TaskVertex.glsl");
    fs.push_back("/Users/waschkbaer/NeuroVis/shader/TaskFragment.glsl");
    ShaderDescriptor sd(vs,fs);
    if(!loadAndCheckShaders(_cubeShader,sd)) return;

}

void OpenGLWidget::createVBO() {
}

void OpenGLWidget::createUtils() {
    _camera = std::unique_ptr<utils::Camera>(new utils::Camera(Core::Math::Vec3f(0,0,15),
                                                               Core::Math::Vec3f(0,0,0),
                                                               Core::Math::Vec3f(0,1,0)));
    //_projectionMatrix.Perspective(45, (float)this->width()*devicePixelRatio()/(float)this->height()*devicePixelRatio(),0.01f, 1000.0f);
    _projectionMatrix.Ortho(-10,10,-10,10,0.01f,1000.0f);
    //_viewMatrix = _camera->buildLookAt();
    _viewMatrix.BuildLookAt(Core::Math::Vec3f(0,0,10),Core::Math::Vec3f(0,0,0),Core::Math::Vec3f(0,1,0));

    updateCube0(Core::Math::Vec3f(0,0,0));
    updateCube1(Core::Math::Vec3f(0,0,0));
    updateCube2(Core::Math::Vec3f(0,0,0));
}

void OpenGLWidget::createGeometry() {
    _cubeGeometry = std::unique_ptr<GLVolumeBox>(new GLVolumeBox(Core::Math::Vec3f(-1.0f,-1.0f,-1.0f),
                                                                   Core::Math::Vec3f(1.0f,1.0f,1.0f)));

    float Points[6]{
        -1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f
    };

    int Indices[2]{
        0,1
    };
    _lineGeometry = std::unique_ptr<GLModel>(new GLModel());
    _lineGeometry->Initialize(Points,Indices,2,2);
}

void OpenGLWidget::initializeGL() {
  makeCurrent();
  initializeOpenGLFunctions();

  const char* r = (const char*)glGetString(GL_RENDERER);
  const char* v = (const char*)glGetString(GL_VERSION);

  LINFO(r);
  LINFO(v);

  //createVBO();
  createShaderProgram();
  createGeometry();
  createUtils();
}

void OpenGLWidget::paintGL() {

    //_tip1.normalize();
    //_tip2.normalize();

    //float c = _tip1 ^ _tip2;
    //float d = 90.0-c*180.0f/M_PI;

    float angle = 2 * std::atan((_tip1*_tip2.length() - _tip1.length()*_tip2).length() / (_tip1 * (_tip2.length()) + (_tip1 * _tip2).length()).length());
    angle = 180.0/M_PI*angle;
    //LINFO(angle);

    Core::Math::Vec3f camPos = _tip1 % _tip2;

    //_viewMatrix.BuildLookAt(camPos*10.0f,Core::Math::Vec3f(0,0,0),Core::Math::Vec3f(0,1,0));
    //_camera->setPosition(camPos);

    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);

    glClearColor(0.3f,0.7f,0.5f,1);
    glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

    _viewMatrix = _camera->buildLookAt();

    _cubeShader->Enable();
    _cubeShader->Set("projectionMatrix",_projectionMatrix);
    _cubeShader->Set("viewMatrix",_viewMatrix);

    _cubeShader->Set("worldMatrix",_worldMatrixCube0);
    //_cubeGeometry->paint();

    _cubeShader->Set("worldMatrix",_worldMatrixCube1);
    _cubeGeometry->paint();
    _lineGeometry->paint(GL_LINES);

    _cubeShader->Set("worldMatrix",_worldMatrixCube2);
    _cubeGeometry->paint();
    _lineGeometry->paint(GL_LINES);

    _cubeShader->Disable();

    glFinish();
}

bool OpenGLWidget::loadAndCheckShaders(std::shared_ptr<GLProgram>& programPtr, ShaderDescriptor& sd){
    programPtr = std::make_shared<GLProgram>();
    programPtr->Load(sd);

    if (!programPtr->IsValid()){
        LERROR("TaskRenderer | programm not valid");
        return false;
    }
    return true;
}

//handrücken
void OpenGLWidget::updateCube0(Core::Math::Vec3f rotation){
    Core::Math::Mat4f scale,translation,rotationX,rotationY,rotationZ;

    scale.Scaling(1.0f,1.0f,1.0f);
    translation.Translation(0,0,0);
    rotationX.RotationX(-rotation.x * M_PI/180.0f);
    rotationY.RotationY(-rotation.y * M_PI/180.0f);
    rotationZ.RotationZ(-rotation.z * M_PI/180.0f);

    _worldMatrixCube0 = scale*translation*rotationX*rotationY*rotationZ;
    update();
}

//zeigefinger
void OpenGLWidget::updateCube1(Core::Math::Vec3f rotation)
{
    Core::Math::Mat4f   scale,translation,translation2,
                        rotationX,rotationY,rotationZ,
                        rotationZBase;
    Core::Math::Vec4f   corner(5.0f,0,0,1.0f);

    //yaw pitch roll

    scale.Scaling(2.5f,0.2f,1.0f);
    translation.Translation(2.5f,0.0f,0);
    translation2.Translation(0.0f,1.0f,0);
    rotationX.RotationX(0);
    rotationY.RotationY(0);
    rotationZ.RotationZ(-rotation.z * M_PI/180.0f);
    rotationZBase.RotationZ(-10 * M_PI/180.0f);

    _worldMatrixCube1 = scale*translation*rotationZBase*rotationX*rotationY*rotationZ*translation2;

    corner = corner*rotationZ;


    _tip1 = corner.xyz();
    update();
}

//daumen
void OpenGLWidget::updateCube2(Core::Math::Vec3f rotation)
{
    Core::Math::Mat4f scale,translation,rotationX,rotationY,rotationZ;
    Core::Math::Vec4f corner(3.0f,0,0,1.0f);

    scale.Scaling(1.5f,0.2f,1.0f);
    translation.Translation(1.5f,0,0);
    rotationX.RotationX(0);
    rotationY.RotationY(0);

    rotationZ.RotationZ(-rotation.z * M_PI/180.0f);

    _worldMatrixCube2 = scale*translation*rotationZ;
    corner = corner*rotationZ;
    _tip2 = corner.xyz();
    update();
}
