#ifndef TASKWIDGET_H
#define TASKWIDGET_H

#include <QWidget>
#include <QTimer>

#include "utils/UDPSocket.h"
#include "qcustomplot.h"
#include "silverbullet/time/Timer.h"
#include "silverbullet/math/Vectors.h"

#include <iostream>
#include <fstream>
#include <qcustomplot.h>

namespace Ui {
class taskWidget;
}

class taskWidget : public QWidget
{
    Q_OBJECT

    struct fftData {
        fftData():_vX(),_vY(),_vZ(){};
        std::vector<double>         _vX;
        std::vector<double>         _vY;
        std::vector<double>         _vZ;
        std::vector<double>          _time;
    };

public:
    explicit taskWidget(QWidget *parent = 0);
    ~taskWidget();

private:
    void setupPlots();

    void startFile(std::string append = "");
    void endFile();
    void writeLine(double time, std::vector<float>& data);
    void write( double time,
                Core::Math::Vec3f a, Core::Math::Vec3f g, Core::Math::Vec3f r,
                bool endline = false);

    void plot(int imu, double time, Core::Math::Vec3f rot,
              Core::Math::Vec3f acl, Core::Math::Vec3f gyr,
              Core::Math::Vec4f qua, QCustomPlot* aclPlot,
              QCustomPlot* gyrPlot, QCustomPlot* rotPlot,
              QCustomPlot* fftPlot, Core::Math::Vec3d baseRotation);

    void handleDataPackage();

private slots:

    void on_init_clicked();

    void on_start_clicked();

    void realtimeDataSlot();
    void realtimePlotSlot();

    void on_loadButton_clicked();

    void on_ac0s_sliderMoved(int position);

    void on_ac1s_sliderMoved(int position);

    void on_ac2s_sliderMoved(int position);

    void on_gy0s_sliderMoved(int position);

    void on_gy1s_sliderMoved(int position);

    void on_gy2s_sliderMoved(int position);

    void on_ro0s_sliderMoved(int position);

    void on_ro1s_sliderMoved(int position);

    void on_ro2s_sliderMoved(int position);

    void on_playButton_clicked();

    void on_persistance_valueChanged(int value);

    void on_timeSlider_valueChanged(int value);

    void on_windowSlider_sliderMoved(int position);

    void on_windowPosition_sliderMoved(int position);

private:
    fftData createFFT(fftData data, int samples = 1000);

    void updateBaseRotation(int imu, Core::Math::Vec3f rot);
    void handleBaseRotation();
    void handleRecord(int imu, double time, Core::Math::Vec3f rot);
    void openFile(std::string filename);

    void setPlotRange(int time, int range = 1500);
    void updateSliderAndRange(int position);

    void displayRotation(int imu, Core::Math::Vec3f rot);
    void displayRotation(Core::Math::Vec3f rotFore, Core::Math::Vec3f rotThumb);

    void persitanceUpdate(float value, int startTime, int endTime);

    std::string buildMetaString();

    int calculatePersitanceMaxIndex(int min, int length);
    void updatePersistanceSettings();

private:
    QTimer dataTimer;
    QTimer plotTimer;
    Ui::taskWidget *ui;
    std::unique_ptr<utils::UDPSocket> _socket;

    std::vector<QCustomPlot*>   _plots;
    std::vector<float>          _data;
    Core::Time::Timer           _timer;
    bool                        _isCountdown;

    Core::Math::Vec3d           _normalAc1;
    Core::Math::Vec3d           _normalAc2;
    Core::Math::Vec3d           _normalAc3;

    Core::Math::Vec3d           _rotationAc1;
    Core::Math::Vec3d           _rotationAc2;
    Core::Math::Vec3d           _rotationAc3;
    Core::Math::Vec3d           _rotConfigCounter;
    Core::Math::Vec2d           _plotrange;

    std::vector<fftData>         _vFFTSamples;

    std::vector<float>           _vTime;
    std::vector<float>           _vRot0Z;
    std::vector<float>           _vRot1Z;
    std::vector<float>           _vRot2Z;
    std::vector<float>           _vTapAngle;
    std::vector<Core::Math::Vec3f> _vTapRotFore;
    std::vector<Core::Math::Vec3f> _vTapRotThumb;

    std::vector<QCPItemLine*>    _vMinMaxLines;
    QCPItemLine*                 _visLine;

    bool                        _record;
    bool                        _updateBaseRotation;
    FILE*                       _pFile;

    int                         _numberofTaps;
    float                       _avgFreq;
    float                       _avgAmp;
    float                       _globalMinAmp;
    float                       _globalMaxAmp;

    std::vector<std::shared_ptr<QCPItemLine>>   _lineList;
    Core::Time::Timer                           _replayTimer;
    int                                         _startTime;
    bool                                        _replay;
};

#endif // TASKWIDGET_H
