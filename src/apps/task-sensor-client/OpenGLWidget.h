#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QOpenGLBuffer>
#include <QMouseEvent>
#include <QWheelEvent>

#include <vector>
#include <cstdint>

#include "rendering/RenderMutex.h"
#include "rendering/IRenderer.h"
#include "silverbullet/math/Vectors.h"
#include <QOpenGLFunctions_3_3_Core>

#include "opengl-base/GLBoundingBox.h"
#include "opengl-base/GLProgram.h"
#include "opengl-base/GLVolumeBox.h"

#include "utils/Camera.h"

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram);
QT_FORWARD_DECLARE_CLASS(QOpenGLTexture)

class OpenGLWidget : public QOpenGLWidget, protected QOpenGLFunctions {
public:
    OpenGLWidget(QWidget* parent = 0);
    ~OpenGLWidget();

    void initializeGL();
    void paintGL();

    void mousePressEvent( QMouseEvent* ev );
    void mouseReleaseEvent( QMouseEvent* ev );
    void mouseMoveEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent* event);

    Core::Math::Vec2ui getWidgetResolution();

    void updateCube0(Core::Math::Vec3f rotation);
    void updateCube1(Core::Math::Vec3f rotation);
    void updateCube2(Core::Math::Vec3f rotation);
private:
  void createVBO();
  void createShaderProgram();
  void createGeometry();
  void createUtils();

  bool loadAndCheckShaders(std::shared_ptr<GLProgram>& programPtr,
                           ShaderDescriptor& sd);

  Core::Math::Vec2i           _lastPosition;
  float                       _movementScale;

  //Rendering Component
  std::unique_ptr<GLVolumeBox>  _cubeGeometry;
  std::unique_ptr<GLModel>      _lineGeometry;
  std::shared_ptr<GLProgram>    _cubeShader;

  std::unique_ptr<utils::Camera> _camera;

  Core::Math::Mat4f                 _worldMatrixCube1;
  Core::Math::Mat4f                 _worldMatrixCube2;
  Core::Math::Mat4f                 _worldMatrixCube0;

  Core::Math::Vec3f                 _tip0;
  Core::Math::Vec3f                 _tip1;
  Core::Math::Vec3f                 _tip2;

  Core::Math::Mat4f                 _projectionMatrix;
  Core::Math::Mat4f                 _viewMatrix;
};

#endif // OPENGLWIDGET_H
