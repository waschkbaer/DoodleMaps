#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>

#include <QTimer>

#include "qcustomplot.h"

#include "neuro-io/ISensorClient.h"

#include "mocca/base/Memory.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
  void realtimeDataSlot();

private:
    Ui::MainWindow *ui;

    std::vector<QCustomPlot*> _analogPlots;
    std::vector<QCustomPlot*> _fftPlots;

    QCustomPlot* test;

    QTimer dataTimer;

    std::unique_ptr<neuroio::ISensorClient<int>>  _sensor;
    uint64_t                        _lastPlottedIndex;
};

#endif // MAINWINDOW_H
