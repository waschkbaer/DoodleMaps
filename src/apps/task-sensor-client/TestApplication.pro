#-------------------------------------------------
#
# Project created by QtCreator 2016-08-25T10:44:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = TestApplication
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    merwidget.cpp \
    merrecordings.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    merwidget.h \
    merrecordings.h

FORMS    += mainwindow.ui \
    merwidget.ui \
    merrecordings.ui
