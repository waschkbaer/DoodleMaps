#include <QApplication>
#include "mainwindow.h"


#include "mocca/base/Memory.h"
#include "mocca/log/ConsoleLog.h"
#include "mocca/log/HTMLLog.h"
#include "mocca/log/LogManager.h"

void initLogger() {
    using mocca::LogManager;
    LogManager::initialize(LogManager::LogLevel::Debug, true);
    auto console = new mocca::ConsoleLog();
    LogMgr.addLog(console);
}


int main(int argc, char *argv[])
{
    QSurfaceFormat format;

    format.setMajorVersion( 3 ); //whatever version
    format.setMinorVersion( 3 ); //
    format.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(format);

    initLogger();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
