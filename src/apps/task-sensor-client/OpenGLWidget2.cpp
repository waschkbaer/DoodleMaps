#include "OpenGLWidget2.h"
#include <QOpenGLTexture>
#include <QOpenGLShaderProgram>
#include "opengl-base/ShaderDescriptor.h"
#include "mocca/log/LogManager.h"


#include <iostream>

using namespace rendering;
using namespace std;

#define PROGRAM_VERTEX_ATTRIBUTE 0
#define PROGRAM_TEXCOORD_ATTRIBUTE 1

OpenGLWidget2::OpenGLWidget2(QWidget* parent) :
QOpenGLWidget(parent),
  _lastPosition(0,0),
  _movementScale(0.4f),
  _cubeGeometry(nullptr),
  _cubeShader(nullptr),
  _camera(nullptr),
  _targetBinder(),
  _offbuffer(nullptr),
  _offbuffer2(nullptr),
  _renderPlane(nullptr),
  _continueAdding(true),
  _shouldreset(true),
  _colorMapBuffer(nullptr),
  _tip0(1,0,0),
  _tip1(1,0,0),
  _tip2(1,0,0),
  _deltaTime(0)
{
    std::printf("opengl works");
}

void OpenGLWidget2::mousePressEvent( QMouseEvent* ev )
{
    QPoint p = ev->pos();
    _lastPosition.x = p.rx();
    _lastPosition.y = p.ry();
}

void OpenGLWidget2::mouseMoveEvent(QMouseEvent *ev)
{
    QPoint p = ev->pos();
    Core::Math::Vec2f delta;
    delta.x = (float)(p.rx()-_lastPosition.x)*_movementScale;
    delta.y = (float)(p.ry()-_lastPosition.y)*_movementScale;
    _lastPosition.x = p.rx();
    _lastPosition.y = p.ry();

    _camera->rotateCamera(Core::Math::Vec3f(-delta.x,-delta.y,0));
    update();
}

void OpenGLWidget2::mouseReleaseEvent( QMouseEvent* ev )
{
    QPoint p = ev->pos();
    _lastPosition.x = p.rx();
    _lastPosition.y = p.ry();
}

void OpenGLWidget2::wheelEvent(QWheelEvent* event)
{

}

Core::Math::Vec2ui OpenGLWidget2::getWidgetResolution(){
    Core::Math::Vec2ui res;
    res.x = this->width()*devicePixelRatio();
    res.y = this->height()*devicePixelRatio();
    return res;
}

OpenGLWidget2::~OpenGLWidget2() {
}

void OpenGLWidget2::createShaderProgram() {

    std::vector<std::string> fs,vs;
    vs.push_back("/Users/waschkbaer/NeuroVis/shader/TaskVertex.glsl");
    fs.push_back("/Users/waschkbaer/NeuroVis/shader/TaskFragment.glsl");
    ShaderDescriptor sd(vs,fs);
    if(!loadAndCheckShaders(_cubeShader,sd)) return;

    vs.clear();
    fs.clear();
    vs.push_back("/Users/waschkbaer/NeuroVis/shader/TaskComposeVertex.glsl");
    fs.push_back("/Users/waschkbaer/NeuroVis/shader/TaskComposeFragment.glsl");
    sd = ShaderDescriptor(vs,fs);
    if(!loadAndCheckShaders(_CompositingShader,sd)) return;

    vs.clear();
    fs.clear();
    vs.push_back("/Users/waschkbaer/NeuroVis/shader/TaskMixVertex.glsl");
    fs.push_back("/Users/waschkbaer/NeuroVis/shader/TaskMixFragment.glsl");
    sd = ShaderDescriptor(vs,fs);
    if(!loadAndCheckShaders(_taskMixShader,sd)) return;

}

void OpenGLWidget2::createVBO() {

    std::shared_ptr<GLFBO> _fbo = std::make_shared<GLFBO>();

    _offbuffer = std::make_shared<GLRenderTexture>(_fbo, GL_LINEAR, GL_LINEAR,
                                                        GL_CLAMP_TO_EDGE,
                                                        getWidgetResolution().x,
                                                        getWidgetResolution().y,
                                                        GL_RGBA, GL_RGBA,
                                                        GL_FLOAT, true, 1);

    _offbuffer2 = std::make_shared<GLRenderTexture>(_fbo, GL_LINEAR, GL_LINEAR,
                                                        GL_CLAMP_TO_EDGE,
                                                        getWidgetResolution().x,
                                                        getWidgetResolution().y,
                                                        GL_RGBA, GL_RGBA,
                                                        GL_FLOAT, true, 1);

    _offbuffer3 = std::make_shared<GLRenderTexture>(_fbo, GL_LINEAR, GL_LINEAR,
                                                        GL_CLAMP_TO_EDGE,
                                                        getWidgetResolution().x,
                                                        getWidgetResolution().y,
                                                        GL_RGBA, GL_RGBA,
                                                        GL_FLOAT, true, 1);
    reset();
}

void OpenGLWidget2::createUtils() {
    _camera = std::unique_ptr<utils::Camera>(new utils::Camera(Core::Math::Vec3f(0,0,10),
                                                               Core::Math::Vec3f(0,0,0),
                                                               Core::Math::Vec3f(0,1,0)));
    //_projectionMatrix.Perspective(45, (float)this->width()*devicePixelRatio()/(float)this->height()*devicePixelRatio(),0.01f, 1000.0f);
    _projectionMatrix.Ortho(-6,6,-3,9,0.01f,1000.0f);
    _viewMatrix = _camera->buildLookAt();

    updateCube0(Core::Math::Vec3f(0,0,0));
    updateCube1(Core::Math::Vec3f(0,0,0));
    updateCube2(Core::Math::Vec3f(0,0,0));
}

void OpenGLWidget2::createGeometry() {
    _cubeGeometry = std::unique_ptr<GLVolumeBox>(new GLVolumeBox(Core::Math::Vec3f(-1.0f,-1.0f,-1.0f),
                                                                  Core::Math::Vec3f(1.0f,1.0f,1.0f)));

    _renderPlane = std::unique_ptr<GLRenderPlane>(new GLRenderPlane());

    float Points[6]{
        -1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f
    };

    int Indices[2]{
        0,1
    };
    _lineGeometry = std::unique_ptr<GLModel>(new GLModel());
    _lineGeometry->Initialize(Points,Indices,2,2);
}

void OpenGLWidget2::initializeGL() {
  makeCurrent();
  initializeOpenGLFunctions();

  const char* r = (const char*)glGetString(GL_RENDERER);
  const char* v = (const char*)glGetString(GL_VERSION);

  LINFO(r);
  LINFO(v);

  createVBO();
  createShaderProgram();
  createGeometry();
  createUtils();
}

bool d = true;
void OpenGLWidget2::paintGL() {
    glGetIntegerv( GL_FRAMEBUFFER_BINDING, &_displayTargetID );
    glBindFramebuffer(GL_FRAMEBUFFER, _displayTargetID);

    if(_vDisplayedRotationsFore.size() != 0){
        //clear old buffers
        _targetBinder.Bind(_offbuffer);
        glClearColor(0.0f,0.0f,0.0f,1);
        glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

        _targetBinder.Bind(_offbuffer2);
        glClearColor(0.0f,0.0f,0.0f,1);
        glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

        _targetBinder.Bind(_offbuffer3);
        glClearColor(0.0f,0.0f,0.0f,1);
        glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

        glBindFramebuffer(GL_FRAMEBUFFER, _displayTargetID);

        //paint colormap
        paintColorMap();

        paintCurrentRotation();
        paintCompositing();
    }else{
        paintCurrentRotation();
        paintCompositing();
    }
}

void OpenGLWidget2::paintCompositing(){
    glBindFramebuffer(GL_FRAMEBUFFER, _displayTargetID);

    glClearColor(0.0f,1.0f,0.0f,1);
    glCullFace(GL_BACK);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    _CompositingShader->Enable();

    if(d){
        _CompositingShader->SetTexture2D("composeImage",_offbuffer2->GetTextureHandle(),0);
    }else{
        _CompositingShader->SetTexture2D("composeImage",_offbuffer3->GetTextureHandle(),0);
    }

    _CompositingShader->SetTexture2D("currentImage",_offbuffer->GetTextureHandle(),1);

    _renderPlane->paint();

    _CompositingShader->Disable();

    glFinish();
}

void OpenGLWidget2::paintCurrentRotation(){
    _targetBinder.Bind(_offbuffer);
    glClearColor(0.0f,0.0f,0.0f,1);
    glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

    _targetBinder.Bind(_offbuffer);

    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);

    glClearColor(0.0f,0.0f,0.0f,1);
    glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

    _viewMatrix = _camera->buildLookAt();

    _cubeShader->Enable();
    _cubeShader->Set("projectionMatrix",_projectionMatrix);
    _cubeShader->Set("viewMatrix",_viewMatrix);

    _cubeShader->Set("worldMatrix",_worldMatrixCube1);
    _lineGeometry->paint(GL_LINES);

    _cubeShader->Set("worldMatrix",_worldMatrixCube2);
    _lineGeometry->paint(GL_LINES);

    _cubeShader->Disable();
    _targetBinder.Unbind();
}

void OpenGLWidget2::paintColorMap(){
    while(_vDisplayedRotationsFore.size() != 0){
        paintColorFinger(1,_vDisplayedRotationsFore[0],_vDisplayedColorFore[0]);
        _vDisplayedRotationsFore.erase(_vDisplayedRotationsFore.begin());
        _vDisplayedColorFore.erase(_vDisplayedColorFore.begin());
    }

    while(_vDisplayedRotationsThumb.size() != 0){
        paintColorFinger(2,_vDisplayedRotationsThumb[0],_vDisplayedColorThumb[0]);
        _vDisplayedRotationsThumb.erase(_vDisplayedRotationsThumb.begin());
        _vDisplayedColorThumb.erase(_vDisplayedColorThumb.begin());
    }
}

void OpenGLWidget2::paintColorFinger(int finger, Core::Math::Vec3f rotation, Core::Math::Vec3f color){
    glGetIntegerv( GL_FRAMEBUFFER_BINDING, &_displayTargetID );
    glBindFramebuffer(GL_FRAMEBUFFER, _displayTargetID);

    //draw current finger position in an offscreen buffer -----------------------------
    _targetBinder.Bind(_offbuffer);

    glCullFace(GL_BACK);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);

    glClearColor(0.0f,0.0f,0.0f,1);
    glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

    _viewMatrix = _camera->buildLookAt();

    _cubeShader->Enable();
    _cubeShader->Set("projectionMatrix",_projectionMatrix);
    _cubeShader->Set("viewMatrix",_viewMatrix);

    // try some fake line
    if(finger == 1){
        Core::Math::Mat4f   scale,translation,translation2,
                            rotationZ,rotationZBase,world;
        float scaleLine = 0.5f;

        scale.Scaling(2.6f+color.z*scaleLine,0.2f,1.0f);
        translation.Translation(2.7f+color.z*scaleLine,0.0f,0);
        translation2.Translation(0.0f,1.0f,0);
        rotationZ.RotationZ(-rotation.z * M_PI/180.0f);
        rotationZBase.RotationZ(-10 * M_PI/180.0f);

        world = scale*translation*rotationZBase*rotationZ*translation2;

        _cubeShader->Set("color",Core::Math::Vec3f(1,1,1));
        _cubeShader->Set("worldMatrix",world);
        _lineGeometry->paint(GL_LINES);

        scaleLine -= 0.001f;

        scale.Scaling(2.6f+color.z*scaleLine,0.2f,1.0f);
        translation.Translation(2.6f+color.z*scaleLine,0.0f,0);
        translation2.Translation(0.0f,1.0f,0);
        rotationZ.RotationZ(-rotation.z * M_PI/180.0f);
        rotationZBase.RotationZ(-10 * M_PI/180.0f);

        world = scale*translation*rotationZBase*rotationZ*translation2;

        _cubeShader->Set("color",Core::Math::Vec3f(0,0,0));
        _cubeShader->Set("worldMatrix",world);
        _lineGeometry->paint(GL_LINES);
    }
    // try some fake line

    switch(finger){
        case 0:     updateCube0(rotation);
                    _cubeShader->Set("worldMatrix",_worldMatrixCube0);
                    break;
        case 1:     updateCube1(rotation);
                    _cubeShader->Set("worldMatrix",_worldMatrixCube1);
                    break;
        case 2:     updateCube2(rotation);
                    _cubeShader->Set("worldMatrix",_worldMatrixCube2);
                    break;
    }
    _cubeShader->Set("color",Core::Math::Vec3f(1,1,1));
    _lineGeometry->paint(GL_LINES);

    _cubeShader->Disable();
    _targetBinder.Unbind();

    //draw the compositing -----------------------------------------------------------
    if(d){
        _targetBinder.Bind(_offbuffer2);
    }else{
        _targetBinder.Bind(_offbuffer3);
    }
    glClearColor(0.0f,0.0f,0.0f,1);
    glCullFace(GL_BACK);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    _taskMixShader->Enable();

    _taskMixShader->SetTexture2D("geometry",_offbuffer->GetTextureHandle(),0);
    _taskMixShader->Set("deltaTime",_deltaTime);

    _taskMixShader->Set("color",color);

    if(d)
        _taskMixShader->SetTexture2D("mix",_offbuffer3->GetTextureHandle(),1);
    else
        _taskMixShader->SetTexture2D("mix",_offbuffer2->GetTextureHandle(),1);

    _renderPlane->paint();

    _taskMixShader->Disable();
    _targetBinder.Unbind();

    d = !d;

    glFinish();
}


bool OpenGLWidget2::loadAndCheckShaders(std::shared_ptr<GLProgram>& programPtr, ShaderDescriptor& sd){
    programPtr = std::make_shared<GLProgram>();
    programPtr->Load(sd);

    if (!programPtr->IsValid()){
        LERROR("TaskRenderer | programm not valid");
        return false;
    }
    return true;
}

//handrücken
void OpenGLWidget2::updateCube0(Core::Math::Vec3f rotation){
    Core::Math::Mat4f scale,translation,rotationX,rotationY,rotationZ;

    scale.Scaling(1.0f,1.0f,1.0f);
    translation.Translation(0,0,0);
    rotationX.RotationX(-rotation.x * M_PI/180.0f);
    rotationY.RotationY(-rotation.y * M_PI/180.0f);
    rotationZ.RotationZ(-rotation.z * M_PI/180.0f);

    _worldMatrixCube0 = scale*translation*rotationX*rotationY*rotationZ;
    update();
}

//zeigefinger
void OpenGLWidget2::updateCube1(Core::Math::Vec3f rotation){
    Core::Math::Mat4f   scale,translation,translation2,
                        rotationX,rotationY,rotationZ,
                        rotationZBase;
    Core::Math::Vec4f   corner(5.0f,0,0,1.0f);

    //yaw pitch roll

    scale.Scaling(2.5f,0.2f,1.0f);
    translation.Translation(2.5f,0.0f,0);
    translation2.Translation(0.0f,1.0f,0);
    rotationX.RotationX(0);
    rotationY.RotationY(0);
    rotationZ.RotationZ(-rotation.z * M_PI/180.0f);
    rotationZBase.RotationZ(-10 * M_PI/180.0f);

    _worldMatrixCube1 = scale*translation*rotationZBase*rotationX*rotationY*rotationZ*translation2;
    corner = corner*rotationX*rotationY*rotationZ;
    _tip1 = corner.xyz();
    update();
}

//daumen
void OpenGLWidget2::updateCube2(Core::Math::Vec3f rotation){
    Core::Math::Mat4f scale,translation,rotationX,rotationY,rotationZ;
    Core::Math::Vec4f corner(3.0f,0,0,1.0f);

    scale.Scaling(1.5f,0.2f,1.0f);
    translation.Translation(1.5f,0,0);
    rotationX.RotationX(0);
    rotationY.RotationY(0);

    rotationZ.RotationZ(-rotation.z * M_PI/180.0f);

    _worldMatrixCube2 = scale*translation*rotationX*rotationY*rotationZ;
    corner = corner*rotationX*rotationY*rotationZ;
    _tip2 = corner.xyz();
    update();
}

void OpenGLWidget2::reset() {
    _continueAdding = true;
    _shouldreset = true;
}
void OpenGLWidget2::stop() {
    _continueAdding = false;
}

void OpenGLWidget2::updateDeltaTime(float time) {
    _deltaTime = time;
    update();
}

void OpenGLWidget2::addRotation(Core::Math::Vec3f r){
    _vDisplayedRotationsFore.push_back(r);
}

void OpenGLWidget2::clearRotation(){
    _vDisplayedRotationsFore.clear();
}

void OpenGLWidget2::addColor(Core::Math::Vec3f r){
    _vDisplayedColorFore.push_back(r);
}

void OpenGLWidget2::clearColor(){
    _vDisplayedColorFore.clear();
}


void OpenGLWidget2::addRotationThumb(Core::Math::Vec3f r){
    _vDisplayedRotationsThumb.push_back(r);
}

void OpenGLWidget2::clearRotationThumb(){
    _vDisplayedRotationsThumb.clear();
}

void OpenGLWidget2::addColorThumb(Core::Math::Vec3f r){
    _vDisplayedColorThumb.push_back(r);
}

void OpenGLWidget2::clearColorThumb(){
    _vDisplayedColorThumb.clear();
}

