#include "taskwidget.h"
#include "ui_taskwidget.h"
#include "mocca/base/Memory.h"

#include <QLabel>

#include "mocca/base/Memory.h"
#include "mocca/log/ConsoleLog.h"
#include "mocca/log/HTMLLog.h"
#include "mocca/log/LogManager.h"

#include "neuro-processing/FFTProcessing.h"
#include "mocca/base/StringTools.h"

#include "utils/persistence1d.h"

#define M_PI 3.14159265359

taskWidget::taskWidget(QWidget *parent) :
    QWidget(parent),
    _socket(nullptr),
    _record(false),
    _data(),
    _timer(),
    _pFile(NULL),
    _isCountdown(false),
    _normalAc1(0,0,1),
    _normalAc2(0,0,1),
    _normalAc3(0,0,1),
    _rotationAc1(0,0,0),
    _rotationAc2(0,0,0),
    _rotationAc3(0,0,0),
    _rotConfigCounter(0,0,0),
    _updateBaseRotation(false),
    _plotrange(0,3000),
    _replayTimer(),
    _startTime(0),
    _replay(false),
    _avgAmp(0),
    _avgFreq(0),
    _numberofTaps(0),
    ui(new Ui::taskWidget)
{
    ui->setupUi(this);
    _socket = std::unique_ptr<utils::UDPSocket>(new utils::UDPSocket());


    _socket->bindPort(9871);
    _socket->start();

    setupPlots();
    _data.resize(19*15);

    _timer.start();

    ui->TypeSelection->addItem("Rechts - Htrem", QVariant("Rechts - Htrem"));
    ui->TypeSelection->addItem("Rechts - Itrem", QVariant("Rechts - Itrem"));
    ui->TypeSelection->addItem("Rechts - Diad", QVariant("Rechts - Diad"));
    ui->TypeSelection->addItem("Rechts - Tapp", QVariant("Rechts - Tapp"));

    ui->TypeSelection->addItem("----------------");

    ui->TypeSelection->addItem("Links  - Htrem", QVariant("Links  - Htrem"));
    ui->TypeSelection->addItem("Links  - Itrem", QVariant("Links  - Itrem"));
    ui->TypeSelection->addItem("Links  - Diad", QVariant("Links  - Diad"));
    ui->TypeSelection->addItem("Links  - Tapp", QVariant("Links  - Tapp"));

    // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
    connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
    dataTimer.start(0);


    ui->timerLabel->setStyleSheet("QLabel { background-color : white; color : black; }");
    ui->timerLabel->setText(QString("Not Recording"));
}

taskWidget::~taskWidget()
{
    endFile();
    delete ui;
}
float lastTime = 0;

const int NUMBEROFSENSORS = 3;
const int NUMBEROFELEMENTSPERSAMPLE = 12;
const int SENSOROFFSET = NUMBEROFSENSORS*NUMBEROFELEMENTSPERSAMPLE;
const int NUMBEROFBUFFEREDSAMPLES = 8;
const int DATAPACKAGESIZE =     NUMBEROFSENSORS*
                                NUMBEROFELEMENTSPERSAMPLE*
                                NUMBEROFBUFFEREDSAMPLES+1;
float last = 0;
void taskWidget::realtimeDataSlot(){
    std::unique_ptr<mocca::ByteArray> dataPackage = _socket->getNext();

    float c = 0;

    if(dataPackage != nullptr && dataPackage->size() == DATAPACKAGESIZE*sizeof(float)){
        if(_data.size() != DATAPACKAGESIZE-1)
            _data.resize(DATAPACKAGESIZE-1);

        //check for lost package!
        std::memcpy(&c,dataPackage->data()+(DATAPACKAGESIZE-1)*sizeof(float),sizeof(float));
        if(last != 0 && c-last != 1){
            LINFO("lost packages: "<< (c-last));
            last = c;
        }

        //copy data from package to buffer!
        std::memcpy(&_data.at(0),dataPackage->data(),(DATAPACKAGESIZE-1)*sizeof(float));

        handleDataPackage();
    }else{
        //handle realtime slot without live data transfer
        if(_replay){
            int time = _replayTimer.elapsed()+_startTime;
            if(time > ui->ac0s->minimum() && time < ui->ac0s->maximum()){
                updateSliderAndRange(time);
            }else{
                on_playButton_clicked();
            }
        }

    }
}

void taskWidget::handleDataPackage()
{
    int dataOffset = 0;
    double timerelapsed = 0;
    for(int k = 0; k < 6;k++){
        dataOffset = k*SENSOROFFSET;
        for(int i = 0; i < SENSOROFFSET;i += NUMBEROFELEMENTSPERSAMPLE){
            int imu         = _data[dataOffset+i+0];
            double time     = _data[dataOffset+i+1];

            Core::Math::Vec3f rot(  _data[dataOffset+i+2],
                                    _data[dataOffset+i+3],
                                    _data[dataOffset+i+4]);

            Core::Math::Vec3f acl(  _data[dataOffset+i+5],
                                    _data[dataOffset+i+6],
                                    _data[dataOffset+i+7]);

            Core::Math::Vec3f gyr(  _data[dataOffset+i+8],
                                    _data[dataOffset+i+9],
                                    _data[dataOffset+i+10]);

            Core::Math::Vec4f qua(  0,0,0,0);

            float hz        = _data[dataOffset+i+11];

            timerelapsed = _timer.elapsed();
            if(_record){
                if(timerelapsed < 3000)
                    ui->timerLabel->setText(QString(std::to_string((int)(_timer.elapsed())/1000.0).c_str()));
                else
                    ui->timerLabel->setText(QString(std::to_string((int)(13000-_timer.elapsed())/1000.0).c_str()));

                startFile();
                bool endLine = false;

                if(imu == 2) endLine = true;

                write(time,acl,gyr,rot,endLine);

            }else if(i == 0){
                ui->timerLabel->setText(QString("Not Recording"));
                endFile();
            }


            //record clicked, record 0,5 to 2,5s to calculate the rotation of the IMU
            if( _record &&
                timerelapsed > 500 &&
                timerelapsed < 2500 &&
                    _updateBaseRotation){

                updateBaseRotation(imu,rot);
                ui->timerLabel->setStyleSheet("QLabel { background-color : grey; color : black; }");
            }

            // if timer elapsed 3 seconds, calculate the base rotation
            if( _record &&
                _updateBaseRotation &&
                timerelapsed > 3000){

                handleBaseRotation();
                ui->timerLabel->setStyleSheet("QLabel { background-color : red; color : white; }");
            }

            //if recording handle the data and add it to fftw arrays
            if( _record &&
                !_updateBaseRotation &&
                timerelapsed > 3000 &&
                timerelapsed < 13000){

                handleRecord(imu, time,rot);

            //stop recording after 13 seconds, calculate the fft and store everything
            }else if(_record && !_updateBaseRotation && _vFFTSamples.size() > 0){

                neuroprocessing::FFTProcessing fftwProcessing;
                std::shared_ptr<std::vector<double>> v;
                std::vector<double> outputX;
                std::vector<double> outputY;
                std::vector<double> outputZ;

                for(int i = 0; i < 3;++i){

                    fftData input = createFFT(_vFFTSamples[i],1000);

                    v = std::make_shared<std::vector<double>>(input._vX);
                    fftwProcessing.setInputData(v,Core::Math::Vec2ui(0,input._time.size()));
                    fftwProcessing.execute();
                    outputX = fftwProcessing.getOutputData();

                    v = std::make_shared<std::vector<double>>(input._vY);
                    fftwProcessing.setInputData(v,Core::Math::Vec2ui(0,input._time.size()));
                    fftwProcessing.execute();
                    outputY = fftwProcessing.getOutputData();

                    v = std::make_shared<std::vector<double>>(input._vZ);
                    fftwProcessing.setInputData(v,Core::Math::Vec2ui(0,input._time.size()));
                    fftwProcessing.execute();
                    outputZ = fftwProcessing.getOutputData();

                    //plot fft
                    switch(i){
                    case 0 :
                        ui->fft0->graph(0)->clearData();
                        ui->fft0->graph(1)->clearData();
                        ui->fft0->graph(2)->clearData();

                        for(int k = 1; k < outputX.size();++k){
                            ui->fft0->graph(0)->addData(k,outputX[k]);
                            ui->fft0->graph(1)->addData(k,outputY[k]);
                            ui->fft0->graph(2)->addData(k,outputZ[k]);
                        }
                        break;
                    case 1 :
                        ui->fft1->graph(0)->clearData();
                        ui->fft1->graph(1)->clearData();
                        ui->fft1->graph(2)->clearData();

                        for(int k = 1; k < outputX.size();++k){
                            ui->fft1->graph(0)->addData(k,outputX[k]);
                            ui->fft1->graph(1)->addData(k,outputY[k]);
                            ui->fft1->graph(2)->addData(k,outputZ[k]);
                        }
                        break;
                    case 2 :
                        ui->fft2->graph(0)->clearData();
                        ui->fft2->graph(1)->clearData();
                        ui->fft2->graph(2)->clearData();

                        for(int k = 1; k < outputX.size();++k){
                            ui->fft2->graph(0)->addData(k,outputX[k]);
                            ui->fft2->graph(1)->addData(k,outputY[k]);
                            ui->fft2->graph(2)->addData(k,outputZ[k]);
                        }
                        break;
                    }

                    ui->fft1->xAxis->setRange(1,outputX.size());
                    ui->fft1->yAxis->setRange(0,40);
                    ui->fft1->replot();

                    ui->fft2->xAxis->setRange(1,outputX.size());
                    ui->fft2->yAxis->setRange(0,40);
                    ui->fft2->replot();

                    ui->fft0->xAxis->setRange(1,outputX.size());
                    ui->fft0->yAxis->setRange(0,40);
                    ui->fft0->replot();

                }
                _record = false;
                ui->timerLabel->setStyleSheet("QLabel { background-color : white; color : black; }");
                ui->openGLWidget2->stop();
            }

            switch(imu){
                case 0 : plot(imu,time,rot,acl,gyr,qua,ui->ac0,ui->gy0,ui->rot0,ui->fft0,_rotationAc1); break;
                case 1 : plot(imu,time,rot,acl,gyr,qua,ui->ac1,ui->gy1,ui->rot1,ui->fft1,_rotationAc2); break;
                case 2 : plot(imu,time,rot,acl,gyr,qua,ui->ac2,ui->gy2,ui->rot2,ui->fft2,_rotationAc3); break;
            }
        }
    }
    realtimePlotSlot();
}

void taskWidget::updateBaseRotation(int imu, Core::Math::Vec3f rot)
{
    switch(imu){
        case 0 :
                    _rotationAc1.x += rot.x;
                    _rotationAc1.y += rot.y;
                    _rotationAc1.z += rot.z;
                    _rotConfigCounter.x++;
                    break;
        case 1 :
                    _rotationAc2.x += rot.x;
                    _rotationAc2.y += rot.y;
                    _rotationAc2.z += rot.z;
                    _rotConfigCounter.y++;
                    break;
        case 2 :
                    _rotationAc3.x += rot.x;
                    _rotationAc3.y += rot.y;
                    _rotationAc3.z += rot.z;
                    _rotConfigCounter.z++;
                    break;
    }
}

void taskWidget::handleBaseRotation()
{
    _rotationAc1.x /= _rotConfigCounter.x;
    _rotationAc1.y /= _rotConfigCounter.x;
    _rotationAc1.z /= _rotConfigCounter.x;

    _rotationAc2.x /= _rotConfigCounter.y;
    _rotationAc2.y /= _rotConfigCounter.y;
    _rotationAc2.z /= _rotConfigCounter.y;

    _rotationAc3.x /= _rotConfigCounter.z;
    _rotationAc3.y /= _rotConfigCounter.z;
    _rotationAc3.z /= _rotConfigCounter.z;

    _updateBaseRotation = false;
    _vFFTSamples.clear();
    _vFFTSamples.push_back(fftData()); //IMU1
    _vFFTSamples.push_back(fftData()); //IMU2
    _vFFTSamples.push_back(fftData()); //IMU3
    ui->openGLWidget2->reset();

}

void taskWidget::handleRecord(int imu, double time, Core::Math::Vec3f rot)
{
    switch(imu){
        case 0 :
                    _vFFTSamples[0]._vX.push_back(rot.x-_rotationAc1.x);
                    _vFFTSamples[0]._vY.push_back(rot.y-_rotationAc1.y);
                    _vFFTSamples[0]._vZ.push_back(rot.z-_rotationAc1.z);
                    _vFFTSamples[0]._time.push_back(time);
                    break;
        case 1 :
                    _vFFTSamples[1]._vX.push_back(rot.x-_rotationAc2.x);
                    _vFFTSamples[1]._vY.push_back(rot.y-_rotationAc2.y);
                    _vFFTSamples[1]._vZ.push_back(rot.z-_rotationAc2.z);
                    _vFFTSamples[1]._time.push_back(time);
                    break;
        case 2 :
                    _vFFTSamples[2]._vX.push_back(rot.x-_rotationAc3.x);
                    _vFFTSamples[2]._vY.push_back(rot.y-_rotationAc3.y);
                    _vFFTSamples[2]._vZ.push_back(rot.z-_rotationAc3.z);
                    _vFFTSamples[2]._time.push_back(time);
                    break;
    }
}

taskWidget::fftData taskWidget::createFFT(fftData data, int samples)
{
    double startTime    = data._time[0];
    double endTime      = data._time[data._time.size()-1];
    double deltaTime    = (endTime-startTime) / samples;

    fftData fftInput;

    int indexSmaller = 0;
    int indexBigger = 0;

    double currentTime = startTime;
    double dTime = 0;

    for(int i = 0; i < samples;++i){

        for(int k = indexSmaller; k < data._time.size();++k){
            if(data._time[k] > currentTime){
                indexSmaller = k-1;
                indexBigger = k;
                k = data._time.size();
            }
        }

        dTime = currentTime-data._time[indexSmaller];

        fftInput._time.push_back(currentTime);

        fftInput._vX.push_back(data._vX[indexSmaller]*(1-dTime) + data._vX[indexBigger]*dTime);
        fftInput._vY.push_back(data._vX[indexSmaller]*(1-dTime) + data._vX[indexBigger]*dTime);
        fftInput._vZ.push_back(data._vX[indexSmaller]*(1-dTime) + data._vX[indexBigger]*dTime);

        currentTime += deltaTime;
    }
    return fftInput;
}

void taskWidget::plot(int imu, double time, Core::Math::Vec3f rot,
          Core::Math::Vec3f acl, Core::Math::Vec3f gyr,
          Core::Math::Vec4f qua, QCustomPlot* aclPlot,
          QCustomPlot* gyrPlot, QCustomPlot* rotPlot,
          QCustomPlot* fftPlot, Core::Math::Vec3d baseRotation)
{
    Core::Math::Vec3f actRot;
    actRot.x = rot.x-baseRotation.x;
    actRot.y = rot.y-baseRotation.y;
    actRot.z = rot.z-baseRotation.z;

    aclPlot->graph(0)->addData(time,acl.x);
    aclPlot->graph(1)->addData(time,acl.y);
    aclPlot->graph(2)->addData(time,acl.z);

    gyrPlot->graph(0)->addData(time,gyr.x);
    gyrPlot->graph(1)->addData(time,gyr.y);
    gyrPlot->graph(2)->addData(time,gyr.z);

    rotPlot->graph(0)->addData(time,rot.x-baseRotation.x);
    rotPlot->graph(1)->addData(time,rot.y-baseRotation.y);
    rotPlot->graph(2)->addData(time,rot.z-baseRotation.z);

    displayRotation(imu,actRot);

    _plotrange.x = time-3000;
    _plotrange.y = time;


    switch(imu){
        case 0: _vTime.push_back(time);
                _vRot0Z.push_back(rot.z-baseRotation.z); break;
        case 1: _vRot1Z.push_back(rot.z-baseRotation.z); break;
        case 2: _vRot2Z.push_back(rot.z-baseRotation.z); break;
    }

    for(int i = 0; i < _plots.size()-3;++i){
        _plots[i]->xAxis->setRange(_plotrange.x,_plotrange.y);
    }
}

void taskWidget::displayRotation(int imu, Core::Math::Vec3f rot){
    switch(imu){
        case 0 : break;
        case 1 : ui->openGLWidget->updateCube1(rot);
                 ui->openGLWidget2->updateCube1(rot);
                 break;
        case 2 : ui->openGLWidget->updateCube2(rot);
                 ui->openGLWidget2->updateCube2(rot);
                 break;
    }
}

void taskWidget::displayRotation(Core::Math::Vec3f rotFore, Core::Math::Vec3f rotThumb){
    ui->openGLWidget->updateCube1(rotFore);
    ui->openGLWidget->updateCube2(rotThumb);


    Core::Math::Vec3f   _tip1;    //fingerspitze
    Core::Math::Vec3f   _tip2;    //daumen

    Core::Math::Vec4f   corner1(5.0f,0,0,1.0f);
    Core::Math::Vec4f   corner2(3.0f,0,0,1.0f);

    Core::Math::Mat4f   rotationZ1,rotationZ2;

    //yaw pitch roll

    rotationZ1.RotationZ(-rotFore.z * M_PI/180.0f);
    rotationZ2.RotationZ(-rotThumb.z * M_PI/180.0f);

    _tip1 = (corner1*rotationZ1).xyz();
    _tip2 = (corner2*rotationZ2).xyz();

    float angle = 2 * std::atan((_tip1*_tip2.length() - _tip1.length()*_tip2).length() / (_tip1 * (_tip2.length()) + (_tip1 * _tip2).length()).length());
    angle = 180.0/M_PI*angle;

    ui->openGLWidget2->updateCube1(Core::Math::Vec3f(0,0,-angle));
    ui->openGLWidget2->updateCube2(Core::Math::Vec3f(0,0,0));
}

void taskWidget::realtimePlotSlot(){
    for(int i = 0; i < _plots.size();++i){
        _plots[i]->replot();
    }
}

void taskWidget::setupPlots()
{
    _plots.push_back(ui->ac0);
    _plots.push_back(ui->ac1);
    _plots.push_back(ui->ac2);

    _plots.push_back(ui->gy0);
    _plots.push_back(ui->gy1);
    _plots.push_back(ui->gy2);

    _plots.push_back(ui->rot0);
    _plots.push_back(ui->rot1);
    _plots.push_back(ui->rot2);

    _plots.push_back(ui->amplitude);
    _plots.push_back(ui->vis);


    _plots.push_back(ui->fft0);
    _plots.push_back(ui->fft1);
    _plots.push_back(ui->fft2);

    for(int i = 0; i < _plots.size();++i){
        _plots[i]->addGraph();
        _plots[i]->graph(0)->setPen(QPen(Qt::red));

        _plots[i]->addGraph();
        _plots[i]->graph(1)->setPen(QPen(Qt::green));

        _plots[i]->addGraph();
        _plots[i]->graph(2)->setPen(QPen(Qt::blue));

        _plots[i]->yAxis->setTicks(false);
        _plots[i]->xAxis->setTicks(false);

        _plots[i]->setInteraction(QCP::iRangeDrag,true);
        _plots[i]->setInteraction(QCP::iSelectAxes,true);
        _plots[i]->setInteraction(QCP::iRangeZoom,true);

        //fftrange
        _plots[i]->yAxis->setRange(0,100);

        //rot
        if(i < 9)
            _plots[i]->yAxis->setRange(-180,180);

        //gyro
        if(i < 6)
            _plots[i]->yAxis->setRange(-250,250);

        //accl
        if(i < 3)
            _plots[i]->yAxis->setRange(-3.5,3.5);
    }

    for(int i = 0; i < _plots.size()-3;++i){
        _lineList.push_back(std::make_shared<QCPItemLine>(_plots[i]));

        _lineList[i]->start->setCoords(0,0);
        _lineList[i]->end->setCoords(0, 0);
    }

    ui->amplitude->xAxis->setTicks(true);
    ui->vis->xAxis->setTicks(true);

    ui->vis->addGraph();
    ui->vis->graph(3)->setPen(QPen(Qt::black));
    ui->vis->addGraph();
    ui->vis->graph(4)->setPen(QPen(Qt::cyan));

    ui->fft0->xAxis->setTicks(true);
    ui->fft1->xAxis->setTicks(true);
    ui->fft2->xAxis->setTicks(true);
}

void taskWidget::on_init_clicked()
{
    mocca::ByteArray dummy;
    int p = 7892;
    dummy.append(&p, sizeof(p));
    _socket->send(dummy,"192.168.1.1",7892);
}

void taskWidget::on_start_clicked()
{
    _record = true;
    _updateBaseRotation = true;
    _timer.start();
    _rotConfigCounter = Core::Math::Vec3d(0,0,0);
}

void taskWidget::startFile(std::string append ){
    if(_pFile == NULL){
        LINFO("creating new file");
        std::string s = ui->id->text().toStdString();
        s += append;
        s += ui->TypeSelection->itemData(ui->TypeSelection->currentIndex()).toString().toStdString();
        if(s.size() == 0){
            s = "noInfo_.csv";
        }else{
            s += ".csv";
        }
        _pFile = fopen (s.c_str(),"w");
        std::string line = "time0,ax0,ay0,az0,gx0,gy0,gz0,rx0,ry0,rz0,time1,ax1,ay1,az1,gx1,gy1,gz1,rx1,ry1,rz1,time2,ax2,ay2,az2,gx2,gy2,gz2,rx2,ry2,rz2\n";
        fwrite (line.c_str(), sizeof(unsigned char), line.size(), _pFile);


    }
}

void taskWidget::endFile(){
    if(_pFile != NULL){
        LINFO("stop file");
        fclose (_pFile);
        _pFile = NULL;
    }
}

void taskWidget::writeLine(double time, std::vector<float>& data){
    if(_pFile != NULL){
        for(int k = 0 ; k < data.size();k+= 19){
            std::string line = "";
            line += std::to_string(data[k]);
            for(int i = k+1; i < k+19;++i){
                line += ","+ std::to_string(data[i]);
            }
            line += "\n";
            fwrite (line.c_str(), sizeof(unsigned char), line.size(), _pFile);
        }
    }
}

void taskWidget::write( double time,
                        Core::Math::Vec3f a, Core::Math::Vec3f g, Core::Math::Vec3f r,
                        bool endline)
{
    if(_pFile != NULL){
        std::string line = "";
        line += std::to_string(time);

        line += ","+ std::to_string(a.x);
        line += ","+ std::to_string(a.y);
        line += ","+ std::to_string(a.z);
        line += ","+ std::to_string(g.x);
        line += ","+ std::to_string(g.y);
        line += ","+ std::to_string(g.z);
        line += ","+ std::to_string(r.x);
        line += ","+ std::to_string(r.y);
        line += ","+ std::to_string(r.z);

        if(endline)
            line += "\n";
        else
            line += ",";
        fwrite (line.c_str(), sizeof(unsigned char), line.size(), _pFile);
    }
}

void taskWidget::on_loadButton_clicked()
{


    QString fileName = QFileDialog::getOpenFileName(this,
         tr("Open Recording"), "", tr("Image Files (*.csv *.CSV)"));

    for(int i = 0; i < _plots.size();++i){
        for(int j = 0; j < 3; ++j){
            _plots[i]->graph(j)->clearData();
        }
    }

    if(fileName != NULL && fileName.size() > 1){
        openFile(fileName.toStdString());
        window()->setWindowTitle(fileName);
    }
}

void taskWidget::openFile(std::string filename)
{
    std::ifstream myfile (filename);
    std::string line;
    int minTime = 0;
    int maxTime = 0;
    int counter = 0;
    bool avg = false;

    if (myfile.is_open())
    {
        std::getline (myfile,line);

        std::vector<float> values;

        while ( std::getline (myfile,line) )
        {
          values = mocca::splitString<float>(line,',');
          if(minTime == 0){
              minTime = values[0];
          }
          values[0] = values[0]-minTime;
          if(   values[0] > 500 &&
                values[0] < 2500){
                _rotationAc1 += Core::Math::Vec3d(values[7],values[8],values[9]);
                _rotationAc2 += Core::Math::Vec3d(values[17],values[18],values[19]);
                _rotationAc3 += Core::Math::Vec3d(values[27],values[28],values[29]);
                counter++;
          }
          if(   values[0] > 3000 && !avg){
                _rotationAc1 /= counter;
                _rotationAc2 /= counter;
                _rotationAc3 /= counter;
                avg = true;
          }

          if(avg){
              plot( 0,
                    values[0],
                    Core::Math::Vec3f(values[7],values[8],values[9]),
                    Core::Math::Vec3f(values[1],values[2],values[3]),
                    Core::Math::Vec3f(values[4],values[5],values[6]),
                    Core::Math::Vec4f(0,0,0,0),
                    ui->ac0,
                    ui->gy0,
                    ui->rot0,
                    ui->fft0,
                    _rotationAc1);

              plot( 1,
                    values[10]-minTime,
                    Core::Math::Vec3f(values[17],values[18],values[19]),
                    Core::Math::Vec3f(values[11],values[12],values[13]),
                    Core::Math::Vec3f(values[14],values[15],values[16]),
                    Core::Math::Vec4f(0,0,0,0),
                    ui->ac1,
                    ui->gy1,
                    ui->rot1,
                    ui->fft1,
                    _rotationAc2);

              plot( 2,
                    values[20]-minTime,
                    Core::Math::Vec3f(values[27],values[28],values[29]),
                    Core::Math::Vec3f(values[21],values[22],values[23]),
                    Core::Math::Vec3f(values[24],values[25],values[26]),
                    Core::Math::Vec4f(0,0,0,0),
                    ui->ac2,
                    ui->gy2,
                    ui->rot2,
                    ui->fft2,
                    _rotationAc3);
          }else{
              plot( 0,
                    values[0],
                    Core::Math::Vec3f(values[7],values[8],values[9]),
                    Core::Math::Vec3f(values[1],values[2],values[3]),
                    Core::Math::Vec3f(values[4],values[5],values[6]),
                    Core::Math::Vec4f(0,0,0,0),
                    ui->ac0,
                    ui->gy0,
                    ui->rot0,
                    ui->fft0,
                    Core::Math::Vec3d(0,0,0));

              plot( 1,
                    values[10]-minTime,
                    Core::Math::Vec3f(values[17],values[18],values[19]),
                    Core::Math::Vec3f(values[11],values[12],values[13]),
                    Core::Math::Vec3f(values[14],values[15],values[16]),
                    Core::Math::Vec4f(0,0,0,0),
                    ui->ac1,
                    ui->gy1,
                    ui->rot1,
                    ui->fft1,
                    Core::Math::Vec3d(0,0,0));

              plot( 2,
                    values[20]-minTime,
                    Core::Math::Vec3f(values[27],values[28],values[29]),
                    Core::Math::Vec3f(values[21],values[22],values[23]),
                    Core::Math::Vec3f(values[24],values[25],values[26]),
                    Core::Math::Vec4f(0,0,0,0),
                    ui->ac2,
                    ui->gy2,
                    ui->rot2,
                    ui->fft2,
                    Core::Math::Vec3d(0,0,0));
          }

          maxTime = values[20];
        }
        myfile.close();
    }
    maxTime = maxTime-minTime;
    ui->ac0s->setRange(0,maxTime);
    ui->ac1s->setRange(0,maxTime);
    ui->ac2s->setRange(0,maxTime);

    ui->gy0s->setRange(0,maxTime);
    ui->gy1s->setRange(0,maxTime);
    ui->gy2s->setRange(0,maxTime);

    ui->ro0s->setRange(0,maxTime);
    ui->ro1s->setRange(0,maxTime);
    ui->ro2s->setRange(0,maxTime);

    ui->timeSlider->setRange(0,maxTime);

    updateSliderAndRange(maxTime);


    LDEBUG(_vTime.size()<<" "<<_vRot0Z.size()<<" "<<_vRot1Z.size()<<" "<<_vRot2Z.size());

    //calculate the vector of each samplepoint
    for(int i = 0; i < _vTime.size();++i){
        Core::Math::Vec3f   _tip1;    //fingerspitze
        Core::Math::Vec3f   _tip2;    //daumen

        Core::Math::Vec4f   corner1(5.0f,0,0,1.0f);
        Core::Math::Vec4f   corner2(3.0f,0,0,1.0f);

        Core::Math::Mat4f   rotationZ1,rotationZ2;

        //yaw pitch roll

        rotationZ1.RotationZ(-_vRot1Z[i] * M_PI/180.0f);
        rotationZ2.RotationZ(-_vRot2Z[i] * M_PI/180.0f);

        _tip1 = (corner1*rotationZ1).xyz();
        _tip2 = (corner2*rotationZ2).xyz();

        float angle = 2 * std::atan((_tip1*_tip2.length() - _tip1.length()*_tip2).length() / (_tip1 * (_tip2.length()) + (_tip1 * _tip2).length()).length());
        angle = 180.0/M_PI*angle;

        _vTapAngle.push_back(angle);
        _vTapRotFore.push_back(Core::Math::Vec3f(0,0,_vRot1Z[i]));
        _vTapRotThumb.push_back(Core::Math::Vec3f(0,0,_vRot2Z[i]));

        //_vTapRotFore.push_back(Core::Math::Vec3f(0,0,-angle));
        //_vTapRotThumb.push_back(Core::Math::Vec3f(0,0,0));

        ui->amplitude->graph(0)->addData(_vTime[i],angle);
    }
    ui->amplitude->replot();
    ui->amplitude->yAxis->setRange(0,120);

    //persitanceUpdate(10,_vTime[0],_vTime[_vTime.size()]-1);

    ui->windowSlider->setRange(0,_vTime.size()-1);
    ui->windowSlider->setValue(_vTime.size()-1);
    std::string max = "Max : "+ std::to_string((int)_vTime[_vTime.size()-1]);
    ui->maxLabel->setText(QString(max.c_str()));

    ui->windowPosition->setRange(0,_vTime.size()-1);
    ui->windowPosition->setValue(0);
    max = std::to_string((int)_vTime[_vTime.size()-1]);
    ui->positionLabel->setText(QString(max.c_str()));

    updatePersistanceSettings();
}

int taskWidget::calculatePersitanceMaxIndex(int min, int length){
    int maxIndex = min+length;
    if(maxIndex > _vTime.size()-1){
        maxIndex = _vTime.size()-1;
    }
    return maxIndex;
}


void taskWidget::setPlotRange(int time, int range){
    for(int i = 0; i < _plots.size()-4;++i){

        _lineList[i]->start->setCoords(time,300);
        _lineList[i]->end->setCoords(time, -300);

        _plots[i]->xAxis->setRange(time-range,time+range);
        _plots[i]->replot();
    }
}

void taskWidget::updateSliderAndRange(int position){
    ui->ac0s->setValue(position);
    ui->ac1s->setValue(position);
    ui->ac2s->setValue(position);

    ui->gy0s->setValue(position);
    ui->gy1s->setValue(position);
    ui->gy2s->setValue(position);

    ui->ro0s->setValue(position);
    ui->ro1s->setValue(position);
    ui->ro2s->setValue(position);

    ui->timeSlider->setValue(position);

    if(_visLine){

        _visLine->start->setCoords(position,120);
        _visLine->end->setCoords(position, -10);

        ui->vis->replot();
    }

    setPlotRange(position);

    QCPItemTracer *tracer = new QCPItemTracer(ui->rot1);

    tracer->setInterpolating(true);

    Core::Math::Vec3f fore;
    Core::Math::Vec3f thumb;

    tracer->setGraph(ui->rot1->graph(0));
    tracer->setGraphKey(position);
    fore.x =  tracer->position->value();

    tracer->setGraph(ui->rot1->graph(1));
    tracer->setGraphKey(position);
    fore.y =  tracer->position->value();

    tracer->setGraph(ui->rot1->graph(2));
    tracer->setGraphKey(position);
    fore.z =  tracer->position->value();

    delete tracer;


    tracer = new QCPItemTracer(ui->rot2);
    tracer->setInterpolating(true);

    tracer->setGraph(ui->rot2->graph(0));
    tracer->setGraphKey(position);
    thumb.x =  tracer->position->value();

    tracer->setGraph(ui->rot2->graph(1));
    tracer->setGraphKey(position);
    thumb.y =  tracer->position->value();

    tracer->setGraph(ui->rot2->graph(2));
    tracer->setGraphKey(position);
    thumb.z =  tracer->position->value();

    delete tracer;

    displayRotation(fore,thumb);

    if(_replay){
        ui->openGLWidget2->updateDeltaTime(_replayTimer.elapsed());
    }else{
        ui->openGLWidget2->updateDeltaTime(0);
    }

}


void taskWidget::persitanceUpdate(float value, int startTimeIndex, int endTimeIndex)
{
    p1d::Persistence1D p;

    std::vector<float> vAngles,vTime;

    vAngles.resize(endTimeIndex-startTimeIndex,sizeof(float));
    std::memcpy(&vAngles[0],&_vTapAngle[startTimeIndex],(endTimeIndex-startTimeIndex)*sizeof(float));
    vTime.resize(endTimeIndex-startTimeIndex,sizeof(float));
    std::memcpy(&vTime[0],&_vTime[startTimeIndex],(endTimeIndex-startTimeIndex)*sizeof(float));

    p.RunPersistence(vAngles);

    std::vector< p1d::TPairedExtrema > Extrema;
    p.GetPairedExtrema(Extrema, value);

    if(Extrema.size() == 0) return;

    std::vector<float> max;

    float t,f;

    float maxRFore = 0.0f;
    float minRFore = -400.0f;

    float maxRThumb = 0.0f;
    float minRThumb = -400.0f;

    _numberofTaps = 0;
    _avgAmp = 0;
    _avgFreq = 0;

    for(QCPItemLine* l : _vMinMaxLines)
    {
        ui->amplitude->removeItem(l);
    }
    _vMinMaxLines.clear();

    ui->openGLWidget2->reset();

    //add line for each detected maximum/minimum
    for(std::vector< p1d::TPairedExtrema >::iterator it = Extrema.begin(); it != Extrema.end(); it++)
    {
        QCPItemLine *line = new QCPItemLine(ui->amplitude);
        line->start->setCoords(vTime[(*it).MinIndex],120);
        line->end->setCoords(vTime[(*it).MinIndex], -10);
        line->setPen(QPen(Qt::blue));
        ui->amplitude->addItem(line);

        _vMinMaxLines.push_back(line);

        line = new QCPItemLine(ui->amplitude);
        line->start->setCoords(vTime[(*it).MaxIndex],120);
        line->end->setCoords(vTime[(*it).MaxIndex], -10);
        line->setPen(QPen(Qt::red));
        ui->amplitude->addItem(line);

        _vMinMaxLines.push_back(line);


        max.push_back((*it).MaxIndex);
    }

    std::sort(max.begin(),max.end());

    //clear the graphs
    ui->vis->graph(0)->clearData();
    ui->vis->graph(2)->clearData();


    for(int i = 0; i < max.size()-1;++i){
        t =vTime[max[i+1]]-vTime[max[i]];
        f = 1000.0f/t;

        if(f < 10.0){

            if(maxRFore >= -vAngles[max[i]])
                maxRFore = -vAngles[max[i]];

            if(minRFore <= -vAngles[max[i]])
                minRFore = -vAngles[max[i]];

            /*if(maxRFore >= _vTapRotFore[max[i]].z)
                maxRFore = _vTapRotFore[max[i]].z;

            if(minRFore <= _vTapRotFore[max[i]].z)
                minRFore = _vTapRotFore[max[i]].z;
*/

            if(maxRThumb >= _vTapRotThumb[max[i]].z)
                maxRThumb = _vTapRotThumb[max[i]].z;

            if(minRThumb <= _vTapRotThumb[max[i]].z)
                minRThumb = _vTapRotThumb[max[i]].z;


            ui->vis->graph(0)->addData(vTime[max[i]],vAngles[max[i]]/10.0f);
            ui->vis->graph(2)->addData(vTime[max[i]],f);

            _avgAmp     += vAngles[max[i]]/10.0f;
            _avgFreq    += f;
            _numberofTaps++;
        }
    }

    _avgAmp /= _numberofTaps;
    _avgFreq /= _numberofTaps;

    ui->vis->graph(3)->addData(0,_avgAmp);
    ui->vis->graph(4)->addData(0,_avgFreq);

    ui->vis->graph(3)->addData(vTime[max[max.size()-1]],_avgAmp);
    ui->vis->graph(4)->addData(vTime[max[max.size()-1]],_avgFreq);

    ui->vis->xAxis->setRange(vTime[max[0]],vTime[max[max.size()-2]]);
    ui->vis->yAxis->setRange(0,11);
    ui->vis->yAxis->setTicks(true);

    _visLine = new QCPItemLine(ui->vis);
    _visLine->start->setCoords(0,120);
    _visLine->end->setCoords(0, -10);
    _visLine->setPen(QPen(Qt::black));
    ui->vis->addItem(_visLine);

    ui->vis->replot();
    ui->amplitude->replot();
    buildMetaString();

    /*
    //START calculate the lines for the colormap maybe change -----
    for(float f = maxRFore; f < minRFore; f += 0.1f){
        float countG = 0;
        for(int i = 0;i < max.size()-1;++i){
            float t =vTime[max[i+1]]-vTime[max[i]];
            float ft = 1000.0f/t;
            if(f >= _vTapRotFore[max[i]].z && ft < 10.0f)
                countG++;
        }

        float p = countG/_numberofTaps;

         //ui->openGLWidget2->addRotation(Core::Math::Vec3f(0,0,f));
         //ui->openGLWidget2->addColor(Core::Math::Vec3f(1.0f,p,0));
    }

    for(float f = maxRThumb; f < minRThumb; f += 0.1f){
        float countG = 0;
        for(int i = 0;i < max.size()-1;++i){
            float t =vTime[max[i+1]]-vTime[max[i]];
            float ft = 1000.0f/t;
            if(f >= _vTapRotThumb[max[i]].z && ft < 10.0f)
                countG++;
        }

        float p = countG/_numberofTaps;

         //ui->openGLWidget2->addRotationThumb(Core::Math::Vec3f(0,0,f));
         ui->openGLWidget2->addRotationThumb(Core::Math::Vec3f(0,0,0));
         ui->openGLWidget2->addColorThumb(Core::Math::Vec3f(1.0f,1.0f-p,0));
    }
    //END  calculate the lines for the colormap maybe change -----
    */

    ui->openGLWidget2->addRotationThumb(Core::Math::Vec3f(0,0,0));
    ui->openGLWidget2->addColorThumb(Core::Math::Vec3f(1.0f,1.0f,1.0f));


    //verteilung berechnen v2
    std::vector<float> TapWindow;
    float windowWidth = 5.0f;
    float counter = 0;
    float maxC = 0;
    for(float d = 0; d < 130; d += windowWidth){
        counter = 0;
        for(int i = 0; i < max.size();++i){
            if( d < vAngles[max[i]] &&
                (d+windowWidth) >= vAngles[max[i]]){
                counter++;
            }
        }

        if(maxC < counter)
            maxC = counter;

        TapWindow.push_back(counter);
    }

    for(int i = 0; i < TapWindow.size();++i){
        int min = i * windowWidth;
        int max = i * windowWidth + windowWidth;

        if(TapWindow[i] != 0){
            //LDEBUG(min << " - "<< max << " : "<< TapWindow[i]);
            for(int k = i+1; k < TapWindow.size();k++){
                if(TapWindow[k] != 0){
                    float startPerc = TapWindow[i]/maxC;
                    float endPerc = TapWindow[k]/maxC;
                    float startDeg = i * windowWidth+ 0.5f * windowWidth;
                    float endDeg = k * windowWidth+ 0.5f * windowWidth;

                    for(float d = startDeg; d < endDeg; d += 0.1f){
                          float deltaP = (d-startDeg) / (endDeg-startDeg);
                          float p =  startPerc * (1.0-deltaP) + endPerc * deltaP;

                          ui->openGLWidget2->addRotation(Core::Math::Vec3f(0,0,-d));
                          ui->openGLWidget2->addColor(Core::Math::Vec3f(p,p,p));
                    }

                    k = TapWindow.size();
                }
            }
        }
    }
    ui->openGLWidget2->update();
}


std::string taskWidget::buildMetaString(){
    std::string output = "";
    output += "Average Freq: ";
    output += std::to_string(_avgFreq);
    output += "Hz - Average Degree between fingers : ";
    output += std::to_string(_avgAmp*10.0f);
    output += " - Number of recorded Taps : ";
    output += std::to_string(_numberofTaps);

    ui->metaText->setText(output.c_str());

    return output;
}


// UI HANDLING --------------------------------------------------------------------------------

void taskWidget::on_timeSlider_valueChanged(int value)
{
    updateSliderAndRange(value);
}

void taskWidget::on_persistance_valueChanged(int value)
{
    persitanceUpdate((float)value/100.0f,0,_vTime.size()-1);
}

void taskWidget::on_playButton_clicked()
{
    if(!_replay){
        _replayTimer.start();
        _startTime = ui->ac0s->value();
        _replay = true;
        ui->openGLWidget2->reset();
        ui->playButton->setText("Stop");
    }else{
        _replay = false;
        ui->openGLWidget2->stop();
        ui->playButton->setText("Play");
    }
}

void taskWidget::on_ac0s_sliderMoved(int position)
{
    updateSliderAndRange(position);
}

void taskWidget::on_ac1s_sliderMoved(int position)
{
    updateSliderAndRange(position);
}

void taskWidget::on_ac2s_sliderMoved(int position)
{
    updateSliderAndRange(position);
}

void taskWidget::on_gy0s_sliderMoved(int position)
{
    updateSliderAndRange(position);
}

void taskWidget::on_gy1s_sliderMoved(int position)
{
    updateSliderAndRange(position);
}

void taskWidget::on_gy2s_sliderMoved(int position)
{
    updateSliderAndRange(position);
}

void taskWidget::on_ro0s_sliderMoved(int position)
{
    updateSliderAndRange(position);
}

void taskWidget::on_ro1s_sliderMoved(int position)
{
    updateSliderAndRange(position);
}

void taskWidget::on_ro2s_sliderMoved(int position)
{
    updateSliderAndRange((float)position/10.0f);
}

void taskWidget::on_windowSlider_sliderMoved(int position)
{
    updatePersistanceSettings();
}

void taskWidget::on_windowPosition_sliderMoved(int position)
{
    updatePersistanceSettings();
}

void taskWidget::updatePersistanceSettings(){
    int maxIndex = calculatePersitanceMaxIndex(ui->windowPosition->value(),
                                               ui->windowSlider->value());
    persitanceUpdate(10,ui->windowPosition->value(),
                     maxIndex);

    std::string output;
    int width = _vTime[ui->windowSlider->value()];

    output = "Window width : "+ std::to_string(width);
    ui->widthLabel->setText(QString(output.c_str()));

    int pos = _vTime[ui->windowPosition->value()];
    output = "Window pos : "+ std::to_string(pos);
    ui->posLabel->setText(QString(output.c_str()));

}
