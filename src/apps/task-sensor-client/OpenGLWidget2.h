#ifndef OPENGLWIDGET2_H
#define OPENGLWIDGET2_H

#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QOpenGLBuffer>
#include <QMouseEvent>
#include <QWheelEvent>

#include <vector>
#include <cstdint>

#include "rendering/RenderMutex.h"
#include "rendering/IRenderer.h"
#include "silverbullet/math/Vectors.h"
#include <QOpenGLFunctions_3_3_Core>

#include "opengl-base/GLBoundingBox.h"
#include "opengl-base/GLProgram.h"
#include "opengl-base/GLVolumeBox.h"
#include "opengl-base/GLFrameBufferObject.h"
#include "opengl-base/GLTargetBinder.h"
#include "opengl-base/GLRenderPlane.h"

#include "utils/Camera.h"

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram);
QT_FORWARD_DECLARE_CLASS(QOpenGLTexture)

class OpenGLWidget2 : public QOpenGLWidget, protected QOpenGLFunctions {
public:
    OpenGLWidget2(QWidget* parent = 0);
    ~OpenGLWidget2();

    void initializeGL();
    void paintGL();

    void paintColorMap();
    void paintColorFinger(int finger, Core::Math::Vec3f rotation, Core::Math::Vec3f color);

    void paintCurrentRotation();
    void paintCompositing();

    void mousePressEvent( QMouseEvent* ev );
    void mouseReleaseEvent( QMouseEvent* ev );
    void mouseMoveEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent* event);

    Core::Math::Vec2ui getWidgetResolution();

    void updateCube0(Core::Math::Vec3f rotation);
    void updateCube1(Core::Math::Vec3f rotation);
    void updateCube2(Core::Math::Vec3f rotation);
    void updateDeltaTime(float time);

    void reset();
    void stop();

    void addRotation(Core::Math::Vec3f r);
    void clearRotation();

    void addColor(Core::Math::Vec3f r);
    void clearColor();

    void addRotationThumb(Core::Math::Vec3f r);
    void clearRotationThumb();

    void addColorThumb(Core::Math::Vec3f r);
    void clearColorThumb();

private:
  void createVBO();
  void createShaderProgram();
  void createGeometry();
  void createUtils();

  bool loadAndCheckShaders(std::shared_ptr<GLProgram>& programPtr,
                           ShaderDescriptor& sd);

  Core::Math::Vec2i           _lastPosition;
  float                       _movementScale;

  //Rendering Component
  std::unique_ptr<GLVolumeBox>      _cubeGeometry;
  std::unique_ptr<GLModel>          _lineGeometry;
  std::shared_ptr<GLProgram>        _cubeShader;
  std::shared_ptr<GLProgram>        _CompositingShader;
  std::shared_ptr<GLProgram>        _taskMixShader;
  std::unique_ptr<GLRenderPlane>    _renderPlane;

  std::unique_ptr<utils::Camera>    _camera;

  Core::Math::Mat4f                 _worldMatrixCube1;
  Core::Math::Mat4f                 _worldMatrixCube2;
  Core::Math::Mat4f                 _worldMatrixCube0;

  Core::Math::Vec3f                 _tip0;
  Core::Math::Vec3f                 _tip1;
  Core::Math::Vec3f                 _tip2;

  Core::Math::Mat4f                 _projectionMatrix;
  Core::Math::Mat4f                 _viewMatrix;

  GLTargetBinder                    _targetBinder;
  std::shared_ptr<GLRenderTexture>  _offbuffer;
  std::shared_ptr<GLRenderTexture>  _offbuffer2;
  std::shared_ptr<GLRenderTexture>  _offbuffer3;
  std::shared_ptr<GLRenderTexture>  _colorMapBuffer;

  GLint                          _displayTargetID;
  bool                           _continueAdding;
  bool                           _shouldreset;
  float                          _deltaTime;

  std::vector<Core::Math::Vec3f> _vDisplayedRotationsFore;
  std::vector<Core::Math::Vec3f> _vDisplayedColorFore;

  std::vector<Core::Math::Vec3f> _vDisplayedRotationsThumb;
  std::vector<Core::Math::Vec3f> _vDisplayedColorThumb;
};

#endif // OPENGLWIDGET2_H
