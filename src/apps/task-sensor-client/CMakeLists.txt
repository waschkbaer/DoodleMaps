file(GLOB TASKCLIENT_SOURCE ${CMAKE_CURRENT_LIST_DIR}/*.cpp)
file(GLOB TASKCLIENT_HEADER ${CMAKE_CURRENT_LIST_DIR}/*.h)


CreateSourceGroups("${TASKCLIENT_HEADER}" ${CMAKE_CURRENT_LIST_DIR})
CreateSourceGroups("${TASKCLIENT_SOURCE}" ${CMAKE_CURRENT_LIST_DIR})


SET(CMAKE_INCLUDE_CURRENT_DIR ON)
SET(CMAKE_AUTOMOC ON)
SET(CMAKE_AUTORCC ON)

# Find the QtWidgets library
find_package(Qt5Widgets REQUIRED)
find_package(Qt5OpenGL REQUIRED)  
find_package(Qt5PrintSupport REQUIRED)
find_package(OpenGL REQUIRED)

QT5_WRAP_UI(UIS_HDRS mainwindow.ui)
QT5_WRAP_UI(UIS_HDRS taskwidget.ui)

add_executable(task-client 
                        ${TASKCLIENT_SOURCE}
                        ${TASKCLIENT_HEADER}
			${UIS_HDRS}
			$<TARGET_OBJECTS:opengl-base>
			$<TARGET_OBJECTS:opencl-base>
			$<TARGET_OBJECTS:mocca>
			$<TARGET_OBJECTS:silverbullet>
                        $<TARGET_OBJECTS:neuro-io>
                        $<TARGET_OBJECTS:neuro-processing>
			$<TARGET_OBJECTS:utils>
			$<TARGET_OBJECTS:tripeg>
			$<TARGET_OBJECTS:discoverylib>
			$<TARGET_OBJECTS:rendering>
			$<TARGET_OBJECTS:volume-io>
			$<TARGET_OBJECTS:common>
			$<TARGET_OBJECTS:lzw>)

TARGET_LINK_LIBRARIES(task-client turbojpeg-static)
TARGET_LINK_LIBRARIES(task-client Qt5::Widgets)
TARGET_LINK_LIBRARIES(task-client Qt5::OpenGL)
TARGET_LINK_LIBRARIES(task-client Qt5::PrintSupport)
TARGET_LINK_LIBRARIES(task-client ${OPENGL_LIBRARIES})
TARGET_LINK_LIBRARIES(task-client ${OpenCL_LIBRARIES})
TARGET_LINK_LIBRARIES(task-client ${EGL_LIBRARIES})

IF(FFTW_LIBRARIES)
	target_link_libraries (task-client ${FFTW_LIBRARIES})
ELSE(FFTW_LIBRARIES)
	target_link_libraries (task-client "${NEUROVIS_ROOT_DIR}/ext/FFTW3/libs/windows/libfftw3-3.lib" )
ENDIF(FFTW_LIBRARIES)

IF (UNIX)
	TARGET_LINK_LIBRARIES(task-client ${CMAKE_DL_LIBS})
ENDIF ()

IF (WIN32)
	TARGET_LINK_LIBRARIES(task-client Shlwapi.lib)
ENDIF ()

#qt5_use_modules(task-client Widgets OpenGL Printsupport)

SET_TARGET_PROPERTIES(task-client PROPERTIES FOLDER "neurovis")
