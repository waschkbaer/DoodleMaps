#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "neuro-io/TaskSensor/TaskClient.h"
#include "neuro-io/MERSensor/MERClient.h"
#include "neuro-processing/FFTProcessing.h"

#include "mocca/log/LogManager.h"

#include "taskwidget.h"

using namespace neuroio;

static int herz = 20000;
static int seconds = 2;
static int fftWindow = herz*seconds;
static int analogWindow = herz*2;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _lastPlottedIndex(0)
{
    ui->setupUi(this);

    taskWidget* task = new taskWidget(this);

    ui->centralWidget->layout()->addWidget(task);

    // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
    connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
    dataTimer.start(250);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::realtimeDataSlot(){

}
