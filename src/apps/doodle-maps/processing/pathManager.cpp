#include "pathManager.h"

#include <mocca/log/LogManager.h>

PathManager::PathManager(int id):
_stop(false),
_id(id),
_startQueueSize(0){

}

PathManager::~PathManager(){
_runThread.join();
}

void PathManager::start(){
    _stop = false;
    _startQueueSize = _taskQeue.size();
    _runThread = std::thread(&PathManager::run, this);
    LINFO("[PathManager::start] thread spawnt");
}

void PathManager::run(){
    LINFO("[PathManager::run] started the run thread of pathManager");
    int counter = 0;
    int numberCores = 4;
    std::vector<std::shared_ptr<PathFinder>> runningFinders;
    int threadSize = 0;
    while(!_stop){

        LINFO("start length " << _taskQeue.size());
        while(!_taskQeue.empty() && runningFinders.size() < numberCores ){
            std::shared_ptr<PathFinder> p = _taskQeue.front();
            runningFinders.push_back(p);
            _taskQeue.pop();
            p->exeute();
            LINFO("started");
            counter++;
            if(counter == 100){
                LINFO("[PathManager::run] ("<<_id << ") "<<_taskQeue.size()<< " p: " <<(1.0f-((float)_taskQeue.size()/(float)_startQueueSize)));
                counter = 0;
            }
        }

        threadSize = runningFinders.size();
        int unfinishedTasks = 0;
        for(int i = 0; i < threadSize;i++){
            if(runningFinders[i]->getFinished()){
                runningFinders[i]->join();
                runningFinders.erase(runningFinders.begin()+i);
                i--;
                threadSize--;
                LINFO("join a thread");
            }else{
                unfinishedTasks++;
            }
        }
        LINFO("end length " << runningFinders.size() << " "<< unfinishedTasks);
        std::this_thread::sleep_for(std::chrono::milliseconds(2));

    }
}
