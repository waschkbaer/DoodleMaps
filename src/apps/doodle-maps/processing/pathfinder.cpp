#include "pathfinder.h"
#include <stack>


PathFinder::PathFinder(std::shared_ptr<OSMMap> map,
           std::shared_ptr<VectorShape> VectorShape, int id,
            double maxTime):
_map(map),
_VectorShape(VectorShape),
_m1Factor(1.0),
_m2Factor(2.0),
_m3Factor(0.4),
_openList(),
_closedList(),
_gValues(),
_finished(false),
_maxTime(maxTime),
_runThread(),
_id(id),
_distanceToStart(0,0),
_gotRoute(false),
_fromMap(),
_timer(),
_interupt(false),
_output()
{

}

PathFinder::~PathFinder(){
}

void PathFinder::exeute(){
    if(_map == nullptr || _VectorShape == nullptr){
        cout << "[PathFinderError] no map or VectorShape set"<<endl;
        return;
    }
    _finished=false;
    _runThread = std::thread(&PathFinder::run, this);

}

void PathFinder::setMetricParameter(double m1, double m2, double m3, double m4){
    _m1Factor = m1;
    _m2Factor = m2;
    _m3Factor = m3;
    _m4Factor = m4;
}

Core::Math::Vec2f getProjectedPointOnLine(Core::Math::Vec2f v1,
                                          Core::Math::Vec2f v2,
                                          Core::Math::Vec2f p)
{
  // get dot product of e1, e2
  Core::Math::Vec2f e1(v2.x - v1.x, v2.y - v1.y);
  Core::Math::Vec2f e2(p.x - v1.x, p.y - v1.y);
  double valDp = e1^e2;
  // get length of vectors
  double lenLineE1 = std::sqrt(e1.x * e1.x + e1.y * e1.y);
  double lenLineE2 = std::sqrt(e2.x * e2.x + e2.y * e2.y);
  double cos = valDp / (lenLineE1 * lenLineE2);
  // length of v1P'
  double projLenOfLine = cos * lenLineE2;
  Core::Math::Vec2f point(  (v1.x + (projLenOfLine * e1.x) / lenLineE1),
                            (v1.y + (projLenOfLine * e1.y) / lenLineE1));
  return point;
}

Core::Math::Vec2f getDir(Core::Math::Vec2f node1,
                         Core::Math::Vec2f node2,
                         Core::Math::Vec2f shapeStart,
                         Core::Math::Vec2f shapeEnd)
{
    Core::Math::Vec2f n1Line = getProjectedPointOnLine(shapeStart,shapeEnd,node1);
    Core::Math::Vec2f n2Line = getProjectedPointOnLine(shapeStart,shapeEnd,node2);

    float length = (n1Line-n2Line).length();

    //Core::Math::Vec2f dir = ((node1-n1Line) + (node2-n2Line)) / length;
    Core::Math::Vec2f dir = (node1-n1Line);
    return dir;
}

double getAngle(Core::Math::Vec2f node1,
                         Core::Math::Vec2f node2,
                         Core::Math::Vec2f shapeStart,
                         Core::Math::Vec2f shapeEnd)
{
    double angle = 0;
    Core::Math::Vec2f dir1 = node1-node2;
    Core::Math::Vec2f dir2 = shapeEnd-shapeStart;

    Core::Math::Vec2f n1Line = getProjectedPointOnLine(shapeStart,shapeEnd,node1);
    Core::Math::Vec2f n2Line = getProjectedPointOnLine(shapeStart,shapeEnd,node2);
    float length = (n1Line-n2Line).length();
    float perc = (length/dir2.length());


    dir1.normalize();
    dir2.normalize();

    angle = dir1^dir2;
    angle = std::acos(angle);

    float angle1 = std::atan2(dir1.x,dir1.y); //node-node // street
    float angle2 = std::atan2(dir2.x,dir2.y); //shape -shape // shape

    if(angle1 < 0.0f) angle1 += M_PI;
    if(angle2 < 0.0f) angle2 += M_PI;

    //LINFO("+++++++++ projle " << length << " shape le "<< dir2.length() << " % "<< (length/dir2.length()) << " angle1 "<< angle1 << " degree1 "<< (angle1*180/M_PI) << " angle2 "<< angle2 << " degree2 "<< (angle2*180/M_PI));
    return (angle1-angle2)*perc;
}

bool isBetween(Core::Math::Vec2f a,
               Core::Math::Vec2f b,
               Core::Math::Vec2f c){
        float crossproduct = (c.y - a.y) * (b.x - a.x) - (c.x - a.x) * (b.y - a.y);
        if(std::abs(crossproduct) > 0.0000001)return false;

        float dotproduct = (c.x - a.x) * (b.x - a.x) + (c.y - a.y)*(b.y - a.y);
        if( dotproduct < 0 ) return false;

        float squaredlengthba = (b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y);
        if(dotproduct > squaredlengthba) return false;

        return true;
}

void PathFinder::clearLists(){
    _openList.clear();
    _closedList.clear();
    _gValues.clear();
    _fromMap.clear();
}

bool isCloseEnough(const Core::Math::Vec2f& shapeStart,
                   const Core::Math::Vec2f& shapeEnd,
                   const std::shared_ptr<Node> NodeStart){
    uint32_t neighbourNodeCount = NodeStart->getNeighbourCount();
    Core::Math::Vec2f startPos = NodeStart->getCoord();
    float length = (shapeEnd-shapeStart).length();
    float lengthNode;
    Core::Math::Vec2f neighbourPos;

    for(int i = 0; i < neighbourNodeCount;++i){
        neighbourPos = NodeStart->getNeighbour(i)->getCoord();
        lengthNode = (neighbourPos-startPos).length();
        if(lengthNode < length)
            return true;
    }
    return false;
}

std::shared_ptr<Node> generateVirtualNode(const std::shared_ptr<Node>& startNode,
                                          const std::shared_ptr<Node>& endNode,
                                          Core::Math::Vec2f startPoint,
                                          Core::Math::Vec2f endPoint){
        Core::Math::Vec2f dir = endNode->getCoord() - startNode->getCoord();

        Core::Math::Vec2f linePosition = getProjectedPointOnLine(startNode->getCoord(),
                                         endNode->getCoord(),
                                         startPoint);

        //check if linePosition is inside start and neighbour end!
        if(isBetween(startNode->getCoord(),
                     endNode->getCoord(),
                     linePosition)){
           std::shared_ptr<Node> node = std::make_shared<Node>();
           node->setLon(linePosition.x);
           node->setLat(linePosition.y);

           node->addNeighbour(startNode);
           node->addNeighbour(endNode);

           startNode->addNeighbour(node);
           startNode->removeNeighbour(endNode);

           endNode->addNeighbour(node);
           endNode->removeNeighbour(startNode);
           return node;
        }

        return nullptr;
}


//#define calculateTransformationOffsets
void PathFinder::run(){
    double preLoopTime = 0;
    _timer.start();

    _interupt = false;
    _accGValue  = 0;
    _gotRoute   = false;

    std::shared_ptr<VectorShapePoint> s = _VectorShape->begin();
    std::shared_ptr<VectorShapePoint> e = s->next();
    std::shared_ptr<Node> start = _map->getClosesNode(s->getWorldPos());;
    _output = std::make_shared<VectorShape>();


    //**************** CALCULATE REAL VIRTUAL CLOSES NODE ON NEIGHBOURS LINE **********

    std::shared_ptr<Node> closesNode = start;
    _distanceToStart = s->getWorldPos()-start->getCoord();
    float distance = _distanceToStart.length();

    for(int j = 0; j < start->getNeighbourCount();++j){
        std::shared_ptr<Node> neighbour = start->getNeighbour(j);

        std::shared_ptr<Node> virtualNode = generateVirtualNode(start,neighbour,s->getWorldPos(),e->getWorldPos());

        if(virtualNode != nullptr){
            float distanceNodeShape = (s->getWorldPos()-virtualNode->getCoord()).length();
            if(distanceNodeShape < distance){
                distance = distanceNodeShape;
                closesNode = virtualNode;
            }
        }
    }

    LINFO(start->getCoord() << " start node of kdTree");
    start = closesNode;
    LINFO(start->getCoord() << " start node of projection");
    _distanceToStart = s->getWorldPos()-start->getCoord();

    //**************** CALCULATE REAL VIRTUAL CLOSES NODE ON NEIGHBOURS LINE **********

    LINFO("[PathFinder::run] distance to start point " << _distanceToStart.length());

    Core::Math::Vec2f                       directionOffset(0,0);
    double                                  zRotation = 0;
    double                                  routeNodeCounter = 0;
    std::stack<std::shared_ptr<Node>>       reverseStack;
    std::shared_ptr<Node>                   currentEndPoint = nullptr;

    std::shared_ptr<Node> nextStart = nullptr;

    do{
        LINFO("[PathFinder::run] execute a-star " << start->getNeighbourCount()<< " "<<start->getCoord()<<  " " <<s->getWorldPos()<< " " << e->getWorldPos());
        nextStart = aStar(start,s->getWorldPos(),e->getWorldPos());

        if(nextStart != nullptr){ //if nextStart == nullptr -> no route from s to e, check s to e->next
            LINFO("[PathFinder::run] got a star result != nullptr " << nextStart->getCoord());
            _accGValue += _gValues.find(nextStart)->second;

            currentEndPoint = nextStart;
            //put reverse route from S to E on a stack, starting with end of line
            do{
                LINFO("[PathFinder::run] reverse putting route ");

#ifdef calculateTransformationOffsets
                if(currentEndPoint != start){
                    directionOffset += getDir(currentEndPoint->getCoord(),
                                              _fromMap.find(c)->second->getCoord(),
                                              s->getWorldPos(),
                                              e->getWorldPos());
                    zRotation += getAngle(currentEndPoint->getCoord(),
                                          _fromMap.find(c)->second->getCoord(),
                                          s->getWorldPos(),
                                          e->getWorldPos());


                }
#endif
                routeNodeCounter++;
                reverseStack.push(currentEndPoint);
                currentEndPoint = _fromMap.find(currentEndPoint)->second;
            }while(currentEndPoint != nullptr);

            //push top of stack to path -> results in path from S to E
            while(!reverseStack.empty()){
                _output->addPoint(reverseStack.top()->getCoord());
                reverseStack.pop();

            }

            LINFO("[PathFinder::run] going to next segment of shape ");
            s = e;
            e = s->next();
            start = nextStart;
        }else{
            LINFO("[PathFinder::run] going to next segment of shape NO RESULT ");
            e = e->next();
            nextStart = nullptr;
        }
    }while(e != nullptr && !_interupt);


    LINFO("[PathFinder::run] finished shape ");

    if(routeNodeCounter != 0){
        _gotRoute = true;
        _dir = directionOffset/routeNodeCounter;
        _rotation = zRotation/routeNodeCounter;
    }
    double pathLength = _output->getLength();
    double shapeLength = _VectorShape->getLength();

    if(shapeLength > pathLength){
        _accGValue = _accGValue * (shapeLength/pathLength);
    }else{
        _accGValue = _accGValue * (pathLength/shapeLength);
    }

    _output->print();

    //LDEBUG("[PathFinder::run] RESULT DirectionOffset: "<< _dir << " angleOffset: "<< (_rotation*180/M_PI) << " RouteValue: "<< _accGValue << " Time : "<< _timer.elapsed());
    //LDEBUG("[PathFinder::run] Time : "<< _timer.elapsed() << " disToStart "<< _distanceToStart.length() << "avg runLoopTime "<< (runLoopTime/(float)runLoopCount) << "total runloop "<< runLoopTime<< "nodes in route "<<routeNodeCounter);
    //LDEBUG("[PathFinder::run] GValue: "<< _accGValue << " count "<< routeNodeCounter << " preloop "<< preLoopTime);
    _finished = true;
    clearLists();
}

std::shared_ptr<Node> PathFinder::aStar(const std::shared_ptr<Node> startNode,
                                        const Core::Math::Vec2f& SegmentStart,
                                        const Core::Math::Vec2f& SegmentEnd){
    //if(!isCloseEnough(SegmentStart,SegmentEnd,startNode)){
    //    return nullptr;
    //}


    //init start parameter
    std::shared_ptr<Node> currentNode = startNode;
    double node_to_end_distance = 0;
    double epsilon = 0.0015f;
    Core::Math::Vec2f middle = (SegmentStart+SegmentEnd)/2.0f;
    float segmentLength = (SegmentStart-SegmentEnd).length();
    float maxLength = segmentLength* 2.0f;
    float nodeDistance = 0;

    //clear the lists
    clearLists();

    //insert start values
    _openList.insert(0,currentNode);
    _gValues.insert(std::make_pair(currentNode,0));

    //clear the from map stack (clear slower than new map?)
    _fromMap.clear();
    _fromMap.insert(std::make_pair(startNode,nullptr));

    //loop while openlist != 0
    do{
        //extract min gvalue node
        currentNode = _openList.extractMin();

        //calculate distance to end of shape segment
        node_to_end_distance = (currentNode->getCoord()-SegmentEnd).length();

        //check epsilon are we close enough to the end?
        if(node_to_end_distance < epsilon && currentNode != startNode){
            return currentNode;
        }

        //push back node to closed list
        _closedList.push_back(currentNode);

        //get the distance of the node to the center of the shape
        //we will only trace shape.length * 2 radius nodes
        nodeDistance = (currentNode->getCoord()-middle).length();
        if(nodeDistance < maxLength)
        {
            //expand neighbours
            expandNode(currentNode,SegmentStart,SegmentEnd);
        }

    }while(!_openList.isEmpty()  && !_interupt);
    return nullptr;
}


void PathFinder::expandNode(std::shared_ptr<Node> currentNode,
                Core::Math::Vec2f start,
                Core::Math::Vec2f end){
    uint32_t neighbourNodeCount = currentNode->getNeighbourCount();
    double gValue = 0;
    double lengthToEndNeighbour = 0;
    double estimateRest = 0;
    uint64_t contains = 0;

    for(int i = 0; i < neighbourNodeCount;++i){
        if(_interupt){
            i = neighbourNodeCount;
            continue;
        }
        std::shared_ptr<Node> n = currentNode->getNeighbour(i);

        //if(currentNode->getFrom() == n) continue; // don't go back!!
        if(_fromMap.find(currentNode) != _fromMap.end() && _fromMap.find(currentNode)->second == n) continue;
        if(std::find(_closedList.begin(),_closedList.end(),n) != _closedList.end()) continue; // check if we handled this node already

        lengthToEndNeighbour = (n->getCoord()-end).length();

        gValue = _gValues.find(currentNode)->second + evaluateNode(currentNode,
                                                                          n,
                                                                          start,
                                                                          end);

        estimateRest = lengthToEndNeighbour;

        contains = _openList.contains(n);
        if(contains > 0 &&
           _gValues.find(n)->second <= gValue){
           continue;
        }else{
            if(_fromMap.find(n) == _fromMap.end()){
                _fromMap.insert(std::make_pair(n,currentNode));
            }else{
                _fromMap.erase(n);
                _fromMap.insert(std::make_pair(n,currentNode));
            }
            if(contains > 0 ){
                _openList.decreaseKey(gValue+estimateRest,n);
                 _gValues.erase(n);
                _gValues.insert(std::make_pair(n,gValue));
            }else{
                _openList.insert(gValue+estimateRest,n);
                _gValues.insert(std::make_pair(n,gValue));
            }
        }
    }
}

double PathFinder::evaluateNode(std::shared_ptr<Node> currentNode,
                                std::shared_ptr<Node> neighbourNode,
                                Core::Math::Vec2f startCoord,
                                Core::Math::Vec2f endCoord,
                                float stepSize)
{
    Core::Math::Vec2f nodeDirection = (neighbourNode->getCoord()-currentNode->getCoord());
    double nodeLength = nodeDirection.length();
    nodeDirection.normalize();

    //metric 1 : distance from tested node position to
    //the target end position of VectorShape
    double alpha = (neighbourNode->getCoord()-endCoord).length();

    //metrix 3 : length of the route
    double gamma = nodeLength;

    //metric 2 : projection from current position onto
    //the current segment of the line
    Core::Math::Vec2f currentPosition = currentNode->getCoord();
    double currentLength = 0;
    double d = 0;
    double x1 = startCoord.x;
    double y1 = startCoord.y;
    double x2 = endCoord.x;
    double y2 = endCoord.y;
    double y = std::sqrt((y2-y1)*(y2-y1) + (x2-x1)*(x2-x1));
    double beta = 0;
    double x0 = 0;
    double y0 = 0;
    double x = 0;

    int c = 0;
    while(currentLength < nodeLength){
        x0 = currentPosition.x;
        y0 = currentPosition.y;

        x = std::abs((y2-y1)*x0 - (x2-x1)*y0 + (x2*y1) - (y2*x1));

        d += x/y;

        currentLength += stepSize;
        currentPosition += stepSize*nodeDirection;

        beta = d;
        c++;
    }
    double sumFactor = _m1Factor+_m2Factor+_m3Factor;

    return (_m1Factor/sumFactor) * alpha + (_m2Factor/sumFactor) * beta + (_m3Factor/sumFactor) * gamma;
}
