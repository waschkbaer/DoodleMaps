#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "rendering/openglrenderer.h"

#include <data/osmmap.h>
#include <data/shape.h>
#include <QImage>
#include <QPainter>
#include "data/priorityqueue.h"
#include <thread>
#include <future>
#include <chrono>
#include <silverbullet/time/Timer.h>

#include <mocca/log/LogManager.h>

class PathFinder
{
public:
    PathFinder(std::shared_ptr<OSMMap> map = nullptr,
               std::shared_ptr<VectorShape> VectorShape = nullptr
                ,int id = 0,
               double maxTime = 3000.0);
    ~PathFinder();

    void exeute();

    void setMap(std::shared_ptr<OSMMap> map) { _map = map; }
    void setVectorShape(std::shared_ptr<VectorShape> VectorShape) { _VectorShape = VectorShape; }
    std::shared_ptr<VectorShape> getShape() {return _VectorShape;}
    std::shared_ptr<VectorShape> getOutput() const {return _output; }

    void setMetricParameter(double m1, double m2, double m3, double m4 = 1);
    Core::Math::Vec2f getShapeTranslation() {   Core::Math::Vec2f d = _dir;
                                                _dir=Core::Math::Vec2f(0,0);
                                                return d;
                                            }
    double getShapeRotation() { double d = _rotation;
                              _rotation = 0;
                              return d;}

    double getGValue() {return _accGValue;}
    bool   getHasRoute() {return _gotRoute;}
    bool   getJoinable() {return _runThread.joinable();}
    void   join() { _runThread.join(); }

    void setDebug(bool b) { _displayDebug = b;}
    const bool getFinished() const {return _finished;};
    void setFinished(bool b) {_finished = b;}

     void run();
     void interupt(){
         _interupt = true;
         //std::this_thread::sleep_for(std::chrono::milliseconds(100));
     }
     void joinThread(){
         if (_runThread.joinable())
             _runThread.join();
     }
     double elapsedTime(){ return _timer.elapsed();}
     int getId() const {return _id;}
     Core::Math::Vec2f getDistanceToStart() {return _distanceToStart;}

     void clearMemElements(){
         _VectorShape.reset();
         _VectorShape=nullptr;
         _output.reset();
         _output=nullptr;
         _map = nullptr;
     }

private:

    std::shared_ptr<Node> aStar(    const std::shared_ptr<Node> startNode,
                                    const Core::Math::Vec2f& SegmentStart,
                                    const Core::Math::Vec2f& SegmentEnd);
    void expandNode(std::shared_ptr<Node> currentNode,
                    Core::Math::Vec2f start,
                    Core::Math::Vec2f end);

    double evaluateNode(std::shared_ptr<Node> currentNode,
                        std::shared_ptr<Node> neighbourNode,
                        Core::Math::Vec2f startCoord,
                        Core::Math::Vec2f endCoord,
                        float stepSize = 0.00025f);

    void clearLists();



private:
    std::shared_ptr<OSMMap>             _map;
    std::shared_ptr<VectorShape>        _VectorShape;
    std::shared_ptr<VectorShape>        _output;

    double                              _m1Factor;
    double                              _m2Factor;
    double                              _m3Factor;
    double                              _m4Factor;
    double                              _accGValue;

    Core::Math::Vec2f                   _dir;
    double                              _rotation;

    PriorityQueue                           _openList;
    std::vector<std::shared_ptr<Node> >     _closedList;
    std::map<std::shared_ptr<Node>,double>  _gValues;
    bool                                    _displayDebug;
    bool                                    _finished;
    bool                                    _gotRoute;
    std::thread                             _runThread;
    double                                  _maxTime;
    int                                     _id;
    Core::Math::Vec2f                       _distanceToStart;
    std::map<std::shared_ptr<Node>,std::shared_ptr<Node>> _fromMap;
    Core::Time::Timer                       _timer;
    bool                                    _interupt;
};

#endif // PATHFINDER_H
