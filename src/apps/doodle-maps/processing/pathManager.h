#ifndef PATHMANAGER_H
#define PATHMANAGER_H

#include "pathfinder.h"
#include <queue>
#include <thread>

class PathManager{
public:
    PathManager(int id = 0);
    ~PathManager();

    void addTask(std::shared_ptr<PathFinder> task){
        _taskQeue.push(task);
    }

    void stop(){
        _stop = true;
    }

    void start();

private:
    void run();

private:
    std::queue<std::shared_ptr<PathFinder>> _taskQeue;
    std::thread                             _runThread;
    bool                                    _stop;
    int                                     _id;
    int                                     _startQueueSize;
};

#endif // PATHMANAGER_H
