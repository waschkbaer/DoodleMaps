#-------------------------------------------------
#
# Project created by QtCreator 2017-03-28T20:27:11
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DoodleMaps
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    node.cpp \
    osmmap.cpp \
    shape.cpp \
    pathfinder.cpp \
    priorityqueue.cpp \
    maplabel.cpp \
    openglrenderer.cpp

HEADERS  += mainwindow.h \
    node.h \
    osmmap.h \
    shape.h \
    vectors.h \
    pathfinder.h \
    priorityqueue.h \
    maplabel.h \
    openglrenderer.h \
    rapidxml.hpp \
    rapidxml_iterators.hpp \
    rapidxml_print.hpp \
    rapidxml_utils.hpp

FORMS    += mainwindow.ui
