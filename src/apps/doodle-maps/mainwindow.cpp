
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <mocca/log/LogManager.h>

#include <thread>
#include <QKeyEvent>

#include <silverbullet/base/DetectEnv.h>

std::string filename = "map"; //duisburg
//std::string filename = "map2"; //new york
// std::string filename = "berlinComplete"; //berlin

#define SingleRoute

int volRes = 16;
double rotSegmentCount = 16.0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _mapimage(nullptr),
    _osmmap(nullptr),
    _painter(nullptr),
    _prevRot(9999.0f),
    _prevGValue(99999.0f),
    _minPathResult(99999.0),
    _finderList(),
    _doneList(),
    _pathManager(),
    _GValueImage(NULL),
    _keepRunning(false),
    _activeMaxCount(12),
    _activeList(),
    _finderIndex(0),
    _minG(9999.0),
    _maxG(0),
    _timer(),
    _lastSave(0),
    _resultarray(),
    _resultarrayDirLength(),
    _resultarrayRot(),
    _finderBounds(),
    _dummyPath(),
    _dummyShape(),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(&_pathTimer, SIGNAL(timeout()), this, SLOT(pathSlot()));
    _pathTimer.start(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::drawMap()
{

}

void MainWindow::loadMap(){
    std::cout << "[LoadMap] start loading " << filename<< ".osm" << std::endl;
    _osmmap = std::make_shared<OSMMap>();
#ifdef DETECTED_OS_LINUX
    _osmmap->loadMap(std::string("/home/waschkbaer/Downloads/"+filename+".osm"));
#endif

#ifdef DETECTED_OS_APPLE
    _osmmap->loadMap(std::string("/Users/waschkbaer/Downloads/"+filename+".osm"));
#endif
    std::cout << "[LoadMap] Finished Loading .osm file: " << _osmmap->toString() << std::endl;


    Core::Math::Vec2f min,max;
    min.x = _osmmap->getBounds().minLon;
    max.x = _osmmap->getBounds().maxLon;
    min.y = _osmmap->getBounds().minLat;
    max.y = _osmmap->getBounds().maxLat;
    ui->openGLWidget->setView(min,max);

    //ui->openGLWidget->generateMap(_osmmap->getGPUMap());
    ui->openGLWidget->setMap(_osmmap);
    ui->openGLWidget->repaint();
}

void MainWindow::prepareVectorShape(){

}
int c = 0;
void MainWindow::pathSlot(){
#ifdef SingleRoute
    shapeSlot();
#else
    volumeSlot();
#endif
}

void MainWindow::shapeSlot(){
    bool done = true;
    int listSize = _finderList.size();
    if(listSize == 0) return;
    std::shared_ptr<PathFinder> path = _finderList.at(0);
    //LDEBUG("[MainWindow::pathSlot] got pathfinder from list "<< i);

    if(path != nullptr && path->getFinished()){
        //LDEBUG("[MainWindow::pathSlot] a pathfinder finished");
        Core::Math::Vec2f dir = path->getShapeTranslation();
        double gVal = path->getGValue();

        //if((dir.x >= 0.00000001 || dir.y >= 0.00000001) && gVal < _prevGValue){
        //    ui->openGLWidget->translateShape(dir);
        //    done=false;
        //}
        //double rot = path->getShapeRotation();
        //if(rot >= 0.0001 && gVal < _prevGValue){
        //   ui->openGLWidget->rotateShape(rot);
        //   done=false;
        //}

        std::shared_ptr<VectorShape> s = path->getOutput();
        if(s->numberOfPoints() > 1){
            //LDEBUG("[MainWindow::pathSlot] creating a GLVectorShape "<< s->numberOfPoints());
            //ui->openGLWidget->setGLRouteShape(s);
            ui->openGLWidget->addShapeAndRoute(path->getShape(),
                                               s);
            //LDEBUG("[MainWindow::pathSlot] test");

        }
    }
    if(!done)on_pushButton_clicked();
}

double lastPercentagePrint = 0.0;
void MainWindow::volumeSlot(){
    if(_keepRunning){
     double t = _timer.elapsed();
     if(_GValueImage == NULL){
         _GValueImage = new QImage(volRes, volRes, QImage::Format_RGB888);
         _GValueImage->fill(qRgb(255, 0, 0));
     }

     while(_activeList.size() < _activeMaxCount &&
         _finderList.size() != 0){

         int random = std::rand() % _finderList.size();
         //int random = _finderList.size()-1;
         _activeList.push_back(_finderList[random]);

         _finderList[random]->exeute();
         _finderList.erase(_finderList.begin()+random);

         if(std::abs(lastPercentagePrint - _finderList.size()) > 1000){
            lastPercentagePrint = _finderList.size();
            LINFO("percentage "<< _finderList.size() << " "<< t);
         }
     }

     bool reImage = false;
     for(int i = _activeList.size()-1; i >= 0;i--){
         std::shared_ptr<PathFinder> p = _activeList[i];
         if(p->getFinished()){
             if(p->getHasRoute()){
                 if(_minG > _activeList[i]->getGValue()) _minG = p->getGValue();
                 if(_maxG < _activeList[i]->getGValue()) _maxG = p->getGValue();
                 //ui->openGLWidget->addShapeAndRoute(p->getShape(),p->getOutput());
                 _resultarray[p->getId()] = p->getGValue();
                 _resultarrayDirLength[p->getId()] = p->getShapeTranslation().length();
                 _resultarrayRot[p->getId()] = p->getShapeRotation();

                 _dummyShape[p->getId()] = p->getShape();
                 _dummyPath[p->getId()] = p->getOutput();
                 //p->clearMemElements();
             }
             reImage = true;
             //_doneList.push_back(p);
             _activeList.erase(_activeList.begin()+i);
             p->joinThread();
             c++;
         }
     }

     if(0 == _finderList.size()){
         //LINFO("All pathFinder started " << _activeList.size());
         if(_activeList.size() != 0){
             for(int i = _activeList.size()-1; i >= 0;i--){
                 //LINFO("not id: " <<_activeList[i]->getId());
             }
         }else{
            LINFO("all done");
            _keepRunning = false;
            _lastSave = -90000;
            LINFO("finished after : "<< t << "milliseconds");
            saveRawFile();
            lastPercentagePrint = 0.0f;
         }
     }


     if(_GValueImage != NULL && (t - _lastSave > 30000) && false){
         _lastSave = t;
         _GValueImage->fill(qRgb(255, 0, 0));
         for(unsigned long i = 0; i < _doneList.size();++i){
             std::shared_ptr<PathFinder> p = _doneList.at(i);
             unsigned long id = p->getId();
             int rotSegment = id / (volRes*volRes);
             int rotIndex = id % (volRes*volRes);
             int y = rotIndex/volRes;
             int x = rotIndex%volRes;


             float GValue = 0;
             bool  hadValue = false;
             double c = 0.0;


             if(p->getHasRoute()){
                 GValue += p->getGValue();;
                 hadValue = true;
                 c++;
             }

             /*
             for(unsigned long j = 0; j < rotSegmentCount; j++){
                 long index = i + (volRes*volRes*j);
                 if(_finderList.at(index)->getHasRoute()){
                     GValue += _finderList.at(index)->getGValue();;
                     hadValue = true;
                     c++;
                 }
             }*/
             GValue /= c;

             GValue = (GValue-_minG)/(_maxG-_minG);
             GValue = (1.0f-GValue);

             if(hadValue){
                 QColor col(255*GValue, 255*GValue, 255*GValue);
                 QColor colSave(_GValueImage->pixel(x, _GValueImage->height()-y-1));
                 if(colSave.red() > col.red()){
                     //LINFO("volSlot - Image - x"<< x << " y"<< y<< " rotSegment"<<rotSegment << " "<< rotIndex<< " "<<_GValueImage->height());

                    _GValueImage->setPixel(x, _GValueImage->height()-y-1, qRgb(255*GValue, 255*GValue, 255*GValue));
                 }
             }
         }
         for(int i = _activeList.size()-1; i >= 0;i--){
             long id = _activeList[i]->getId();
             int y = id/volRes;
             int x = id%volRes;
             _GValueImage->setPixel(x, _GValueImage->height()-y-1, qRgb(0,0,255));
         }


         _GValueImage->save("SOMEMAP.png");
         c= 0;
     }
     }
}

void MainWindow::on_loadButton_clicked()
{
    loadMap();

    prepareVectorShape();

    drawMap();
}

void MainWindow::on_pushButton_clicked()
{
#ifdef SingleRoute
    traceShape();
#else
    MapBounds b = _osmmap->getBounds();
    traceVolume(b.minLat,b.maxLat,b.minLon,b.maxLon);
#endif
}

void MainWindow::traceShape(){
    MapBounds b = _osmmap->getBounds();
    std::shared_ptr<VectorShape> shape = ui->openGLWidget->getShape();
    shape->calcMidPoint();
    LINFO("[Pushbutton]  "<<b.maxLat<< " "<< b.minLat<< " | "<< b.maxLon<< " "<<b.minLon);
    LINFO("[Pushbutton]  "<< shape->getMidPoint());

    std::shared_ptr<PathFinder> pathFinder = std::make_shared<PathFinder>(_osmmap,
                                                                          shape);

    pathFinder->setMetricParameter( ui->m1->text().toDouble(),
                                    ui->m2->text().toDouble(),
                                    ui->m3->text().toDouble());
    pathFinder->exeute();
    _prevGValue = 99999.0f;

    _finderList. push_back(pathFinder);
}

void MainWindow::traceVolume(double minLatF, double maxLatF, double minLonF, double maxLonF){
    _keepRunning = false;

    _finderBounds.clear();
    _doneList.clear();
    _resultarray.clear();
    _activeList.clear();
    _resultarrayDirLength.clear();
    _resultarrayRot.clear();
    _finderBounds.clear();
    _dummyPath.clear();
    _dummyShape.clear();

    //MapBounds b = _osmmap->getVisibleBounds();
    std::shared_ptr<VectorShape> shape = ui->openGLWidget->getShape();
    shape->calcMidPoint();

    LINFO("[Pushbutton]  Segement Bounds: "<<minLatF<< " "<< maxLatF << " | "<< minLonF << " "<< maxLonF);

    double difLat = maxLatF-minLatF;
    double difLon = maxLonF-minLonF;

    double resolution = volRes;

    double deltaX = difLat/resolution;
    double deltaY = difLon/resolution;

    _prevGValue = 99999.0f;

    LINFO("[MainWindow::traceVolume] creating pathTasks");
    //spawn multiple pathfinders
    int id = 0;
    double deltaRot = (2*M_PI)/rotSegmentCount;

    uint32_t countF = 0;
    for(double r = 0.0; r < 2*M_PI; r+= deltaRot){
        for(double x = minLatF+ (0.5f*deltaX); x < maxLatF; x +=deltaX){
            for(double y = minLonF+ (0.5f*deltaY); y < maxLonF; y +=deltaY){
                Core::Math::Vec2f mid( y, x);

                std::shared_ptr<VectorShape> copyShape = shape->copyShape();
                copyShape->setStartPoint(mid);
                copyShape->rotate(r);

                Core::Math::Vec4f shapeBounds = copyShape->getBounds();
                //LINFO(mid<< " " <<copyShape->getMidPoint() << " " <<shapeBounds << " "<< b.minLon << " "<< b.maxLon << " "<< b.minLat << " "<< b.maxLat);

                std::shared_ptr<PathFinder> pathFinder = std::make_shared<PathFinder>(_osmmap,
                                                                                      copyShape,id);

                pathFinder->setMetricParameter( ui->m1->text().toDouble(),
                                                ui->m2->text().toDouble(),
                                                ui->m3->text().toDouble(),
                                                ui->m4->text().toDouble());

                id++;
                _finderList.push_back(pathFinder);
                _resultarray.push_back(0.0f);
                _resultarrayDirLength.push_back(0.0f);
                _resultarrayRot.push_back(0.0f);
                _finderBounds.push_back(Core::Math::Vec4d(x-deltaX*2.0/3.0,
                                                          x+deltaX*2.0/3.0,
                                                          y-deltaY*2.0/3.0,
                                                          y+deltaY*2.0/3.0));

                _dummyPath.push_back(nullptr);
                _dummyShape.push_back(nullptr);

                countF++;
                if(countF >= 500){
                    LINFO("number of finders: " <<_finderList.size());
                    countF = 0;
                }
            }
        }
    }
    LINFO("number of finders: " <<_finderList.size());

    _keepRunning=true;
    _minG = 99999.0;
    _maxG = 0;
    _timer.start();
    _lastSave = 0;
}

void MainWindow::updatePaint(){
    repaint();
    qApp->processEvents();
}

float f = 0.005f;
bool b = true;
void MainWindow::on_drawButton_clicked()
{
    if(ui->openGLWidget->getShape() != nullptr){
        ui->openGLWidget->getShape()->saveShape("shape.csv");
        LINFO("saved shape");
    }
}

void MainWindow::on_resetbutton_clicked()
{
    ui->openGLWidget->resetShape();
}

void MainWindow::on_distanceSlider_sliderReleased()
{
    ui->m1->setText(QString::number(ui->distanceSlider->value()));
    //on_pushButton_clicked();
}

void MainWindow::on_riemannSlider_sliderReleased()
{
    ui->m2->setText(QString::number(ui->riemannSlider->value()));
    //on_pushButton_clicked();
}

void MainWindow::on_lengthSlider_sliderReleased()
{
    ui->m3->setText(QString::number(ui->lengthSlider->value()));
    //on_pushButton_clicked();
}


void MainWindow::on_ratioSlider_sliderReleased()
{
    ui->m4->setText(QString::number(ui->ratioSlider->value()));
}


void MainWindow::keyPressEvent(QKeyEvent* event){
    if(event->key() == Qt::Key_Alt)
        ui->openGLWidget->toggleDraw(true);
    if(event->key() == Qt::Key_B)
        ui->openGLWidget->toggleCoord(true);
    if(event->key() == Qt::Key_Space){
        LDEBUG("Space Down");
        ui->openGLWidget->toggleMove(true);
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent* event){
    if(event->key() == Qt::Key_Alt)
        ui->openGLWidget->toggleDraw(false);
    if(event->key() == Qt::Key_B)
        ui->openGLWidget->toggleCoord(false);
    if(event->key() == Qt::Key_Space){
        LDEBUG("Space Up");
        ui->openGLWidget->toggleMove(false);
        on_pushButton_clicked();
    }
}

void MainWindow::on_loadshape_clicked()
{
    ui->openGLWidget->loadShape("shape.csv");
}

int level = 0;
void MainWindow::saveRawFile(){
    //float* packedArray = new float[_resultarray.size()];
    std::ofstream OutFile;
    std::ofstream OutFileDir;
    std::ofstream OutFileRot;

    std::string filename = "volumeGValue_"+std::to_string(volRes)+"_"+std::to_string(volRes)+"_"+std::to_string((int)rotSegmentCount)+".raw";
    std::string filenameDir = "volumeDir"+std::to_string(volRes)+"_"+std::to_string(volRes)+"_"+std::to_string((int)rotSegmentCount)+".raw";
    std::string filenameRot = "volumeRot"+std::to_string(volRes)+"_"+std::to_string(volRes)+"_"+std::to_string((int)rotSegmentCount)+".raw";

    OutFile.open(filename, ios::out | ios::binary);
    OutFileDir.open(filenameDir, ios::out | ios::binary);
    OutFileRot.open(filenameRot, ios::out | ios::binary);

    for(int i = 0; i < _resultarray.size();++i){
        OutFile.write( (char*)&_resultarray[i], sizeof(float));
        OutFileDir.write( (char*)&_resultarrayDirLength[i], sizeof(float));
        OutFileRot.write( (char*)&_resultarrayRot[i], sizeof(float));
    }

    OutFile.close();
    OutFileDir.close();
    OutFileRot.close();

    LINFO("wrote number of bytes? "<< sizeof(float)*_resultarray.size());
    LINFO("wrote file raw file: dimensions ["<< volRes<<","<<volRes<<","<<rotSegmentCount<<"]");

    //create a 2D raw file!
    std::vector<float> twoDResultGValue;
    std::vector<float> twoDResultDir;
    std::vector<float> twoDResultRot;
    for(int i = 0; i < volRes*volRes;i++){
        twoDResultGValue.push_back(0);
        twoDResultDir.push_back(0);
        twoDResultRot.push_back(0);
    }

    for(int i = 0; i < twoDResultGValue.size();++i){
        for(int k = 0; k < rotSegmentCount;k++){
            twoDResultGValue[i] += _resultarray[i+(volRes*volRes*k)];
            twoDResultDir[i] += _resultarrayDirLength[i+(volRes*volRes*k)];
            twoDResultRot[i] += _resultarrayRot[i+(volRes*volRes*k)];
        }
    }

    std::string filename2DG = "2DGValue"+std::to_string(volRes)+"_"+std::to_string(volRes)+"_2D.raw";
    std::string filename2DD = "2DDir"+std::to_string(volRes)+"_"+std::to_string(volRes)+"_2D.raw";
    std::string filename2DR = "2DRot"+std::to_string(volRes)+"_"+std::to_string(volRes)+"_2D.raw";

    OutFile.open(filename2DG, ios::out | ios::binary);
    OutFileDir.open(filename2DD, ios::out | ios::binary);
    OutFileRot.open(filename2DR, ios::out | ios::binary);

    for(int i = 0; i < twoDResultGValue.size();++i){
        OutFile.write( (char*)&twoDResultGValue[i], sizeof(float));
        OutFileDir.write( (char*)&twoDResultDir[i], sizeof(float));
        OutFileRot.write( (char*)&twoDResultRot[i], sizeof(float));
    }

    OutFile.close();
    OutFileDir.close();
    OutFileRot.close();


    if(level < 10){
        level++;
        float minLocationValue = 9999999999.0f;
        float minIndex = 0;
        for(int i = 0; i < twoDResultGValue.size();i++){
            if(twoDResultGValue.at(i) != 0 &&
                    twoDResultGValue.at(i) < minLocationValue){
                minLocationValue = twoDResultGValue.at(i);
                minIndex = i;
            }
        }
        Core::Math::Vec4d bounds = _finderBounds.at(minIndex);
        ui->openGLWidget->addShapeAndRoute(_dummyShape[minIndex],_dummyPath[minIndex]);
        LINFO("min index "<< minIndex << " min value "<< minLocationValue);
        LINFO("do a restart! "<< bounds);

        traceVolume(bounds.x,
                    bounds.y,
                    bounds.z,
                    bounds.w);

    }
}
