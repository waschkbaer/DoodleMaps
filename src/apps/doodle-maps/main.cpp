#include "mainwindow.h"

#include <QApplication>
#include <QSurfaceFormat>

#include "mocca/base/Memory.h"
#include "mocca/log/ConsoleLog.h"
#include "mocca/log/HTMLLog.h"
#include "mocca/log/LogManager.h"

#include <data/node.h>

// Start of the app

void initLogger() {
    using mocca::LogManager;
    LogManager::initialize(LogManager::LogLevel::Debug, true);
    auto console = new mocca::ConsoleLog();
    LogMgr.addLog(console);
}


int main(int argc, char *argv[])
{

    initLogger();
    /*
    std::vector<std::shared_ptr<Node>> nodes;
    nodes.push_back(std::make_shared<Node>(2,8));
    nodes.push_back(std::make_shared<Node>(6,3));
    nodes.push_back(std::make_shared<Node>(3,4));
    nodes.push_back(std::make_shared<Node>(4,6));
    nodes.push_back(std::make_shared<Node>(1,9));
    kdTree tree;

    tree.generateKdTree(nodes,0,nodes.size()-1,0);
    tree.treePrint();
    */

    QSurfaceFormat format;

    format.setMajorVersion( 4 ); //whatever version
    format.setMinorVersion( 1 ); //
#ifdef DETECTED_OS_APPLE
    format.setProfile(QSurfaceFormat::CoreProfile);
#else
    format.setProfile(QSurfaceFormat::CompatibilityProfile);
#endif   
    QSurfaceFormat::setDefaultFormat(format);

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();

}
