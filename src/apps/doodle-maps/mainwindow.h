#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "processing/pathfinder.h"
#include "data/osmmap.h"
#include "data/shape.h"
#include "processing/pathManager.h"
#include <silverbullet/time/Timer.h>

#include <QMainWindow>
#include <QImage>
#include <QPainter>
#include <QTimer>

#include <memory>
#include <mutex>


// Main Window UI

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void drawMap();
    void loadMap();
    void prepareVectorShape();

    void updatePaint();

    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);

private slots:
    void on_loadButton_clicked();

    void on_pushButton_clicked();

    void on_drawButton_clicked();

    void on_resetbutton_clicked();

    void on_distanceSlider_sliderReleased();

    void on_riemannSlider_sliderReleased();

    void on_lengthSlider_sliderReleased();

    void pathSlot();

    void on_ratioSlider_sliderReleased();

    void on_loadshape_clicked();

private:
    void traceShape();
    void traceVolume(double minLatF = 0,
                     double maxLatF = 0,
                     double minLonF = 0,
                     double maxLonF = 0);

    void shapeSlot();
    void volumeSlot();

    void saveRawFile();

private:
    Ui::MainWindow                                  *ui;

    std::unique_ptr<QImage>                         _mapimage;
    std::unique_ptr<QImage>                         _VectorShapeimage;
    std::unique_ptr<QImage>                         _blendimage;
    std::shared_ptr<QPainter>                       _painter;

    std::shared_ptr<OSMMap>                         _osmmap;
    std::mutex                                      _paintmutex;
    double                                          _prevRot;
    double                                          _prevGValue;

    QTimer                                          _pathTimer;

    double                                          _minPathResult;

    std::vector<std::shared_ptr<PathFinder>>        _finderList;
    std::vector<std::shared_ptr<PathFinder>>        _doneList;
    std::vector<std::shared_ptr<PathFinder>>        _activeList;
    int                                             _activeMaxCount;
    unsigned long                                   _finderIndex;
    bool                                            _keepRunning;
    std::vector<std::shared_ptr<PathManager>>       _pathManager;
    QImage*                                         _GValueImage;
    double                                          _minG;
    double                                          _maxG;
    Core::Time::Timer                               _timer;
    double                                          _lastSave;
    std::vector<float>                              _resultarray;
    std::vector<float>                              _resultarrayDirLength;
    std::vector<float>                              _resultarrayRot;
    std::vector<Core::Math::Vec4d>                  _finderBounds;

    std::vector<std::shared_ptr<VectorShape>>       _dummyPath;
    std::vector<std::shared_ptr<VectorShape>>       _dummyShape;
};

#endif // MAINWINDOW_H
