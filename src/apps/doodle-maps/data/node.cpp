#include "node.h"
#include <mocca/log/Log>
#include <mocca/base/StringTools.h>

Node::Node():
    id(0),
    _polarCoord(0,0),
_leftTree(nullptr),
_rightTree(nullptr),
_parent(nullptr),
_depth(0),
neighbours(){

}

Node::Node(float lo, float la):
    id(0),
    _polarCoord(lo,la),
    _from(nullptr){

}

Node::Node(long i, float lo, float la):
    id(i),
    _polarCoord(lo,la){

}

void Node::setId(long i){
    id = i;
}

void Node::setLon(float f){
    _polarCoord.x = f;
}

void Node::setLat(float f){
    _polarCoord.y = f;
}

long Node::getId() const{
    return id;
}
float Node::getLon() const {
    return _polarCoord.x;
}
float Node::getLat() const {
    return _polarCoord.y;
}

Core::Math::Vec2f Node::getCoord() const {
    return _polarCoord;
}

void Node::addNeighbour(std::shared_ptr<Node> nodePtr){
    for(std::shared_ptr<Node> n : neighbours){
        if(n->getId() == nodePtr->getId()) return;
    }
    neighbours.push_back(nodePtr);
}

void Node::removeNeighbour(std::shared_ptr<Node> nodePtr){
    for(int i = 0; i < neighbours.size();i++){
        if(neighbours.at(i)->getId() == nodePtr->getId()){
            neighbours.erase(neighbours.begin()+i);
            return;
        }
    }
}


const int Node::getNeighbourCount() const{
    return neighbours.size();
}

std::string Node::serialize()
{

    std::string s = "{\n";
    s += "\"Type\":\"Node\",\n";
    s += "\"id\":\""+ std::to_string(getId()) + "\",\n";
    s += "\"lon\":\"" + std::to_string(getLon()) + "\",\n";
    s += "\"lat\":\"" + std::to_string(getLat()) + "\",\n";
    s += "\"neighbour\":[";
        for(int i = 0; i < getNeighbourCount();++i){
            s += "\""+std::to_string(getNeighbour(i)->getId())+"\"";
            if(i != getNeighbourCount()-1){
                s += ",";
            }
        }
    s += "],\n";
    if(getLeft() != nullptr)
    s += "\"left\":\""+ std::to_string(getLeft()->getId())+ "\",\n";
    else
    s += "\"left\":\"NONE\",\n";

    if(getRight() != nullptr)
    s += "\"right\":\""+ std::to_string(getRight()->getId())+ "\",\n";
    else
    s += "\"right\":\"NONE\",\n";

    if(getParent() != nullptr)
    s += "\"parent\":\""+ std::to_string(getParent()->getId())+ "\"\n";
    else
    s += "\"parent\":\"NONE\"\n";

    s += "}";
    return s;
}

void Node::deserialize(std::string s)
{
    std::vector<std::string> lines = mocca::splitString<std::string>(s,'\n');
    std::vector<std::string> param;

    for(std::string line: lines){
        param = mocca::splitString<std::string>(line,'\"');
        //extract id;
        if(line.find("id") != std::string::npos){
              setId(std::stoi(param.at(param.size()-2)));
        }
        if(line.find("lon") != std::string::npos){
              setLon(std::stof(param.at(param.size()-2)));
        }
        if(line.find("lat") != std::string::npos){
              setLon(std::stof(param.at(param.size()-2)));
        }

    }
}


bool xAxisSortNode(const std::shared_ptr<Node>& a,const  std::shared_ptr<Node>& b){return a->getCoord().x < b->getCoord().x;}
bool yAxisSortNode(const std::shared_ptr<Node>& a,const  std::shared_ptr<Node>& b){return a->getCoord().y < b->getCoord().y;}

static unsigned int counter = 0;
std::shared_ptr<Node> kdTree::generateKdTree(std::vector<std::shared_ptr<Node>> nodes,
                    uint32_t s, uint32_t e, int depth){

    std::vector<std::shared_ptr<Node>> _nodes = nodes;


    int length = e-s;
    int mid = (length/2) + s ;
    LINFO(counter);
    counter++;

    if(depth > _MaxDepth) _MaxDepth = depth;

    if(length == 0){
        return _nodes.at(s);
    }

    int i = depth % 2;

    switch (i) {
    case 0:
        std::sort(_nodes.begin()+s,_nodes.begin()+e+1,xAxisSortNode);
        break;
    case 1:
        std::sort(_nodes.begin()+s,_nodes.begin()+e+1,yAxisSortNode);
        break;
    }

    std::shared_ptr<Node> currentNode = _nodes.at(mid);
   if(depth == 0) _root = currentNode;


    _nodes.at(s)->setDepth(depth);
    //left
    if(s-(mid) > 0){
        //LINFO("start left s "<< s << " e  "<< (mid-1)<< " d "<< (depth+1)<< " m "<<mid);
        currentNode->setLeft(generateKdTree(_nodes,s,mid-1,depth+1));
        if(currentNode->getLeft() != nullptr){
            currentNode->getLeft()->setParent(currentNode);
        }
    }else{
        //LINFO("start left s "<< s << " e  "<< s<< " d "<< (depth+1)<< " m "<<mid);
        currentNode->setLeft(nullptr);
    }

    //right
    if((mid)-e > 0){
        //LINFO("start right s "<< (mid+1) << " e  "<< e << " d "<< (depth+1)<< " m "<<mid);
        currentNode->setRight(generateKdTree(_nodes,(mid+1),e,depth+1));

        if(currentNode->getRight() != nullptr){
            currentNode->getRight()->setParent(currentNode);
        }
    }else{
        //LINFO("start right s "<< e << " e  "<< e << " d "<< (depth+1)<< " m "<<mid);
        currentNode->setRight(nullptr);
    }
    return currentNode;
}

void print(std::shared_ptr<Node> parent){
    if(parent != NULL){
        int i = parent->getDepth() % 2;
        switch (i) {
        case 0:
            LINFO("\t\t"<<parent->getCoord()<< "  x parent");
             break;
        case 1:
            LINFO("\t\t"<<parent->getCoord()<< "  y parent");
             break;
    }

    }
    if(parent->getLeft() != NULL)
    LINFO(parent->getLeft()->getCoord());
    if(parent->getRight() != NULL)
    LINFO("\t\t\t\t"<<parent->getRight()->getCoord());

    if(parent->getLeft() != NULL)
        print(parent->getLeft());

    if(parent->getRight() != NULL)
        print(parent->getRight());
}

void kdTree::treePrint(){
     /*LINFO(_root->getNode()->getCoord());
     kdNode* n = _root->getLeft();
     while(n != NULL){
        LINFO(n->getNode()->getCoord());
        n = n->getLeft();
     }*/
    print(_root);
}

std::shared_ptr<Node> kdTree::getClosesNode(Core::Math::Vec2f n ){
    std::shared_ptr<Node> tn = _root;
    bool isX = true;
    bool atBottom = false;

    float distance = 99999.0f;
    std::shared_ptr<Node> minNode = nullptr;

    while(!atBottom){
        float f = (tn->getCoord()-n).length();
        if(f < distance){
            distance = f;
            minNode = tn;
        }

        if(n.x < tn->getCoord().x && isX){
            if(tn->getLeft() == nullptr){atBottom = true; continue;}
            tn = tn->getLeft();
            isX = !isX;
            continue;
        }else if (n.x >= tn->getCoord().x && isX){
            if(tn->getRight() == nullptr){atBottom = true; continue;}
            tn = tn->getRight();
            isX = !isX;
            continue;
        }

        if(n.y < tn->getCoord().y && !isX){
            if(tn->getLeft() == nullptr){atBottom = true; continue;}
            tn = tn->getLeft();
             isX = !isX;
            continue;
        }else if (n.y >= tn->getCoord().y && !isX){
            if(tn->getRight() == nullptr){atBottom = true; continue;}
            tn = tn->getRight();
            isX = !isX;
            continue;
        }

    }
    return minNode;
}
