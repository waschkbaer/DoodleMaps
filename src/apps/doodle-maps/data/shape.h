#ifndef VectorShape_H
#define VectorShape_H

#include <vector>
#include <silverbullet/math/Vectors.h>
#include <iostream>

class VectorShapePoint
{
public:
    VectorShapePoint():_point(0,0),_pointWorld(0,0,1),_id(0){};
    VectorShapePoint(float xPos, float yPos):_point(xPos,yPos),_pointWorld(xPos,yPos,1),_id(0){};
    VectorShapePoint(Core::Math::Vec2f point):_point(point),_pointWorld(point.x,point.y,1),_id(0){};
    ~VectorShapePoint(){};

    const float& getX() const {return _point.x;}
    const float& getY() const {return _point.y;}
    const uint32_t& getId() const {return _id;}

    std::shared_ptr<VectorShapePoint> next(){ return _next; }
    void setNext(std::shared_ptr<VectorShapePoint> next){ _next = next; }

    const Core::Math::Vec2f getWorldPos() const { return _pointWorld.xy();}
    const Core::Math::Vec2f getPos() const {return _point;}

    void setWorldPos(Core::Math::Vec2f p){ _pointWorld.x = p.x; _pointWorld.y = p.y;}

    void resetWorldPos(){
        _pointWorld = Core::Math::Vec3f(_point.x,_point.y,1);
    }

    void transform(Core::Math::Mat3f& mat){
        _pointWorld =  _pointWorld*mat;
    }

private:
    Core::Math::Vec2f                       _point;
    Core::Math::Vec3f                       _pointWorld;
    uint32_t                                _id;
    std::shared_ptr<VectorShapePoint>       _next;
};

class VectorShape
{
public:
    VectorShape();
    ~VectorShape();

    void addPoint(VectorShapePoint s);
    void addPoint(float x,float y);

    std::shared_ptr<VectorShapePoint> begin();
    std::shared_ptr<VectorShapePoint> next();
    std::shared_ptr<VectorShapePoint> last();
    int currentPosition();

    const uint32_t numberOfPoints() const {return _pointList.size();}

    void transform(Core::Math::Mat3f mat);
    void transforms(std::vector<Core::Math::Mat3f> mats);

    void translate(Core::Math::Vec2f direction);
    void rotate(float degree);
    void scale(Core::Math::Vec2f scale);

    void calcMidPoint();
    Core::Math::Vec2f getMidPoint();

    void print();

    std::shared_ptr<VectorShape> copyShape(){
        std::shared_ptr<VectorShape> shape = std::make_shared<VectorShape>();
        for(std::shared_ptr<VectorShapePoint> p : _pointList){
            shape->addPoint(p->getX(),p->getY());
        }
        shape->calcMidPoint();
        shape->calculateBounds();
        return shape;
    }

    void setMidPoint(Core::Math::Vec2f mid){
        for(std::shared_ptr<VectorShapePoint> p : _pointList){
           Core::Math::Vec2f d = p->getPos()-_midPoint;
           p->setWorldPos(d+mid);
        }
        calcMidPoint();
        calculateBounds();
    }

    void setStartPoint(Core::Math::Vec2f start){
        for(std::shared_ptr<VectorShapePoint> p : _pointList){
           Core::Math::Vec2f d = p->getPos()-_pointList.at(0)->getPos();
           p->setWorldPos(d+start);
        }
        calcMidPoint();
        calculateBounds();
    }

    Core::Math::Vec4f getBounds() { return _bounds;}

    void calculateBounds();
    void calculateLength();
    double getLength() {return _length;}

    void saveShape(const std::string& filename);
    void loadShape(const std::string& filename);

    void subdivide(double segmentLength = 0.0001f);
private:
    std::vector<std::shared_ptr<VectorShapePoint>>  _pointList;
    int                                             _currentPosition;
    Core::Math::Vec2f                               _midPoint;
    Core::Math::Vec4f                               _bounds; // minx miny max maxy
    double                                          _length;
};

#endif // VectorShape_H
