#include "osmmap.h"
#include <algorithm>
#include <mocca/log/Log>
#include <silverbullet/time/Timer.h>
#include <mocca/base/StringTools.h>

OSMMap::OSMMap():
    _bounds(),
    _visbleBounds(),
    _gpuData(nullptr),
    _tree(nullptr),
    _sortedNodeVector(),
    _treeThread(),
    _treeDone(false)
{

}

OSMMap::~OSMMap(){
    _treeThread.join();
}

void OSMMap::loadMap(std::string filename){
    _filename = filename;
    std::map<uint64_t,std::shared_ptr<Node> > 	_nodeHashMap;
    _nodeHashMap.clear();
    _sortedNodeVector.clear();
    bool bProcLoaded = loadProcFile(filename);

    uint64_t maxId = 0;
    if(!bProcLoaded){
        LINFO("Could not open proc file, load osm");
        std::string s = readFile(filename);


        char* c = (char*)s.c_str();
        xml_document<> doc;    // character type defaults to char
        doc.parse<0>(c);    // 0 means default parse flags

        xml_node<> *node = doc.first_node(doc.first_node()->name());

        int streetcounter = 0;
        for (xml_node<> *child = node->first_node(); child; child = child->next_sibling())
        {
            //if we find a node
            if (internal::compare(child->name(), child->name_size(), "node", internal::measure("node"), true)){
                Node n;

                for (xml_attribute<> *attr = child->first_attribute(); attr; attr = attr->next_attribute())
                {
                    if (internal::compare(attr->name(), attr->name_size(), "id", internal::measure("id"), true)){
                        n.setId(std::atol(attr->value()));
                    }
                    if (internal::compare(attr->name(), attr->name_size(), "lat", internal::measure("lat"), true)){
                        n.setLat(std::atof(attr->value()));
                    }
                    if (internal::compare(attr->name(), attr->name_size(), "lon", internal::measure("lon"), true)){
                        n.setLon(std::atof(attr->value()));
                    }
                }
                Core::Math::Vec2f coord = n.getCoord();
                //coord.x = coord.x *std::cos((coord.y * M_PI) / (180.0));

                n.setLon(coord.x);

                if(n.getId() > maxId) maxId = n.getId();
                std::pair<uint64_t,std::shared_ptr<Node> > p = std::make_pair(n.getId(),std::make_shared<Node>(n));
                _nodeHashMap.insert(p);
            }

            //if we find a way
            if (internal::compare(child->name(), child->name_size(), "way", internal::measure("way"), true)){
                bool isWay = false;
                bool hasName = false;
                std::vector<uint64_t> ids;
                for (xml_node<> *waychild = child->first_node(); waychild; waychild = waychild->next_sibling())
                {
                    if (internal::compare(waychild->name(), waychild->name_size(), "tag", internal::measure("tag"), true)){
                        xml_attribute<> *k = waychild->first_attribute();
                        xml_attribute<> *v = k->next_attribute();

                        std::string kValue(k->value());
                        if(kValue == "highway"){
                            isWay = true;
                        }
                        if(kValue == "name"){
                            hasName = true;
                        }
                    }

                    if (internal::compare(waychild->name(), waychild->name_size(), "nd", internal::measure("nd"), true)){
                        xml_attribute<> *k = waychild->first_attribute();
                        uint64_t id= std::atol(k->value());
                        ids.push_back(id);
                    }


                }
                //if(isWay && hasName){
                if(isWay){
                    streetcounter++;
                    for(int i = 0; i < ids.size()-1;++i){
                        std::shared_ptr<Node> n1 = _nodeHashMap.find(ids[i])->second;
                        std::shared_ptr<Node> n2 = _nodeHashMap.find(ids[i+1])->second;

                        n1->addNeighbour(n2);
                        n2->addNeighbour(n1);
                    }
                }
            }
        }
        std::vector<uint64_t> removeIds;
        for(auto it = _nodeHashMap.begin(); it != _nodeHashMap.end(); it++){
            if(it->second->getNeighbourCount() == 0){
                removeIds.push_back(it->first);
            }
        }

        for(int i = 0; i < removeIds.size();++i){
            _nodeHashMap.erase(removeIds[i]);
        }
        removeIds.clear();

//subdivide street network
 /*
        LINFO("size of node hashMap before subdivision: "<< _sortedNodeVector.size());
        int counter = 0;

        double segmentLength = 0.00002;
        for(auto it = _nodeHashMap.begin();it != _nodeHashMap.end();it++){
            std::shared_ptr<Node> startNode = it->second;
            _sortedNodeVector.push_back(startNode);
            Core::Math::Vec2d startCoord(startNode->getCoord().x,startNode->getCoord().y);
            counter++;

            //get original neighbours
            std::vector<std::shared_ptr<Node>> neighbours;
            for(int j = 0; j < startNode->getNeighbourCount();j++){
                neighbours.push_back(startNode->getNeighbour(j));
            }

            for(int j = 0; j < neighbours.size();j++){
                std::shared_ptr<Node> endNode = neighbours.at(j);

                Core::Math::Vec2d endCoord(endNode->getCoord().x,endNode->getCoord().y);

                double neighbourLength = (endCoord - startCoord).length();

                //to large segment between start node and end node, do some subdivision!
                if(neighbourLength >= segmentLength){
                    LINFO("should do subdivision "<< counter << " elements in nodemap "<< _sortedNodeVector.size());

                    startNode->removeNeighbour(endNode);
                    endNode->removeNeighbour(startNode);

                    Core::Math::Vec2d dir = endCoord-startCoord;
                    dir.normalize();
                    dir *= segmentLength;

                    std::shared_ptr<Node> prev = startNode;
                    Core::Math::Vec2d currentCoord = startCoord+dir;
                    double numberSegement = neighbourLength/segmentLength;
                    int countseg = 0;
                    LINFO("segnum: " <<numberSegement<< " in int "<< (int)numberSegement);
                    for(double segmentMultiplier = 1; segmentMultiplier < (int)numberSegement; segmentMultiplier += 1.0){
                        currentCoord = startCoord+ (segmentMultiplier * dir);

                        std::shared_ptr<Node> nextNode = std::make_shared<Node>();
                        nextNode->setLat(currentCoord.y);
                        nextNode->setLon(currentCoord.x);

                        //_nodeHashMap.insert(std::make_pair(maxId,nextNode));
                        _sortedNodeVector.push_back(nextNode);

                        prev->addNeighbour(nextNode);
                        nextNode->addNeighbour(prev);

                        prev = nextNode;
                        countseg++;
                    }
                    LINFO("segnumCoun: " <<countseg);
                    prev->addNeighbour(endNode);
                    endNode->addNeighbour(prev);
                }else{
                    LINFO("skip subdivision");
                }
            }
        }
        LINFO("size of node hashMap after subdivision: "<< _sortedNodeVector.size());
*/
    }
    //------------------ NO HASH MAP NEEDED ANYMORE!???!?!?!

    float minLat =  9998118.0f;
    float maxLat = -9998118.0f;
    float minLon =  9998118.0f;
    float maxLon = -9998118.0f;


   for(auto it = _nodeHashMap.begin(); it != _nodeHashMap.end(); it++){
        _sortedNodeVector.push_back(it->second);
    }

    for(std::shared_ptr<Node> n : _sortedNodeVector){
        minFunction(minLon,n->getLon());
        maxFunction(maxLon,n->getLon());

        minFunction(minLat,n->getLat());
        maxFunction(maxLat,n->getLat());
    }

    _bounds.minLat = minLat;
    _bounds.maxLat = maxLat;
    _bounds.minLon = minLon;
    _bounds.maxLon = maxLon;

    _visbleBounds = _bounds;

    _gpuData = std::make_shared<GPUMap>();


    std::cout << "Start creating GPU structure" << std::endl;

    //for(auto it = _nodeHashMap.begin(); it != _nodeHashMap.end(); it++){
    for(std::shared_ptr<Node> n : _sortedNodeVector){

        for(int i = 0; i < n->getNeighbourCount();i++){
            std::shared_ptr<Node> nn = n->getNeighbour(i);
/*
            bool inList = false;
            for(std::pair<std::shared_ptr<Node>,std::shared_ptr<Node>> p : dList){
                if( (p.first == n && p.second == nn) ||
                    (p.second == n && p.first == nn)){
                    inList = true;
                }
            }*/

            //if(!inList){
                _gpuData->insertData(nn->getCoord().x,nn->getCoord().y);
                _gpuData->insertData(n->getCoord().x,n->getCoord().y);

                //dList.push_back(std::make_pair(n,nn));
            //}
        }
    }
    std::cout << "gpu vertex map elements " << _gpuData->numVertex() << std::endl;
    std::cout << "gpu index  map elements " << _gpuData->numIndex() << " " << (_gpuData->numIndex()/2) << std::endl;

    _treeDone = true;
    if(!bProcLoaded){
        _treeDone = false;
        _treeThread = std::thread(&OSMMap::createKDTree,this);
    }
}

std::string OSMMap::readFile(std::string filename){
    string file = " ";
    string line;
    ifstream myfile (filename.c_str());
    if (myfile.is_open())
    {
        while ( getline (myfile,line) )
        {
          file += line;
        }
        myfile.close();
    }
    else cout << "Unable to open file";

    return file;
}

std::string OSMMap::toString(){
    std::string output;
    output += "MapBounds    - lon ("+std::to_string(_bounds.minLon)+"/"+std::to_string(_bounds.maxLon)+") lat ("+std::to_string(_bounds.minLat)+"/"+std::to_string(_bounds.maxLat)+")\n";
    output += "HashMap Info - Number of street nodes : "+ std::to_string(_sortedNodeVector.size());
    return output;
}


void OSMMap::minFunction(float &target, float test){
    if(target > test){
        target = test;
    }
}

void OSMMap::maxFunction(float &target, float test){
    if(target < test){
        target = test;
    }
}

std::shared_ptr<Node> OSMMap::getClosesNode(const Core::Math::Vec2f& coord)
{
    Core::Time::Timer t;
    if(_treeDone){
        //LERROR("[OSMMap::getClosesNode] kdsearch "<< coord);
        t.start();
        std::shared_ptr<Node> close = _tree->getClosesNode(coord);
        //LDEBUG("[OSMMap::getClosesNode] kdsearch time "<< t.elapsed());
        return close;
    }else{
        t.start();
        float distance = 99999999.0f;
        std::shared_ptr<Node> n = nullptr;
        for(std::shared_ptr<Node> cn : _sortedNodeVector){
            float deltaX = cn->getLon()-coord.x;
            float deltaY = cn->getLat()-coord.y;

            float curDis = std::sqrt(deltaX*deltaX+deltaY*deltaY);
            if(curDis < distance){
                distance = curDis;
                n = cn;
            }
        }
        return n;
    }
}

void OSMMap::createKDTree(){
    LINFO("--------------- kdtree buildup");
    _tree = std::make_shared<kdTree>();
    _tree->generateKdTree(_sortedNodeVector,0,_sortedNodeVector.size()-1,0);
    LINFO("--------------- kdtree buildup end");

    for(unsigned int i = 0; i < _sortedNodeVector.size();++i){
        _sortedNodeVector.at(i)->setId(i);
    }
    _treeDone = true;
    saveFile(_filename);
}

void OSMMap::saveFile(std::string filename){
    std::string realFile = filename+".proc";
    LINFO("[OSMMap::saveFile] save file "<< realFile);
    std::ofstream file;
    file.open(realFile);
    file << "{\n";
    file << "\"nodecount\":\""<< std::to_string(_sortedNodeVector.size())<< "\",\n";
    file << "\"treeRoot\":\""<<std::to_string(_tree->getRoot()->getId()) <<"\",\n";
    file << "\"nodes\":[\n";

    for(int i = 0; i < _sortedNodeVector.size();++i){
        file << _sortedNodeVector.at(i)->serialize();
        if(i != _sortedNodeVector.size()-1){
            file << ",\n";
        }else{
            file<<"\n";
        }
    }
    file << "]\n";
    file <<"}";
    file.close();
}

bool OSMMap::loadProcFile(std::string filename){
    std::string realFile = filename+ ".proc";
    LINFO("[OSMMap::loadProcFile] load file "<< realFile);
    std::ifstream file;
    file.open(realFile);
    std::string line = "";
    std::vector<std::string> param;
    _sortedNodeVector.clear();

    if(file.is_open()){
        std::getline(file,line); // {

        std::getline(file,line); // nodecount
        param = mocca::splitString<std::string>(line,'\"');
        uint32_t count = std::stoi(param.at(param.size()-2));
        LDEBUG("NodeCount : " << count);
        for(int i = 0; i < count;++i){
            _sortedNodeVector.push_back(std::make_shared<Node>(i));
        }

        std::getline(file,line); // treeroot
        param = mocca::splitString<std::string>(line,'\"');
        uint32_t rootId = std::stoi(param.at(param.size()-2));
        _tree = std::make_shared<kdTree>();
        LDEBUG("RootId : " << rootId);
        _tree->setRoot(_sortedNodeVector.at(rootId));

        std::getline(file,line); // treeroot

        //read node
        for(int i = 0; i < count;++i){
            std::getline(file,line); // {
            std::getline(file,line); // type
            std::getline(file,line); // id
            param = mocca::splitString<std::string>(line,'\"');
            if(i != std::stoi(param.at(param.size()-2))) LERROR("[OSMMap::loadProcFile] id not matching i");

            std::getline(file,line); // lon
            param = mocca::splitString<std::string>(line,'\"');
            _sortedNodeVector.at(i)->setLon(std::stof(param.at(param.size()-2)));

            std::getline(file,line); // lat
            param = mocca::splitString<std::string>(line,'\"');
            _sortedNodeVector.at(i)->setLat(std::stof(param.at(param.size()-2)));

            std::getline(file,line); // neigh
            param = mocca::splitString<std::string>(line,'\"');
            bool start = false;
            for(int h = 3; h < param.size()-1;h+=2){
                try{
                    int k = std::stoi(param.at(h));
                    _sortedNodeVector.at(i)->addNeighbour(_sortedNodeVector.at(k));
                }catch(const std::exception& e){
                    LERROR("no number");
                }
            }

            std::getline(file,line); // left
            param = mocca::splitString<std::string>(line,'\"');
            if(param.at(param.size()-2) != "NONE"){
                _sortedNodeVector.at(i)->setLeft(_sortedNodeVector.at(std::stoi(param.at(param.size()-2))));
            }

            std::getline(file,line); // right
            param = mocca::splitString<std::string>(line,'\"');
            if(param.at(param.size()-2) != "NONE"){
                _sortedNodeVector.at(i)->setRight(_sortedNodeVector.at(std::stoi(param.at(param.size()-2))));
            }

            std::getline(file,line); // parent
            param = mocca::splitString<std::string>(line,'\"');
            if(param.at(param.size()-1) != "NONE"){
                _sortedNodeVector.at(i)->setParent(_sortedNodeVector.at(std::stoi(param.at(param.size()-1))));
            }
            std::getline(file,line); // parent
        }
        file.close();
        return true;
    }else{
        return false;
    }

}
