#include "shape.h"
#include "mocca/log/Log.h"

#include "iostream"
#include "fstream"
#include "mocca/base/StringTools.h"

VectorShape::VectorShape():
    _pointList(),
    _currentPosition(0),
    _midPoint(0,0),
    _bounds(999,999,-999,-999)
{

}

VectorShape::~VectorShape(){

}

void VectorShape::addPoint(VectorShapePoint s){
    std::shared_ptr<VectorShapePoint> s_ptr = std::make_shared<VectorShapePoint>(s.getX(),s.getY());
    _pointList.push_back(s_ptr);
    if(_pointList.size() >= 2)
        _pointList[_pointList.size()-2]->setNext(_pointList[_pointList.size()-1]);
    calcMidPoint();
    calculateBounds();
    calculateLength();
}

void VectorShape::addPoint(float x,float y){
    VectorShapePoint s(x,y);
    addPoint(s);
}

std::shared_ptr<VectorShapePoint> VectorShape::begin(){
    _currentPosition = 0;
    return _pointList[_currentPosition];
}

std::shared_ptr<VectorShapePoint> VectorShape::next(){
    _currentPosition = (_currentPosition+1)%_pointList.size();
    return _pointList[_currentPosition];
}

std::shared_ptr<VectorShapePoint> VectorShape::last(){
    _currentPosition = _pointList.size()-1;
    return _pointList[_currentPosition];
}

int VectorShape::currentPosition(){
    return _currentPosition;
}

void VectorShape::transform(Core::Math::Mat3f mat){
    for(std::shared_ptr<VectorShapePoint> s : _pointList){
        s->transform(mat);
    }
    calculateBounds();
    calculateLength();
}

void VectorShape::transforms(std::vector<Core::Math::Mat3f> mats){
    for(std::shared_ptr<VectorShapePoint> s : _pointList){
        s->resetWorldPos();
        for(Core::Math::Mat3f& m : mats){
            s->transform(m);
        }
    }
    calculateBounds();
    calculateLength();
}

void VectorShape::translate(Core::Math::Vec2f direction){
    for(std::shared_ptr<VectorShapePoint> s : _pointList){
        Core::Math::Vec2f pos = s->getWorldPos();
        pos.x += direction.x;
        pos.y += direction.y;
        s->setWorldPos(pos);
    }
    calculateBounds();
    calculateLength();
}

void VectorShape::rotate(float degree){
    calcMidPoint();
    Core::Math::Mat3f rotX;
    rotX.RotationZ(degree);
    //Core::Math::Vec2f rotationCenter = _midPoint;
    Core::Math::Vec2f rotationCenter = _pointList.at(0)->getWorldPos();

    for(std::shared_ptr<VectorShapePoint> s : _pointList){
        Core::Math::Vec3f p(s->getWorldPos()-rotationCenter,0);

        p = rotX*p;

        s->setWorldPos(p.xy()+rotationCenter);
    }
    calculateBounds();
    calculateLength();
}

void VectorShape::scale(Core::Math::Vec2f scale){
    calcMidPoint();
    for(std::shared_ptr<VectorShapePoint> s : _pointList){
        Core::Math::Vec2f p = s->getWorldPos()-_midPoint;

        p = p*scale;
        s->setWorldPos(p+_midPoint);
    }
    calculateBounds();
}

void VectorShape::calcMidPoint(){
    _midPoint = Core::Math::Vec2f(0,0);
    for(std::shared_ptr<VectorShapePoint> s : _pointList){
        Core::Math::Vec2f pos = s->getWorldPos();
        _midPoint += pos;
    }

    _midPoint /= _pointList.size();
    calculateBounds();
}

Core::Math::Vec2f VectorShape::getMidPoint(){
    return _midPoint;
}

void VectorShape::calculateBounds(){
    if(_pointList.size() == 0) return;
    float minX = _pointList.at(0)->getWorldPos().x;
    float maxX = _pointList.at(0)->getWorldPos().x;
    float minY = _pointList.at(0)->getWorldPos().y;
    float maxY = _pointList.at(0)->getWorldPos().y;

    for(std::shared_ptr<VectorShapePoint> s : _pointList){
        Core::Math::Vec2f pos = s->getWorldPos();

        if(pos.x < minX) minX = pos.x;
        if(pos.x > maxX) maxX = pos.x;

        if(pos.y < minY) minY = pos.y;
        if(pos.y > maxY) maxY = pos.y;
    }
    _bounds.x = minX;
    _bounds.y = minY;
    _bounds.z = maxX;
    _bounds.w = maxY;
}

void VectorShape::calculateLength(){
    std::shared_ptr<VectorShapePoint> start = _pointList.at(0);
    _length = 0;
    for(int i = 1; i < _pointList.size();++i){
        std::shared_ptr<VectorShapePoint> end = _pointList.at(i);

        Core::Math::Vec2f posStart = start->getWorldPos();
        Core::Math::Vec2f posEnd = end->getWorldPos();

        _length += (posEnd-posStart).length();
        posStart = posEnd;
    }
}

void VectorShape::saveShape(const std::string& filename)
{
    std::ofstream file;
    file.open (filename.c_str());
    for(std::shared_ptr<VectorShapePoint> p : _pointList){
        file << std::to_string(p->getWorldPos().x) << ";"<< std::to_string(p->getWorldPos().y) <<"\n";
    }
    file.close();
}

void VectorShape::loadShape(const std::string& filename)
{
    _pointList.clear();

    std::string line;
    std::ifstream file (filename.c_str());
    if (file.is_open())
    {
        while ( getline (file,line) )
        {
            std::vector<std::string> values = mocca::splitString<std::string>(line,';');
            if(values.size() != 2) continue;
            addPoint(std::stof(values[0]),std::stof(values[1]));
        }
        file.close();
    }
}

void VectorShape::subdivide(double segmentLength){
    std::vector<std::shared_ptr<VectorShapePoint>>  subList;

    for(int i = 1; i < _pointList.size();++i){
        Core::Math::Vec2f start = _pointList.at(i-1)->getWorldPos();
        Core::Math::Vec2f end = _pointList.at(i)->getWorldPos();

        Core::Math::Vec2f dir = end-start;
        double length = dir.length();
        dir.normalize();
        dir *= segmentLength;

        Core::Math::Vec2f pos = start;

        for(double l = 0; l < length; l+=segmentLength){
            subList.push_back(std::make_shared<VectorShapePoint>(pos.x,pos.y));

            if(subList.size() > 1){
                subList.at(subList.size()-2)->setNext(subList.at(subList.size()-1));
            }

            pos += dir;
        }
    }

    if(subList.size() != 0){
        _pointList = subList;
        calcMidPoint();
        calculateBounds();
        calculateLength();
    }
}

void VectorShape::print(){
    for(std::shared_ptr<VectorShapePoint> p : _pointList){
        LINFO(p->getWorldPos());
    }
}
