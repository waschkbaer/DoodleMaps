#include "priorityqueue.h"
#include <algorithm>
#include <mocca/log/Log>

PriorityQueue::PriorityQueue():
#ifdef useVector
    _queueVector()
#else
    _queueMap()
#endif
{

}

bool sortFunction(std::pair<double,std::shared_ptr<Node>> a,
                  std::pair<double,std::shared_ptr<Node>>b) {
                return a.first < b.first;
                }

void PriorityQueue::insert(double cost, std::shared_ptr<Node>& node){
#ifdef useVector
    _queueVector.push_back(std::make_pair(cost,node));
    std::sort(_queueVector.begin(),_queueVector.end(),sortFunction);
#else
    _queueMap.insert(std::make_pair(cost,node));
#endif
}

std::shared_ptr<Node> PriorityQueue::extractMin(){
#ifdef useVector
    std::shared_ptr<Node> n = _queueVector.at(0).second;
    _queueVector.erase(_queueVector.begin());
    return n;
#else
    double min = std::numeric_limits<double>::max();
    for(auto it = _queueMap.begin(); it != _queueMap.end();++it){
        if(it->first < min){
            min = it->first;
        }
    }
    std::shared_ptr<Node> n = _queueMap.find(min)->second;
    _queueMap.erase(min);
    return n;
#endif
}

void PriorityQueue::clear(){
#ifdef useVector
    _queueVector.clear();
#else
    _queueMap.clear();
#endif
}

void PriorityQueue::decreaseKey(double cost, std::shared_ptr<Node>& node){
#ifdef useVector
    int64_t index = contains(node);
    if(index > 0){
        _queueVector.erase(_queueVector.begin()+(index-1));
        insert(cost, node);
    }
#else
    if(contains(node)){
        for(auto it = _queueMap.begin(); it != _queueMap.end();++it){
            if(it->second == node){
                _queueMap.erase(it);
                insert(cost,node);
                return;
            }
        }
    }
#endif
}

int64_t PriorityQueue::contains(std::shared_ptr<Node>& node){
#ifdef useVector
    for(int i = 0; i < _queueVector.size();++i){
        if(_queueVector.at(i).second->getId() == node->getId()){
            return i+1;
        }
    }
    return 0;
#else
    for(auto it = _queueMap.begin(); it != _queueMap.end();++it){
        if(it->second->getId() == node->getId()){
            return true;
        }
    }
    return false;
#endif
}
