#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

#include <memory>

#include "data/node.h"

//#define useVector

class PriorityQueue
{
public:
    PriorityQueue();

    void insert(double cost, std::shared_ptr<Node>& node);
    void decreaseKey(double cost, std::shared_ptr<Node>& node);
    std::shared_ptr<Node> extractMin();
    void clear();

    bool isEmpty() const {
#ifdef useVector
        return _queueVector.empty();
#else
        return _queueMap.empty();
#endif
    }
    int64_t contains(std::shared_ptr<Node>& node);

    int size() {
#ifdef useVector
            return _queueVector.size();
#else
            return _queueMap.size();
#endif
    }

private:

#ifdef useVector
    std::vector<std::pair<double,std::shared_ptr<Node>>> _queueVector;
#else
    std::map<double,std::shared_ptr<Node>> _queueMap;
#endif
};

#endif // PRIORITYQUEUE_H
