#ifndef NODE_H
#define NODE_H

#include <map>
#include <memory>
#include <silverbullet/math/Vectors.h>
#include <algorithm>

class Node{
    bool operator() (Node a, Node b) {return a.getCoord().x < b.getCoord().x;}
public:
    Node();
    Node(float lo, float la);
    Node(long i, float lo = 0.0f, float la = 0.0f);

    void setId(long i);
    void setLon(float f);
    void setLat(float f);

    long getId() const;
    float getLon() const;
    float getLat() const;
    Core::Math::Vec2f getCoord() const;

    void addNeighbour(std::shared_ptr<Node> nodePtr);
    void removeNeighbour(std::shared_ptr<Node> nodePtr);
    const int getNeighbourCount() const;

    const std::shared_ptr<Node> getNeighbour(int index) const {
        if(index > getNeighbourCount()) return nullptr;
        return neighbours.at(index);
    }


    std::string serialize();
    void deserialize(std::string s);

    std::shared_ptr<Node> getLeft() {return _leftTree;}
    std::shared_ptr<Node> getRight() {return _rightTree;}
    std::shared_ptr<Node> getParent() {return _parent;}

    void setLeft(std::shared_ptr<Node> left) {_leftTree = left;}
    void setRight(std::shared_ptr<Node> right) {_rightTree = right;}
    void setParent(std::shared_ptr<Node> parent) {_parent = parent;}

    uint32_t getDepth() {return _depth;}
    void setDepth(uint32_t d){_depth = d;}

private:
    long  id;
    Core::Math::Vec2f _polarCoord;
    std::vector<std::shared_ptr<Node>>          neighbours;
    std::shared_ptr<Node>                       _from;

    std::shared_ptr<Node>                       _leftTree;
    std::shared_ptr<Node>                       _rightTree;
    std::shared_ptr<Node>                       _parent;
    uint32_t                                    _depth;
};


class kdTree{
public:
    kdTree():_MaxDepth(0),_root(0){}
    ~kdTree(){
    }

    std::shared_ptr<Node> generateKdTree(std::vector<std::shared_ptr<Node>> nodes,
                        uint32_t s, uint32_t e, int depth);

    uint32_t getMaxDepth() {return _MaxDepth;}

    void treePrint();

    std::shared_ptr<Node> getClosesNode(Core::Math::Vec2f n );

    void setRoot(std::shared_ptr<Node> n){_root = n;}
    std::shared_ptr<Node> getRoot(){return _root;}

private:
    std::shared_ptr<Node>                   _root;
    uint32_t                                _MaxDepth;
};

#endif // NODE_H
