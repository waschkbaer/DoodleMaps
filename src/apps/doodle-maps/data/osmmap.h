#ifndef OSMMAP_H
#define OSMMAP_H

#include "rapidxml.hpp"
#include "data/node.h"
#include "shape.h"

#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <memory>
#include <thread>

using namespace rapidxml;
using namespace std;


struct MapBounds{
    MapBounds():
    minLat(0),
    maxLat(0),
    minLon(0),
    maxLon(0){};

    float minLat;
    float maxLat;
    float minLon;
    float maxLon;
};

class GPUMap{
public:
    GPUMap():
        _data(),
        _index(){};
    ~GPUMap(){}

    void insertData(float x, float y){
        _data.push_back(x);
        _data.push_back(y);
        _data.push_back(0);

        _index.push_back(_index.size());
    }

    float* getData() {return &_data[0];}
    uint32_t* getIndex() {return &_index[0];}

    uint32_t numVertex() {return _data.size()/3;}
    uint32_t numIndex() {return _index.size();}

private:
    std::vector<float> _data;
    std::vector<uint32_t> _index;
};


class OSMMap{
public:
    OSMMap();
    ~OSMMap();

private:
    std::string readFile(std::string filename);
    void minFunction(float &target, float test);
    void maxFunction(float &target, float test);

    void saveFile(std::string filename);
    bool loadProcFile(std::string filename);

public:
    void loadMap(std::string filename);
    std::string toString();

    const MapBounds& getBounds() const { return _bounds; }
    const uint64_t   getNodeCount() const { return _sortedNodeVector.size(); }
    const std::shared_ptr<Node> getNode(int i) const {
        if(i > getNodeCount()) return nullptr;
        return _sortedNodeVector[i];
    }

    std::shared_ptr<Node> getClosesNode(const Core::Math::Vec2f& coord);
    std::shared_ptr<GPUMap> getGPUMap() {return _gpuData;}

    void setVisibleBounds(MapBounds b){_visbleBounds = b;}
    MapBounds getVisibleBounds() {return _visbleBounds;}

private:
    std::string                                 _filename;
    std::vector<std::shared_ptr<Node>>          _sortedNodeVector;
    MapBounds									_bounds;
    MapBounds									_visbleBounds;
    std::shared_ptr<GPUMap>                     _gpuData;
    std::shared_ptr<kdTree>                     _tree;

    std::thread                                 _treeThread;
    bool                                        _treeDone;

    void createKDTree();
};
#endif // OSMMAP_H
