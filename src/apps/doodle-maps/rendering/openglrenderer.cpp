#include <silverbullet/base/DetectEnv.h>
#ifdef DETECTED_OS_APPLE
#include <opengl-base/OpenGLincludes.h>
#else
#include <opengl-base/GL/glew.h>
#endif
#ifdef DETECTED_OS_WINDOWS
#include <Windows.h>
#endif

#include "openglrenderer.h"
#include "mocca/log/LogManager.h"
#include <QWheelEvent>
#include <QMouseEvent>
#include <QKeyEvent>


QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram);
QT_FORWARD_DECLARE_CLASS(QOpenGLTexture)

#include <silverbullet/base/DetectEnv.h>

static GLenum errorCode = 0;
#define GL(x) x; errorCode = glGetError(); if(errorCode != GL_NO_ERROR) LINFO("GL ERROR Line: "<< __LINE__ <<" ("<< __FILE__ <<") code:"<< errorCode);


#include <iostream>

float mapScaleFactor = 100.0f;

openGLrenderer::openGLrenderer(QWidget* parent):
    QOpenGLWidget(parent),
    _mapShader(nullptr),
    _shapeShader(nullptr),
    _lineShader(nullptr),
    _map(nullptr),
    _projectionMatrix(),
    _viewMatrix(),
    _transformationMatrix(),
    _boxTransformationMatrix(),
    _camera(),
    _cameraDistance(100.0f),
    _mouseDown(false),
    _lastPos(),
    _shapeKeyDown(false),
    _moveKeyDown(false),
    _shape(nullptr),
    _shapeList(),
    _routeList(),
    _osmmap(nullptr),
    _updateGLShape(false),
    _updateGLLists(false)
{

}


openGLrenderer::~openGLrenderer(){

}

void openGLrenderer::initializeGL() {
  makeCurrent();
  initializeOpenGLFunctions();

  GL(LINFO("[openGLRenderer:Init] init qt opengl functions"));

#ifdef DETECTED_OS_LINUX
  glewExperimental=GL_TRUE;
  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
    LERROR("glew not init");
  }else{
      LINFO("[openGLRenderer:Init] GLEW_OK true Vers__updateGLShapeion: "<< glewGetString(GLEW_VERSION));
  }

  GL(LINFO("[openGLRenderer:Init] check after Glew Init"));
#endif
  const char* r = (const char*)glGetString(GL_RENDERER);
  const char* v = (const char*)glGetString(GL_VERSION);

  GL(LINFO("[openGLRenderer:Init] Renderer "<< r));
  GL(LINFO("[openGLRenderer:Init] Version "<< v));

  //setup base matrices
  _projectionMatrix.Ortho(6 ,7 ,51 ,52 ,0.1f,1000.0f);
  _camera.setPosition(Core::Math::Vec3f(0,0,100));
  _camera.setLookAt(Core::Math::Vec3f(0,0,0));
  _viewMatrix = _camera.buildLookAt();
  _transformationMatrix.Scaling(mapScaleFactor,mapScaleFactor,0);

  GL(LINFO("[openGLRenderer:Init] error check after setting mvp values"));

  generateShader();

  GL(LINFO("[openGLRenderer:Init] error check after creating shaders"));
  //generateMap();

}

void openGLrenderer::paintGL() {
    if(_osmmap != nullptr) generateMap();
    if(_updateGLShape){
       _glShape = generateShape(_shape);
    }
    if(_updateGLRoute){
        setRouteShape(_route);
    }
    if(_updateGLLists){
        addShapeAndRouteGL();
    }

    GL(glCullFace(GL_BACK));
    GL(glDisable(GL_DEPTH_TEST));
    GL(glDisable(GL_BLEND));
    GL(glDisable(GL_CULL_FACE));

    GL(glClearColor(1.0f,1.0f,1.0f,1));
    GL(glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT));

    _mapShader->Enable();
    _mapShader->Set("projectionMatrix",_projectionMatrix);
    _mapShader->Set("viewMatrix",_viewMatrix);
    _mapShader->Set("worldMatrix",_transformationMatrix);
    _mapShader->Set("lineColor",Core::Math::Vec3f(0.8f,0.8f,0.8f));

    if(_map != nullptr){
        _map->paint(GL_LINES);
    }
    paintShape();
    paintLine();

    if(_glShape != nullptr){
        _mapShader->Set("lineColor",Core::Math::Vec3f(1.0f,0.0f,0.0f));
        _glShape->paint(GL_LINE_STRIP);
    }

    if(_glroute != nullptr){
        _mapShader->Set("lineColor",Core::Math::Vec3f(0.0f,0.0f,1.0f));
        _glroute->paint(GL_LINE_STRIP);
    }

    _mapShader->Disable();
    GL();
}


void openGLrenderer::paintMap(){

}

void openGLrenderer::paintShape(){
    for(std::shared_ptr<GLModel> m : _shapeList){
        _mapShader->Set("lineColor",Core::Math::Vec3f(0.8f,0.0f,0.0f));
        m->paint(GL_LINE_STRIP);
    }
}

void openGLrenderer::paintLine(){
    for(std::shared_ptr<GLModel> m : _routeList){
        _mapShader->Set("lineColor",Core::Math::Vec3f(0.0f,0.0f,0.8f));
        m->paint(GL_LINE_STRIP);
    }
}

void openGLrenderer::generateShader(){
#ifdef DETECTED_OS_LINUX
    std::vector<std::string> fs,vs;
    vs.push_back("/home/waschkbaer/DoodleMaps/shader/map_shader_vertex.glsl");
    fs.push_back("/home/waschkbaer/DoodleMaps/shader/map_shader_fragment.glsl");
    ShaderDescriptor sd(vs,fs);
    if(!loadAndCheckShaders(_mapShader,sd)) return;

#else
    std::vector<std::string> fs,vs;
    vs.push_back("/Users/waschkbaer/DoodleMaps_git/shader/map_shader_vertex.glsl");
    fs.push_back("/Users/waschkbaer/DoodleMaps_git/shader/map_shader_fragment.glsl");
    ShaderDescriptor sd(vs,fs);
    if(!loadAndCheckShaders(_mapShader,sd)) return;
#endif
}

void openGLrenderer::generateFrameBuffer(){

}

void openGLrenderer::generateMap(std::shared_ptr<GPUMap> mapdata){

    if(_osmmap != nullptr){
        _map = std::make_shared<GLModel>();
        _map->Initialize(_osmmap->getGPUMap()->getData(),
                         (int*)_osmmap->getGPUMap()->getIndex(),
                         _osmmap->getGPUMap()->numVertex(),
                         _osmmap->getGPUMap()->numIndex());
        _osmmap = nullptr;
    }

    //_glBox = std::make_shared<GLVolumeBox>(Core::Math::Vec3f(-0.005f,-0.005f,-0.005f),
    //                                       Core::Math::Vec3f(0.005f,0.005f,0.005f));
}

//generate the gpu memory shape structure
std::shared_ptr<GLModel> openGLrenderer::generateShape(std::shared_ptr<VectorShape> shape){
    //LINFO("[openGLrenderer::generateShape] should generate GLShape");
    std::shared_ptr<GLModel> glShape = std::make_shared<GLModel>();

    std::vector<float> position;
    std::vector<uint32_t> index;
    shape->subdivide();
    for(std::shared_ptr<VectorShapePoint> p = shape->begin();
        p != nullptr;
        p = p->next())
    {
        position.push_back(p->getWorldPos().x);
        position.push_back(p->getWorldPos().y);
        position.push_back(0);
        index.push_back(index.size());
    }
    glShape->Initialize(&position[0],(int*)&index[0],position.size()/3,index.size());
    _updateGLShape = false;
    return glShape;
}

void openGLrenderer::setView(Core::Math::Vec2f min,
             Core::Math::Vec2f max){

    float x = 1.0*_cameraDistance;
    float y = 0.5*_cameraDistance;

    _projectionMatrix.Ortho(-x,x,
                            -y,y,
                            0.0001f,1000.0f);


    _min = min;
    _max = max;

    Core::Math::Vec2f middle;
    middle.x = (_min.x+_max.x)/2.0f;
    middle.y = (_min.y+_max.y)/2.0f;

    _viewMatrix.BuildLookAt(Core::Math::Vec3f(middle.x*mapScaleFactor,middle.y*mapScaleFactor,0.1f),
                            Core::Math::Vec3f(middle.x*mapScaleFactor,middle.y*mapScaleFactor,0),
                            Core::Math::Vec3f(0,1,0));
}

bool openGLrenderer::loadAndCheckShaders(std::shared_ptr<GLProgram>& programPtr, ShaderDescriptor& sd){
    programPtr = std::make_shared<GLProgram>();
    programPtr->Load(sd);

    if (!programPtr->IsValid()){
        LERROR("MapRendering | programm not valid");
        return false;
    }
    return true;
}

void openGLrenderer::wheelEvent(QWheelEvent* event){

#ifdef DETECTED_OS_APPLE
    _cameraDistance += (float)event->pixelDelta().y()*0.5f;
    _cameraDistance = std::max(_cameraDistance,1.0f);
#else
    _cameraDistance += (float)event->delta()*0.03f;
    _cameraDistance = std::max(_cameraDistance,1.0f);
#endif

    float x = 1.0*_cameraDistance;
    float y = 0.5*_cameraDistance;

    _projectionMatrix.Ortho(-x,x,
                            -y,y,
                            0.0001f,1000.0f);

    Core::Math::Vec2f middle;
    middle.x = (_min.x+_max.x)/2.0f;
    middle.y = (_min.y+_max.y)/2.0f;

    _viewMatrix.BuildLookAt(Core::Math::Vec3f(middle.x*mapScaleFactor,middle.y*mapScaleFactor,0.1f),
                            Core::Math::Vec3f(middle.x*mapScaleFactor,middle.y*mapScaleFactor,0),
                            Core::Math::Vec3f(0,1,0));
    update();

    float xPr = 1.0*_cameraDistance;
    float yPr = 0.5*_cameraDistance;

    float minLo = ((middle.x*mapScaleFactor)-xPr)/mapScaleFactor;
    float maxLo = ((middle.x*mapScaleFactor)+xPr)/mapScaleFactor;

    float minLa = ((middle.y*mapScaleFactor)-yPr)/mapScaleFactor;
    float maxLa = ((middle.y*mapScaleFactor)+yPr)/mapScaleFactor;

    MapBounds m;
    m.maxLat = maxLa;
    m.minLat = minLa;
    m.maxLon = maxLo;
    m.minLon = minLo;

    if(_osmmap != nullptr){
        _osmmap->setVisibleBounds(m);
    }
}

void openGLrenderer::mousePressEvent(QMouseEvent* event){
        _mouseDown = true;
        _lastPos.x = event->pos().x();
        _lastPos.y = event->pos().y();

}

void openGLrenderer::mouseMoveEvent(QMouseEvent* event){
    if(!_mouseDown) return;

    int32_t deltaX = _lastPos.x - event->pos().x();
    int32_t deltaY = _lastPos.y - event->pos().y();

    _lastPos.x = event->pos().x();
    _lastPos.y = event->pos().y();


    if(_shapeKeyDown){
        LINFO("shape pixelpos : "<< event->pos().x() << " "<< event->pos().y());

        float xPr = 1.0*_cameraDistance;
        float yPr = 0.5*_cameraDistance;
        Core::Math::Vec2f middle;
        middle.x = (_min.x+_max.x)/2.0f;
        middle.y = (_min.y+_max.y)/2.0f;

        float minLo = ((middle.x*mapScaleFactor)-xPr)/mapScaleFactor;
        float maxLo = ((middle.x*mapScaleFactor)+xPr)/mapScaleFactor;

        float minLa = ((middle.y*mapScaleFactor)-yPr)/mapScaleFactor;
        float maxLa = ((middle.y*mapScaleFactor)+yPr)/mapScaleFactor;

        LINFO("VISIBLE "<< minLo << " "<< maxLo);
        LINFO("VISIBLE "<< minLa << " "<< maxLa);


        float x = (maxLo-minLo)*((double)event->pos().x()/(double)width()) + minLo;
        float y = (maxLa-minLa)*((double)(height()-event->pos().y())/(double)height()) + minLa;



        LINFO("polar : "<< x << " "<< y);

        if(_shape == nullptr){
            _shape = std::make_shared<VectorShape>();
        }

        Core::Math::Vec2f last(0,0);

        if(_shape->numberOfPoints() > 0)
            last = _shape->last()->getWorldPos();

        if((Core::Math::Vec2f(x,y)-last).length() > 0.00001){
         _shape->addPoint(x,y);
        }
        _updateGLShape = true;
        //_glShape = generateShape(_shape);
        update();
    }else if(_coordKeyDown){
        LINFO("shape pixelpos : "<< event->pos().x() << " "<< event->pos().y());

        float xPr = 1.0*_cameraDistance;
        float yPr = 0.5*_cameraDistance;
        Core::Math::Vec2f middle;
        middle.x = (_min.x+_max.x)/2.0f;
        middle.y = (_min.y+_max.y)/2.0f;

        float minLo = ((middle.x*mapScaleFactor)-xPr)/mapScaleFactor;
        float maxLo = ((middle.x*mapScaleFactor)+xPr)/mapScaleFactor;

        float minLa = ((middle.y*mapScaleFactor)-yPr)/mapScaleFactor;
        float maxLa = ((middle.y*mapScaleFactor)+yPr)/mapScaleFactor;

        float x = (maxLo-minLo)*((double)event->pos().x()/(double)width()) + minLo;
        float y = (maxLa-minLa)*((double)(height()-event->pos().y())/(double)height()) + minLa;

        LINFO("[OpenGlRenderer] polar coord clicked : "<< x << " "<< y);

    }else if(_moveKeyDown){
            translateShape(Core::Math::Vec2f(-(double)deltaX/10000.0,
                                             (double)deltaY/10000.0));
    }else{
        _min.x += (double)deltaX/10000.0;
        _min.y -= (double)deltaY/10000.0;

        _max.x += (double)deltaX/10000.0;
        _max.y -= (double)deltaY/10000.0;

        Core::Math::Vec2f middle;
        middle.x = (_min.x+_max.x)/2.0f;
        middle.y = (_min.y+_max.y)/2.0f;

        _viewMatrix.BuildLookAt(Core::Math::Vec3f(middle.x*mapScaleFactor,middle.y*mapScaleFactor,0.1f),
                                Core::Math::Vec3f(middle.x*mapScaleFactor,middle.y*mapScaleFactor,0),
                                Core::Math::Vec3f(0,1,0));
    }


    update();
}

void openGLrenderer::mouseReleaseEvent(QMouseEvent* event){
    _mouseDown = false;
}

void openGLrenderer::setRouteShape(std::shared_ptr<VectorShape> route){
    if(_route == nullptr) return;
    if(_glroute == nullptr){
        _glroute = std::make_shared<GLModel>();
    }

    std::vector<float> position;
    std::vector<uint32_t> index;
    for(std::shared_ptr<VectorShapePoint> p = route->begin();
        p != nullptr;
        p = p->next()){
        position.push_back(p->getWorldPos().x);
        position.push_back(p->getWorldPos().y);
        position.push_back(0);
        index.push_back(index.size());
    }

    _glroute->Initialize(   &position[0],
                            (int*)&index[0],
                            position.size()/3,
                            index.size());
    _updateGLShape = false;
    _route = nullptr;
    update();
}


void openGLrenderer::addTestPoint(Core::Math::Vec2f position){
    _testPositionVector.push_back(Core::Math::Vec3f(position.x*mapScaleFactor,
                                                position.y*mapScaleFactor,
                                                0));
    update();
}

void openGLrenderer::clearTestPoint(){
    _testPositionMutex.lock();
    _testPositionVector.clear();
    _testPositionMutex.unlock();
}

void openGLrenderer::rotateShape(float degree){
    if(_shape != nullptr){
        _shape->rotate(degree);
        _glShape = generateShape(_shape);
        _updateGLShape = true;
        update();
    }
}

void openGLrenderer::translateShape(Core::Math::Vec2f delta){
    if(_shape != nullptr){
        _shape->translate(delta);
        //_glShape = generateShape(_shape);
        _updateGLShape = true;
        update();
    }
}

void openGLrenderer::scaleShape(Core::Math::Vec2f size){
    if(_shape != nullptr){
        _shape->scale(size);
        //_glShape = generateShape(_shape);
        _updateGLShape = true;
        update();
    }
}

void openGLrenderer::addShapeAndRoute(std::shared_ptr<VectorShape> shape,
                      std::shared_ptr<VectorShape> route){
    if(shape != nullptr)
    _shapeListTemp.push_back(shape);

    if(route != nullptr)
    _routeListTemp.push_back(route);

    _updateGLLists = true;

    update();
}

void openGLrenderer::addShapeAndRouteGL(){
    std::shared_ptr<VectorShape> shape = nullptr;
    std::shared_ptr<VectorShape> route = nullptr;
    if(!_shapeListTemp.empty()){
        shape = _shapeListTemp.at(0);
        if(shape != nullptr)
            _shapeList.push_back(generateShape(shape));
        _shapeListTemp.erase(_shapeListTemp.begin());
    }

    if(!_routeListTemp.empty()){
        route = _routeListTemp.at(0);
        if(route != nullptr)
            _routeList.push_back(generateShape(route));
        _routeListTemp.erase(_routeListTemp.begin());
    }

    if(!_routeListTemp.empty() || !_shapeListTemp.empty()){
        _updateGLLists = true;
    }else{
        _updateGLLists = false;
    }
    update();
}
