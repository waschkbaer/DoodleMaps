#ifndef OPENGLRENDERER_H
#define OPENGLRENDERER_H

#include <silverbullet/base/DetectEnv.h>



#include <opengl-base/ShaderDescriptor.h>
#include <opengl-base/GLProgram.h>
#ifdef DETECTED_OS_LINUX
    #undef Bool
    #undef Status
    #undef CursorShape
    #undef None
    #undef KeyPress
    #undef KeyRelease
    #undef FocusIn
    #undef FocusOut
    #undef FontChange
    #undef Expose
    #undef Unsorted
    #include <QtX11Extras/QtX11Extras>
#endif
#include <opengl-base/GLModel.h>
#include <opengl-base/GLVolumeBox.h>

#include <QOpenGLFunctions>
#include <QOpenGLWidget>

#include <QTimer>

#include <memory>



#include <utils/Camera.h>
#include <data/osmmap.h>
#include <data/shape.h>
#include <mutex>

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram);
QT_FORWARD_DECLARE_CLASS(QOpenGLTexture)

class openGLrenderer : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    openGLrenderer(QWidget* parent = 0);
    ~openGLrenderer();

    void initializeGL();
    void paintGL();

    void setView(Core::Math::Vec2f min,
                 Core::Math::Vec2f max);

    void paintMap();
    void paintShape();
    void paintLine();

    void generateShader();
    void generateFrameBuffer();
    void generateMap(std::shared_ptr<GPUMap> mapdata = nullptr);
    std::shared_ptr<GLModel> generateShape(std::shared_ptr<VectorShape> shape);

    void wheelEvent(QWheelEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);

    void repaint(){update();}
    void toggleDraw(bool b) {_shapeKeyDown = b;}
    void toggleMove(bool b) {_moveKeyDown = b;}
    void toggleCoord(bool b) {_coordKeyDown = b;}

    void setRouteShape(std::shared_ptr<VectorShape> route);
    std::shared_ptr<VectorShape> getShape() const {return _shape;}
    void resetShape() { _shape = nullptr;
                        _glShape = nullptr;
                        _glroute = nullptr;
                        _moveKeyDown = false;
                        _shapeKeyDown = false;
                        _updateGLLists = false;
                        _shapeListTemp.clear();
                        _routeListTemp.clear();
                        _shapeList.clear();
                        _routeList.clear();
                        update();}
    void loadShape(const std::string& filename){
          resetShape();
          _shape = std::make_shared<VectorShape>();
          _shape->loadShape(filename);
          _updateGLShape = true;
          update();
    }

    void setGLRouteShape(std::shared_ptr<VectorShape> route){
        _route = route;
        _glroute = nullptr;
        _updateGLRoute = true;
        update();
    }

    void addShapeAndRoute(std::shared_ptr<VectorShape> shape,
                          std::shared_ptr<VectorShape> route);

    void addShapeAndRouteGL();

    void addTestPoint(Core::Math::Vec2f position);
    void clearTestPoint();

    void rotateShape(float degree);
    void translateShape(Core::Math::Vec2f delta);
    void scaleShape(Core::Math::Vec2f size);

    void setMap(std::shared_ptr<OSMMap> map){_osmmap = map;}

private:
    bool loadAndCheckShaders(std::shared_ptr<GLProgram>& programPtr, ShaderDescriptor& sd);

private:
    std::shared_ptr<GLProgram>      _mapShader;
    std::shared_ptr<GLProgram>      _shapeShader;
    std::shared_ptr<GLProgram>      _lineShader;

    std::shared_ptr<GLModel>        _map;
    std::shared_ptr<GLModel>        _glShape;
    std::shared_ptr<GLModel>        _glroute;
    std::shared_ptr<GLVolumeBox>    _glBox;

    Core::Math::Mat4f               _projectionMatrix;
    Core::Math::Mat4f               _viewMatrix;
    Core::Math::Mat4f               _transformationMatrix;
    Core::Math::Mat4f               _boxTransformationMatrix;
    std::vector<Core::Math::Vec3f>  _testPositionVector;
    std::mutex                      _testPositionMutex;

    utils::Camera                   _camera;

    Core::Math::Vec2f               _min;
    Core::Math::Vec2f               _max;
    float                           _cameraDistance;
    bool                            _mouseDown;
    bool                            _shapeKeyDown;
    bool                            _moveKeyDown;
    bool                            _coordKeyDown;

    Core::Math::Vec2i              _lastPos;

    std::shared_ptr<VectorShape>    _shape;
    std::shared_ptr<VectorShape>    _route;
    bool                            _updateGLShape;
    bool                            _updateGLRoute;

    QTimer                          _loopTimer;


    std::vector<std::shared_ptr<VectorShape>>   _shapeListTemp;
    std::vector<std::shared_ptr<VectorShape>>   _routeListTemp;
    std::vector<std::shared_ptr<GLModel>>       _shapeList;
    std::vector<std::shared_ptr<GLModel>>       _routeList;
    bool                                        _updateGLLists;

    std::shared_ptr<OSMMap>                 _osmmap;
};

#endif // OPENGLRENDERER_H
