#version 330 core

uniform vec3 lineColor = vec3(0,0,0);

// OUTPUT VARIABLES
out vec4 outputColor;

// Pixel Shader
void main(void)
{
  outputColor = vec4(lineColor,1);
}
